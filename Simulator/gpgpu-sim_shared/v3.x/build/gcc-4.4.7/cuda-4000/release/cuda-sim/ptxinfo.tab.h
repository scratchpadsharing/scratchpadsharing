/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     INT_OPERAND = 258,
     HEADER = 259,
     INFO = 260,
     FUNC = 261,
     USED = 262,
     REGS = 263,
     BYTES = 264,
     LMEM = 265,
     SMEM = 266,
     CMEM = 267,
     IDENTIFIER = 268,
     PLUS = 269,
     COMMA = 270,
     LEFT_SQUARE_BRACKET = 271,
     RIGHT_SQUARE_BRACKET = 272,
     COLON = 273,
     SEMICOLON = 274,
     QUOTE = 275,
     LINE = 276,
     WARNING = 277,
     FOR = 278
   };
#endif
/* Tokens.  */
#define INT_OPERAND 258
#define HEADER 259
#define INFO 260
#define FUNC 261
#define USED 262
#define REGS 263
#define BYTES 264
#define LMEM 265
#define SMEM 266
#define CMEM 267
#define IDENTIFIER 268
#define PLUS 269
#define COMMA 270
#define LEFT_SQUARE_BRACKET 271
#define RIGHT_SQUARE_BRACKET 272
#define COLON 273
#define SEMICOLON 274
#define QUOTE 275
#define LINE 276
#define WARNING 277
#define FOR 278




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 30 "ptxinfo.y"
{
  int    int_value;
  char * string_value;
}
/* Line 1489 of yacc.c.  */
#line 100 "/home/vishwesh/Desktop/Register_Sharing_Pbm/CompilerOptimizations/gpgpu-sim_shared/v3.x/build/gcc-4.4.7/cuda-4000/release/cuda-sim/ptxinfo.tab.h"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE ptxinfo_lval;

