/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TOKEN_LABEL = 258,
     TOKEN_IDENTIFIER = 259,
     TOKEN_STRING = 260,
     TOKEN_METADATA = 261,
     TOKEN_INV_PREDICATE_IDENTIFIER = 262,
     TOKEN_PREDICATE_IDENTIFIER = 263,
     OPCODE_COPYSIGN = 264,
     OPCODE_COS = 265,
     OPCODE_SQRT = 266,
     OPCODE_ADD = 267,
     OPCODE_RSQRT = 268,
     OPCODE_MUL = 269,
     OPCODE_SAD = 270,
     OPCODE_SUB = 271,
     OPCODE_EX2 = 272,
     OPCODE_LG2 = 273,
     OPCODE_ADDC = 274,
     OPCODE_RCP = 275,
     OPCODE_SIN = 276,
     OPCODE_REM = 277,
     OPCODE_MUL24 = 278,
     OPCODE_MAD24 = 279,
     OPCODE_DIV = 280,
     OPCODE_ABS = 281,
     OPCODE_NEG = 282,
     OPCODE_MIN = 283,
     OPCODE_MAX = 284,
     OPCODE_MAD = 285,
     OPCODE_MADC = 286,
     OPCODE_SET = 287,
     OPCODE_SETP = 288,
     OPCODE_SELP = 289,
     OPCODE_SLCT = 290,
     OPCODE_MOV = 291,
     OPCODE_ST = 292,
     OPCODE_CVT = 293,
     OPCODE_AND = 294,
     OPCODE_XOR = 295,
     OPCODE_OR = 296,
     OPCODE_CVTA = 297,
     OPCODE_ISSPACEP = 298,
     OPCODE_LDU = 299,
     OPCODE_SULD = 300,
     OPCODE_TXQ = 301,
     OPCODE_SUST = 302,
     OPCODE_SURED = 303,
     OPCODE_SUQ = 304,
     OPCODE_BRA = 305,
     OPCODE_CALL = 306,
     OPCODE_RET = 307,
     OPCODE_EXIT = 308,
     OPCODE_TRAP = 309,
     OPCODE_BRKPT = 310,
     OPCODE_SUBC = 311,
     OPCODE_TEX = 312,
     OPCODE_LD = 313,
     OPCODE_BARSYNC = 314,
     OPCODE_ATOM = 315,
     OPCODE_RED = 316,
     OPCODE_NOT = 317,
     OPCODE_CNOT = 318,
     OPCODE_VOTE = 319,
     OPCODE_SHR = 320,
     OPCODE_SHL = 321,
     OPCODE_FMA = 322,
     OPCODE_MEMBAR = 323,
     OPCODE_PMEVENT = 324,
     OPCODE_POPC = 325,
     OPCODE_PRMT = 326,
     OPCODE_CLZ = 327,
     OPCODE_BFIND = 328,
     OPCODE_BREV = 329,
     OPCODE_BFI = 330,
     OPCODE_BFE = 331,
     OPCODE_TESTP = 332,
     OPCODE_TLD4 = 333,
     OPCODE_BAR = 334,
     OPCODE_PREFETCH = 335,
     OPCODE_PREFETCHU = 336,
     OPCODE_SHFL = 337,
     PREPROCESSOR_INCLUDE = 338,
     PREPROCESSOR_DEFINE = 339,
     PREPROCESSOR_IF = 340,
     PREPROCESSOR_IFDEF = 341,
     PREPROCESSOR_ELSE = 342,
     PREPROCESSOR_ENDIF = 343,
     PREPROCESSOR_LINE = 344,
     PREPROCESSOR_FILE = 345,
     TOKEN_ENTRY = 346,
     TOKEN_EXTERN = 347,
     TOKEN_FILE = 348,
     TOKEN_VISIBLE = 349,
     TOKEN_LOC = 350,
     TOKEN_FUNCTION = 351,
     TOKEN_STRUCT = 352,
     TOKEN_UNION = 353,
     TOKEN_TARGET = 354,
     TOKEN_VERSION = 355,
     TOKEN_SECTION = 356,
     TOKEN_ADDRESS_SIZE = 357,
     TOKEN_WEAK = 358,
     TOKEN_MAXNREG = 359,
     TOKEN_MAXNTID = 360,
     TOKEN_MAXNCTAPERSM = 361,
     TOKEN_MINNCTAPERSM = 362,
     TOKEN_SM11 = 363,
     TOKEN_SM12 = 364,
     TOKEN_SM13 = 365,
     TOKEN_SM20 = 366,
     TOKEN_MAP_F64_TO_F32 = 367,
     TOKEN_SM21 = 368,
     TOKEN_SM10 = 369,
     TOKEN_SM30 = 370,
     TOKEN_SM35 = 371,
     TOKEN_TEXMODE_INDEPENDENT = 372,
     TOKEN_TEXMODE_UNIFIED = 373,
     TOKEN_CONST = 374,
     TOKEN_GLOBAL = 375,
     TOKEN_LOCAL = 376,
     TOKEN_PARAM = 377,
     TOKEN_PRAGMA = 378,
     TOKEN_PTR = 379,
     TOKEN_REG = 380,
     TOKEN_SHARED = 381,
     TOKEN_TEXREF = 382,
     TOKEN_CTA = 383,
     TOKEN_SURFREF = 384,
     TOKEN_GL = 385,
     TOKEN_SYS = 386,
     TOKEN_SAMPLERREF = 387,
     TOKEN_U32 = 388,
     TOKEN_S32 = 389,
     TOKEN_S8 = 390,
     TOKEN_S16 = 391,
     TOKEN_S64 = 392,
     TOKEN_U8 = 393,
     TOKEN_U16 = 394,
     TOKEN_U64 = 395,
     TOKEN_B8 = 396,
     TOKEN_B16 = 397,
     TOKEN_B32 = 398,
     TOKEN_B64 = 399,
     TOKEN_F16 = 400,
     TOKEN_F64 = 401,
     TOKEN_F32 = 402,
     TOKEN_PRED = 403,
     TOKEN_EQ = 404,
     TOKEN_NE = 405,
     TOKEN_LT = 406,
     TOKEN_LE = 407,
     TOKEN_GT = 408,
     TOKEN_GE = 409,
     TOKEN_LS = 410,
     TOKEN_HS = 411,
     TOKEN_EQU = 412,
     TOKEN_NEU = 413,
     TOKEN_LTU = 414,
     TOKEN_LEU = 415,
     TOKEN_GTU = 416,
     TOKEN_GEU = 417,
     TOKEN_NUM = 418,
     TOKEN_NAN = 419,
     TOKEN_HI = 420,
     TOKEN_LO = 421,
     TOKEN_AND = 422,
     TOKEN_OR = 423,
     TOKEN_XOR = 424,
     TOKEN_RN = 425,
     TOKEN_RM = 426,
     TOKEN_RZ = 427,
     TOKEN_RP = 428,
     TOKEN_SAT = 429,
     TOKEN_VOLATILE = 430,
     TOKEN_TAIL = 431,
     TOKEN_UNI = 432,
     TOKEN_ALIGN = 433,
     TOKEN_BYTE = 434,
     TOKEN_WIDE = 435,
     TOKEN_CARRY = 436,
     TOKEN_RNI = 437,
     TOKEN_RMI = 438,
     TOKEN_RZI = 439,
     TOKEN_RPI = 440,
     TOKEN_FTZ = 441,
     TOKEN_APPROX = 442,
     TOKEN_FULL = 443,
     TOKEN_SHIFT_AMOUNT = 444,
     TOKEN_R = 445,
     TOKEN_G = 446,
     TOKEN_B = 447,
     TOKEN_A = 448,
     TOKEN_TO = 449,
     TOKEN_CALL_PROTOTYPE = 450,
     TOKEN_CALL_TARGETS = 451,
     TOKEN_V2 = 452,
     TOKEN_V4 = 453,
     TOKEN_X = 454,
     TOKEN_Y = 455,
     TOKEN_Z = 456,
     TOKEN_W = 457,
     TOKEN_ANY = 458,
     TOKEN_ALL = 459,
     TOKEN_UP = 460,
     TOKEN_DOWN = 461,
     TOKEN_BFLY = 462,
     TOKEN_IDX = 463,
     TOKEN_MIN = 464,
     TOKEN_MAX = 465,
     TOKEN_DEC = 466,
     TOKEN_INC = 467,
     TOKEN_ADD = 468,
     TOKEN_CAS = 469,
     TOKEN_EXCH = 470,
     TOKEN_1D = 471,
     TOKEN_2D = 472,
     TOKEN_3D = 473,
     TOKEN_A1D = 474,
     TOKEN_A2D = 475,
     TOKEN_CUBE = 476,
     TOKEN_ACUBE = 477,
     TOKEN_CA = 478,
     TOKEN_WB = 479,
     TOKEN_CG = 480,
     TOKEN_CS = 481,
     TOKEN_LU = 482,
     TOKEN_CV = 483,
     TOKEN_WT = 484,
     TOKEN_NC = 485,
     TOKEN_L1 = 486,
     TOKEN_L2 = 487,
     TOKEN_P = 488,
     TOKEN_WIDTH = 489,
     TOKEN_DEPTH = 490,
     TOKEN_HEIGHT = 491,
     TOKEN_NORMALIZED_COORDS = 492,
     TOKEN_FILTER_MODE = 493,
     TOKEN_ADDR_MODE_0 = 494,
     TOKEN_ADDR_MODE_1 = 495,
     TOKEN_ADDR_MODE_2 = 496,
     TOKEN_CHANNEL_DATA_TYPE = 497,
     TOKEN_CHANNEL_ORDER = 498,
     TOKEN_TRAP = 499,
     TOKEN_CLAMP = 500,
     TOKEN_ZERO = 501,
     TOKEN_ARRIVE = 502,
     TOKEN_RED = 503,
     TOKEN_POPC = 504,
     TOKEN_SYNC = 505,
     TOKEN_BALLOT = 506,
     TOKEN_F4E = 507,
     TOKEN_B4E = 508,
     TOKEN_RC8 = 509,
     TOKEN_ECL = 510,
     TOKEN_ECR = 511,
     TOKEN_RC16 = 512,
     TOKEN_FINITE = 513,
     TOKEN_INFINITE = 514,
     TOKEN_NUMBER = 515,
     TOKEN_NOT_A_NUMBER = 516,
     TOKEN_NORMAL = 517,
     TOKEN_SUBNORMAL = 518,
     TOKEN_DECIMAL_CONSTANT = 519,
     TOKEN_UNSIGNED_DECIMAL_CONSTANT = 520,
     TOKEN_SINGLE_CONSTANT = 521,
     TOKEN_DOUBLE_CONSTANT = 522
   };
#endif
/* Tokens.  */
#define TOKEN_LABEL 258
#define TOKEN_IDENTIFIER 259
#define TOKEN_STRING 260
#define TOKEN_METADATA 261
#define TOKEN_INV_PREDICATE_IDENTIFIER 262
#define TOKEN_PREDICATE_IDENTIFIER 263
#define OPCODE_COPYSIGN 264
#define OPCODE_COS 265
#define OPCODE_SQRT 266
#define OPCODE_ADD 267
#define OPCODE_RSQRT 268
#define OPCODE_MUL 269
#define OPCODE_SAD 270
#define OPCODE_SUB 271
#define OPCODE_EX2 272
#define OPCODE_LG2 273
#define OPCODE_ADDC 274
#define OPCODE_RCP 275
#define OPCODE_SIN 276
#define OPCODE_REM 277
#define OPCODE_MUL24 278
#define OPCODE_MAD24 279
#define OPCODE_DIV 280
#define OPCODE_ABS 281
#define OPCODE_NEG 282
#define OPCODE_MIN 283
#define OPCODE_MAX 284
#define OPCODE_MAD 285
#define OPCODE_MADC 286
#define OPCODE_SET 287
#define OPCODE_SETP 288
#define OPCODE_SELP 289
#define OPCODE_SLCT 290
#define OPCODE_MOV 291
#define OPCODE_ST 292
#define OPCODE_CVT 293
#define OPCODE_AND 294
#define OPCODE_XOR 295
#define OPCODE_OR 296
#define OPCODE_CVTA 297
#define OPCODE_ISSPACEP 298
#define OPCODE_LDU 299
#define OPCODE_SULD 300
#define OPCODE_TXQ 301
#define OPCODE_SUST 302
#define OPCODE_SURED 303
#define OPCODE_SUQ 304
#define OPCODE_BRA 305
#define OPCODE_CALL 306
#define OPCODE_RET 307
#define OPCODE_EXIT 308
#define OPCODE_TRAP 309
#define OPCODE_BRKPT 310
#define OPCODE_SUBC 311
#define OPCODE_TEX 312
#define OPCODE_LD 313
#define OPCODE_BARSYNC 314
#define OPCODE_ATOM 315
#define OPCODE_RED 316
#define OPCODE_NOT 317
#define OPCODE_CNOT 318
#define OPCODE_VOTE 319
#define OPCODE_SHR 320
#define OPCODE_SHL 321
#define OPCODE_FMA 322
#define OPCODE_MEMBAR 323
#define OPCODE_PMEVENT 324
#define OPCODE_POPC 325
#define OPCODE_PRMT 326
#define OPCODE_CLZ 327
#define OPCODE_BFIND 328
#define OPCODE_BREV 329
#define OPCODE_BFI 330
#define OPCODE_BFE 331
#define OPCODE_TESTP 332
#define OPCODE_TLD4 333
#define OPCODE_BAR 334
#define OPCODE_PREFETCH 335
#define OPCODE_PREFETCHU 336
#define OPCODE_SHFL 337
#define PREPROCESSOR_INCLUDE 338
#define PREPROCESSOR_DEFINE 339
#define PREPROCESSOR_IF 340
#define PREPROCESSOR_IFDEF 341
#define PREPROCESSOR_ELSE 342
#define PREPROCESSOR_ENDIF 343
#define PREPROCESSOR_LINE 344
#define PREPROCESSOR_FILE 345
#define TOKEN_ENTRY 346
#define TOKEN_EXTERN 347
#define TOKEN_FILE 348
#define TOKEN_VISIBLE 349
#define TOKEN_LOC 350
#define TOKEN_FUNCTION 351
#define TOKEN_STRUCT 352
#define TOKEN_UNION 353
#define TOKEN_TARGET 354
#define TOKEN_VERSION 355
#define TOKEN_SECTION 356
#define TOKEN_ADDRESS_SIZE 357
#define TOKEN_WEAK 358
#define TOKEN_MAXNREG 359
#define TOKEN_MAXNTID 360
#define TOKEN_MAXNCTAPERSM 361
#define TOKEN_MINNCTAPERSM 362
#define TOKEN_SM11 363
#define TOKEN_SM12 364
#define TOKEN_SM13 365
#define TOKEN_SM20 366
#define TOKEN_MAP_F64_TO_F32 367
#define TOKEN_SM21 368
#define TOKEN_SM10 369
#define TOKEN_SM30 370
#define TOKEN_SM35 371
#define TOKEN_TEXMODE_INDEPENDENT 372
#define TOKEN_TEXMODE_UNIFIED 373
#define TOKEN_CONST 374
#define TOKEN_GLOBAL 375
#define TOKEN_LOCAL 376
#define TOKEN_PARAM 377
#define TOKEN_PRAGMA 378
#define TOKEN_PTR 379
#define TOKEN_REG 380
#define TOKEN_SHARED 381
#define TOKEN_TEXREF 382
#define TOKEN_CTA 383
#define TOKEN_SURFREF 384
#define TOKEN_GL 385
#define TOKEN_SYS 386
#define TOKEN_SAMPLERREF 387
#define TOKEN_U32 388
#define TOKEN_S32 389
#define TOKEN_S8 390
#define TOKEN_S16 391
#define TOKEN_S64 392
#define TOKEN_U8 393
#define TOKEN_U16 394
#define TOKEN_U64 395
#define TOKEN_B8 396
#define TOKEN_B16 397
#define TOKEN_B32 398
#define TOKEN_B64 399
#define TOKEN_F16 400
#define TOKEN_F64 401
#define TOKEN_F32 402
#define TOKEN_PRED 403
#define TOKEN_EQ 404
#define TOKEN_NE 405
#define TOKEN_LT 406
#define TOKEN_LE 407
#define TOKEN_GT 408
#define TOKEN_GE 409
#define TOKEN_LS 410
#define TOKEN_HS 411
#define TOKEN_EQU 412
#define TOKEN_NEU 413
#define TOKEN_LTU 414
#define TOKEN_LEU 415
#define TOKEN_GTU 416
#define TOKEN_GEU 417
#define TOKEN_NUM 418
#define TOKEN_NAN 419
#define TOKEN_HI 420
#define TOKEN_LO 421
#define TOKEN_AND 422
#define TOKEN_OR 423
#define TOKEN_XOR 424
#define TOKEN_RN 425
#define TOKEN_RM 426
#define TOKEN_RZ 427
#define TOKEN_RP 428
#define TOKEN_SAT 429
#define TOKEN_VOLATILE 430
#define TOKEN_TAIL 431
#define TOKEN_UNI 432
#define TOKEN_ALIGN 433
#define TOKEN_BYTE 434
#define TOKEN_WIDE 435
#define TOKEN_CARRY 436
#define TOKEN_RNI 437
#define TOKEN_RMI 438
#define TOKEN_RZI 439
#define TOKEN_RPI 440
#define TOKEN_FTZ 441
#define TOKEN_APPROX 442
#define TOKEN_FULL 443
#define TOKEN_SHIFT_AMOUNT 444
#define TOKEN_R 445
#define TOKEN_G 446
#define TOKEN_B 447
#define TOKEN_A 448
#define TOKEN_TO 449
#define TOKEN_CALL_PROTOTYPE 450
#define TOKEN_CALL_TARGETS 451
#define TOKEN_V2 452
#define TOKEN_V4 453
#define TOKEN_X 454
#define TOKEN_Y 455
#define TOKEN_Z 456
#define TOKEN_W 457
#define TOKEN_ANY 458
#define TOKEN_ALL 459
#define TOKEN_UP 460
#define TOKEN_DOWN 461
#define TOKEN_BFLY 462
#define TOKEN_IDX 463
#define TOKEN_MIN 464
#define TOKEN_MAX 465
#define TOKEN_DEC 466
#define TOKEN_INC 467
#define TOKEN_ADD 468
#define TOKEN_CAS 469
#define TOKEN_EXCH 470
#define TOKEN_1D 471
#define TOKEN_2D 472
#define TOKEN_3D 473
#define TOKEN_A1D 474
#define TOKEN_A2D 475
#define TOKEN_CUBE 476
#define TOKEN_ACUBE 477
#define TOKEN_CA 478
#define TOKEN_WB 479
#define TOKEN_CG 480
#define TOKEN_CS 481
#define TOKEN_LU 482
#define TOKEN_CV 483
#define TOKEN_WT 484
#define TOKEN_NC 485
#define TOKEN_L1 486
#define TOKEN_L2 487
#define TOKEN_P 488
#define TOKEN_WIDTH 489
#define TOKEN_DEPTH 490
#define TOKEN_HEIGHT 491
#define TOKEN_NORMALIZED_COORDS 492
#define TOKEN_FILTER_MODE 493
#define TOKEN_ADDR_MODE_0 494
#define TOKEN_ADDR_MODE_1 495
#define TOKEN_ADDR_MODE_2 496
#define TOKEN_CHANNEL_DATA_TYPE 497
#define TOKEN_CHANNEL_ORDER 498
#define TOKEN_TRAP 499
#define TOKEN_CLAMP 500
#define TOKEN_ZERO 501
#define TOKEN_ARRIVE 502
#define TOKEN_RED 503
#define TOKEN_POPC 504
#define TOKEN_SYNC 505
#define TOKEN_BALLOT 506
#define TOKEN_F4E 507
#define TOKEN_B4E 508
#define TOKEN_RC8 509
#define TOKEN_ECL 510
#define TOKEN_ECR 511
#define TOKEN_RC16 512
#define TOKEN_FINITE 513
#define TOKEN_INFINITE 514
#define TOKEN_NUMBER 515
#define TOKEN_NOT_A_NUMBER 516
#define TOKEN_NORMAL 517
#define TOKEN_SUBNORMAL 518
#define TOKEN_DECIMAL_CONSTANT 519
#define TOKEN_UNSIGNED_DECIMAL_CONSTANT 520
#define TOKEN_SINGLE_CONSTANT 521
#define TOKEN_DOUBLE_CONSTANT 522




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 36 "ocelot/parser/implementation/ptxgrammar.yy"
{
	char text[1024];
	long long int value;
	long long unsigned int uvalue;

	double doubleFloat;
	float singleFloat;
}
/* Line 1489 of yacc.c.  */
#line 592 ".release_build/ptxgrammar.hpp"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


