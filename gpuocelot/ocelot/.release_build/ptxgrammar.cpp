/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Using locations.  */
#define YYLSP_NEEDED 1



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TOKEN_LABEL = 258,
     TOKEN_IDENTIFIER = 259,
     TOKEN_STRING = 260,
     TOKEN_METADATA = 261,
     TOKEN_INV_PREDICATE_IDENTIFIER = 262,
     TOKEN_PREDICATE_IDENTIFIER = 263,
     OPCODE_COPYSIGN = 264,
     OPCODE_COS = 265,
     OPCODE_SQRT = 266,
     OPCODE_ADD = 267,
     OPCODE_RSQRT = 268,
     OPCODE_MUL = 269,
     OPCODE_SAD = 270,
     OPCODE_SUB = 271,
     OPCODE_EX2 = 272,
     OPCODE_LG2 = 273,
     OPCODE_ADDC = 274,
     OPCODE_RCP = 275,
     OPCODE_SIN = 276,
     OPCODE_REM = 277,
     OPCODE_MUL24 = 278,
     OPCODE_MAD24 = 279,
     OPCODE_DIV = 280,
     OPCODE_ABS = 281,
     OPCODE_NEG = 282,
     OPCODE_MIN = 283,
     OPCODE_MAX = 284,
     OPCODE_MAD = 285,
     OPCODE_MADC = 286,
     OPCODE_SET = 287,
     OPCODE_SETP = 288,
     OPCODE_SELP = 289,
     OPCODE_SLCT = 290,
     OPCODE_MOV = 291,
     OPCODE_ST = 292,
     OPCODE_CVT = 293,
     OPCODE_AND = 294,
     OPCODE_XOR = 295,
     OPCODE_OR = 296,
     OPCODE_CVTA = 297,
     OPCODE_ISSPACEP = 298,
     OPCODE_LDU = 299,
     OPCODE_SULD = 300,
     OPCODE_TXQ = 301,
     OPCODE_SUST = 302,
     OPCODE_SURED = 303,
     OPCODE_SUQ = 304,
     OPCODE_BRA = 305,
     OPCODE_CALL = 306,
     OPCODE_RET = 307,
     OPCODE_EXIT = 308,
     OPCODE_TRAP = 309,
     OPCODE_BRKPT = 310,
     OPCODE_SUBC = 311,
     OPCODE_TEX = 312,
     OPCODE_LD = 313,
     OPCODE_BARSYNC = 314,
     OPCODE_ATOM = 315,
     OPCODE_RED = 316,
     OPCODE_NOT = 317,
     OPCODE_CNOT = 318,
     OPCODE_VOTE = 319,
     OPCODE_SHR = 320,
     OPCODE_SHL = 321,
     OPCODE_FMA = 322,
     OPCODE_MEMBAR = 323,
     OPCODE_PMEVENT = 324,
     OPCODE_POPC = 325,
     OPCODE_PRMT = 326,
     OPCODE_CLZ = 327,
     OPCODE_BFIND = 328,
     OPCODE_BREV = 329,
     OPCODE_BFI = 330,
     OPCODE_BFE = 331,
     OPCODE_TESTP = 332,
     OPCODE_TLD4 = 333,
     OPCODE_BAR = 334,
     OPCODE_PREFETCH = 335,
     OPCODE_PREFETCHU = 336,
     OPCODE_SHFL = 337,
     PREPROCESSOR_INCLUDE = 338,
     PREPROCESSOR_DEFINE = 339,
     PREPROCESSOR_IF = 340,
     PREPROCESSOR_IFDEF = 341,
     PREPROCESSOR_ELSE = 342,
     PREPROCESSOR_ENDIF = 343,
     PREPROCESSOR_LINE = 344,
     PREPROCESSOR_FILE = 345,
     TOKEN_ENTRY = 346,
     TOKEN_EXTERN = 347,
     TOKEN_FILE = 348,
     TOKEN_VISIBLE = 349,
     TOKEN_LOC = 350,
     TOKEN_FUNCTION = 351,
     TOKEN_STRUCT = 352,
     TOKEN_UNION = 353,
     TOKEN_TARGET = 354,
     TOKEN_VERSION = 355,
     TOKEN_SECTION = 356,
     TOKEN_ADDRESS_SIZE = 357,
     TOKEN_WEAK = 358,
     TOKEN_MAXNREG = 359,
     TOKEN_MAXNTID = 360,
     TOKEN_MAXNCTAPERSM = 361,
     TOKEN_MINNCTAPERSM = 362,
     TOKEN_SM11 = 363,
     TOKEN_SM12 = 364,
     TOKEN_SM13 = 365,
     TOKEN_SM20 = 366,
     TOKEN_MAP_F64_TO_F32 = 367,
     TOKEN_SM21 = 368,
     TOKEN_SM10 = 369,
     TOKEN_SM30 = 370,
     TOKEN_SM35 = 371,
     TOKEN_TEXMODE_INDEPENDENT = 372,
     TOKEN_TEXMODE_UNIFIED = 373,
     TOKEN_CONST = 374,
     TOKEN_GLOBAL = 375,
     TOKEN_LOCAL = 376,
     TOKEN_PARAM = 377,
     TOKEN_PRAGMA = 378,
     TOKEN_PTR = 379,
     TOKEN_REG = 380,
     TOKEN_SHARED = 381,
     TOKEN_TEXREF = 382,
     TOKEN_CTA = 383,
     TOKEN_SURFREF = 384,
     TOKEN_GL = 385,
     TOKEN_SYS = 386,
     TOKEN_SAMPLERREF = 387,
     TOKEN_U32 = 388,
     TOKEN_S32 = 389,
     TOKEN_S8 = 390,
     TOKEN_S16 = 391,
     TOKEN_S64 = 392,
     TOKEN_U8 = 393,
     TOKEN_U16 = 394,
     TOKEN_U64 = 395,
     TOKEN_B8 = 396,
     TOKEN_B16 = 397,
     TOKEN_B32 = 398,
     TOKEN_B64 = 399,
     TOKEN_F16 = 400,
     TOKEN_F64 = 401,
     TOKEN_F32 = 402,
     TOKEN_PRED = 403,
     TOKEN_EQ = 404,
     TOKEN_NE = 405,
     TOKEN_LT = 406,
     TOKEN_LE = 407,
     TOKEN_GT = 408,
     TOKEN_GE = 409,
     TOKEN_LS = 410,
     TOKEN_HS = 411,
     TOKEN_EQU = 412,
     TOKEN_NEU = 413,
     TOKEN_LTU = 414,
     TOKEN_LEU = 415,
     TOKEN_GTU = 416,
     TOKEN_GEU = 417,
     TOKEN_NUM = 418,
     TOKEN_NAN = 419,
     TOKEN_HI = 420,
     TOKEN_LO = 421,
     TOKEN_AND = 422,
     TOKEN_OR = 423,
     TOKEN_XOR = 424,
     TOKEN_RN = 425,
     TOKEN_RM = 426,
     TOKEN_RZ = 427,
     TOKEN_RP = 428,
     TOKEN_SAT = 429,
     TOKEN_VOLATILE = 430,
     TOKEN_TAIL = 431,
     TOKEN_UNI = 432,
     TOKEN_ALIGN = 433,
     TOKEN_BYTE = 434,
     TOKEN_WIDE = 435,
     TOKEN_CARRY = 436,
     TOKEN_RNI = 437,
     TOKEN_RMI = 438,
     TOKEN_RZI = 439,
     TOKEN_RPI = 440,
     TOKEN_FTZ = 441,
     TOKEN_APPROX = 442,
     TOKEN_FULL = 443,
     TOKEN_SHIFT_AMOUNT = 444,
     TOKEN_R = 445,
     TOKEN_G = 446,
     TOKEN_B = 447,
     TOKEN_A = 448,
     TOKEN_TO = 449,
     TOKEN_CALL_PROTOTYPE = 450,
     TOKEN_CALL_TARGETS = 451,
     TOKEN_V2 = 452,
     TOKEN_V4 = 453,
     TOKEN_X = 454,
     TOKEN_Y = 455,
     TOKEN_Z = 456,
     TOKEN_W = 457,
     TOKEN_ANY = 458,
     TOKEN_ALL = 459,
     TOKEN_UP = 460,
     TOKEN_DOWN = 461,
     TOKEN_BFLY = 462,
     TOKEN_IDX = 463,
     TOKEN_MIN = 464,
     TOKEN_MAX = 465,
     TOKEN_DEC = 466,
     TOKEN_INC = 467,
     TOKEN_ADD = 468,
     TOKEN_CAS = 469,
     TOKEN_EXCH = 470,
     TOKEN_1D = 471,
     TOKEN_2D = 472,
     TOKEN_3D = 473,
     TOKEN_A1D = 474,
     TOKEN_A2D = 475,
     TOKEN_CUBE = 476,
     TOKEN_ACUBE = 477,
     TOKEN_CA = 478,
     TOKEN_WB = 479,
     TOKEN_CG = 480,
     TOKEN_CS = 481,
     TOKEN_LU = 482,
     TOKEN_CV = 483,
     TOKEN_WT = 484,
     TOKEN_NC = 485,
     TOKEN_L1 = 486,
     TOKEN_L2 = 487,
     TOKEN_P = 488,
     TOKEN_WIDTH = 489,
     TOKEN_DEPTH = 490,
     TOKEN_HEIGHT = 491,
     TOKEN_NORMALIZED_COORDS = 492,
     TOKEN_FILTER_MODE = 493,
     TOKEN_ADDR_MODE_0 = 494,
     TOKEN_ADDR_MODE_1 = 495,
     TOKEN_ADDR_MODE_2 = 496,
     TOKEN_CHANNEL_DATA_TYPE = 497,
     TOKEN_CHANNEL_ORDER = 498,
     TOKEN_TRAP = 499,
     TOKEN_CLAMP = 500,
     TOKEN_ZERO = 501,
     TOKEN_ARRIVE = 502,
     TOKEN_RED = 503,
     TOKEN_POPC = 504,
     TOKEN_SYNC = 505,
     TOKEN_BALLOT = 506,
     TOKEN_F4E = 507,
     TOKEN_B4E = 508,
     TOKEN_RC8 = 509,
     TOKEN_ECL = 510,
     TOKEN_ECR = 511,
     TOKEN_RC16 = 512,
     TOKEN_FINITE = 513,
     TOKEN_INFINITE = 514,
     TOKEN_NUMBER = 515,
     TOKEN_NOT_A_NUMBER = 516,
     TOKEN_NORMAL = 517,
     TOKEN_SUBNORMAL = 518,
     TOKEN_DECIMAL_CONSTANT = 519,
     TOKEN_UNSIGNED_DECIMAL_CONSTANT = 520,
     TOKEN_SINGLE_CONSTANT = 521,
     TOKEN_DOUBLE_CONSTANT = 522
   };
#endif
/* Tokens.  */
#define TOKEN_LABEL 258
#define TOKEN_IDENTIFIER 259
#define TOKEN_STRING 260
#define TOKEN_METADATA 261
#define TOKEN_INV_PREDICATE_IDENTIFIER 262
#define TOKEN_PREDICATE_IDENTIFIER 263
#define OPCODE_COPYSIGN 264
#define OPCODE_COS 265
#define OPCODE_SQRT 266
#define OPCODE_ADD 267
#define OPCODE_RSQRT 268
#define OPCODE_MUL 269
#define OPCODE_SAD 270
#define OPCODE_SUB 271
#define OPCODE_EX2 272
#define OPCODE_LG2 273
#define OPCODE_ADDC 274
#define OPCODE_RCP 275
#define OPCODE_SIN 276
#define OPCODE_REM 277
#define OPCODE_MUL24 278
#define OPCODE_MAD24 279
#define OPCODE_DIV 280
#define OPCODE_ABS 281
#define OPCODE_NEG 282
#define OPCODE_MIN 283
#define OPCODE_MAX 284
#define OPCODE_MAD 285
#define OPCODE_MADC 286
#define OPCODE_SET 287
#define OPCODE_SETP 288
#define OPCODE_SELP 289
#define OPCODE_SLCT 290
#define OPCODE_MOV 291
#define OPCODE_ST 292
#define OPCODE_CVT 293
#define OPCODE_AND 294
#define OPCODE_XOR 295
#define OPCODE_OR 296
#define OPCODE_CVTA 297
#define OPCODE_ISSPACEP 298
#define OPCODE_LDU 299
#define OPCODE_SULD 300
#define OPCODE_TXQ 301
#define OPCODE_SUST 302
#define OPCODE_SURED 303
#define OPCODE_SUQ 304
#define OPCODE_BRA 305
#define OPCODE_CALL 306
#define OPCODE_RET 307
#define OPCODE_EXIT 308
#define OPCODE_TRAP 309
#define OPCODE_BRKPT 310
#define OPCODE_SUBC 311
#define OPCODE_TEX 312
#define OPCODE_LD 313
#define OPCODE_BARSYNC 314
#define OPCODE_ATOM 315
#define OPCODE_RED 316
#define OPCODE_NOT 317
#define OPCODE_CNOT 318
#define OPCODE_VOTE 319
#define OPCODE_SHR 320
#define OPCODE_SHL 321
#define OPCODE_FMA 322
#define OPCODE_MEMBAR 323
#define OPCODE_PMEVENT 324
#define OPCODE_POPC 325
#define OPCODE_PRMT 326
#define OPCODE_CLZ 327
#define OPCODE_BFIND 328
#define OPCODE_BREV 329
#define OPCODE_BFI 330
#define OPCODE_BFE 331
#define OPCODE_TESTP 332
#define OPCODE_TLD4 333
#define OPCODE_BAR 334
#define OPCODE_PREFETCH 335
#define OPCODE_PREFETCHU 336
#define OPCODE_SHFL 337
#define PREPROCESSOR_INCLUDE 338
#define PREPROCESSOR_DEFINE 339
#define PREPROCESSOR_IF 340
#define PREPROCESSOR_IFDEF 341
#define PREPROCESSOR_ELSE 342
#define PREPROCESSOR_ENDIF 343
#define PREPROCESSOR_LINE 344
#define PREPROCESSOR_FILE 345
#define TOKEN_ENTRY 346
#define TOKEN_EXTERN 347
#define TOKEN_FILE 348
#define TOKEN_VISIBLE 349
#define TOKEN_LOC 350
#define TOKEN_FUNCTION 351
#define TOKEN_STRUCT 352
#define TOKEN_UNION 353
#define TOKEN_TARGET 354
#define TOKEN_VERSION 355
#define TOKEN_SECTION 356
#define TOKEN_ADDRESS_SIZE 357
#define TOKEN_WEAK 358
#define TOKEN_MAXNREG 359
#define TOKEN_MAXNTID 360
#define TOKEN_MAXNCTAPERSM 361
#define TOKEN_MINNCTAPERSM 362
#define TOKEN_SM11 363
#define TOKEN_SM12 364
#define TOKEN_SM13 365
#define TOKEN_SM20 366
#define TOKEN_MAP_F64_TO_F32 367
#define TOKEN_SM21 368
#define TOKEN_SM10 369
#define TOKEN_SM30 370
#define TOKEN_SM35 371
#define TOKEN_TEXMODE_INDEPENDENT 372
#define TOKEN_TEXMODE_UNIFIED 373
#define TOKEN_CONST 374
#define TOKEN_GLOBAL 375
#define TOKEN_LOCAL 376
#define TOKEN_PARAM 377
#define TOKEN_PRAGMA 378
#define TOKEN_PTR 379
#define TOKEN_REG 380
#define TOKEN_SHARED 381
#define TOKEN_TEXREF 382
#define TOKEN_CTA 383
#define TOKEN_SURFREF 384
#define TOKEN_GL 385
#define TOKEN_SYS 386
#define TOKEN_SAMPLERREF 387
#define TOKEN_U32 388
#define TOKEN_S32 389
#define TOKEN_S8 390
#define TOKEN_S16 391
#define TOKEN_S64 392
#define TOKEN_U8 393
#define TOKEN_U16 394
#define TOKEN_U64 395
#define TOKEN_B8 396
#define TOKEN_B16 397
#define TOKEN_B32 398
#define TOKEN_B64 399
#define TOKEN_F16 400
#define TOKEN_F64 401
#define TOKEN_F32 402
#define TOKEN_PRED 403
#define TOKEN_EQ 404
#define TOKEN_NE 405
#define TOKEN_LT 406
#define TOKEN_LE 407
#define TOKEN_GT 408
#define TOKEN_GE 409
#define TOKEN_LS 410
#define TOKEN_HS 411
#define TOKEN_EQU 412
#define TOKEN_NEU 413
#define TOKEN_LTU 414
#define TOKEN_LEU 415
#define TOKEN_GTU 416
#define TOKEN_GEU 417
#define TOKEN_NUM 418
#define TOKEN_NAN 419
#define TOKEN_HI 420
#define TOKEN_LO 421
#define TOKEN_AND 422
#define TOKEN_OR 423
#define TOKEN_XOR 424
#define TOKEN_RN 425
#define TOKEN_RM 426
#define TOKEN_RZ 427
#define TOKEN_RP 428
#define TOKEN_SAT 429
#define TOKEN_VOLATILE 430
#define TOKEN_TAIL 431
#define TOKEN_UNI 432
#define TOKEN_ALIGN 433
#define TOKEN_BYTE 434
#define TOKEN_WIDE 435
#define TOKEN_CARRY 436
#define TOKEN_RNI 437
#define TOKEN_RMI 438
#define TOKEN_RZI 439
#define TOKEN_RPI 440
#define TOKEN_FTZ 441
#define TOKEN_APPROX 442
#define TOKEN_FULL 443
#define TOKEN_SHIFT_AMOUNT 444
#define TOKEN_R 445
#define TOKEN_G 446
#define TOKEN_B 447
#define TOKEN_A 448
#define TOKEN_TO 449
#define TOKEN_CALL_PROTOTYPE 450
#define TOKEN_CALL_TARGETS 451
#define TOKEN_V2 452
#define TOKEN_V4 453
#define TOKEN_X 454
#define TOKEN_Y 455
#define TOKEN_Z 456
#define TOKEN_W 457
#define TOKEN_ANY 458
#define TOKEN_ALL 459
#define TOKEN_UP 460
#define TOKEN_DOWN 461
#define TOKEN_BFLY 462
#define TOKEN_IDX 463
#define TOKEN_MIN 464
#define TOKEN_MAX 465
#define TOKEN_DEC 466
#define TOKEN_INC 467
#define TOKEN_ADD 468
#define TOKEN_CAS 469
#define TOKEN_EXCH 470
#define TOKEN_1D 471
#define TOKEN_2D 472
#define TOKEN_3D 473
#define TOKEN_A1D 474
#define TOKEN_A2D 475
#define TOKEN_CUBE 476
#define TOKEN_ACUBE 477
#define TOKEN_CA 478
#define TOKEN_WB 479
#define TOKEN_CG 480
#define TOKEN_CS 481
#define TOKEN_LU 482
#define TOKEN_CV 483
#define TOKEN_WT 484
#define TOKEN_NC 485
#define TOKEN_L1 486
#define TOKEN_L2 487
#define TOKEN_P 488
#define TOKEN_WIDTH 489
#define TOKEN_DEPTH 490
#define TOKEN_HEIGHT 491
#define TOKEN_NORMALIZED_COORDS 492
#define TOKEN_FILTER_MODE 493
#define TOKEN_ADDR_MODE_0 494
#define TOKEN_ADDR_MODE_1 495
#define TOKEN_ADDR_MODE_2 496
#define TOKEN_CHANNEL_DATA_TYPE 497
#define TOKEN_CHANNEL_ORDER 498
#define TOKEN_TRAP 499
#define TOKEN_CLAMP 500
#define TOKEN_ZERO 501
#define TOKEN_ARRIVE 502
#define TOKEN_RED 503
#define TOKEN_POPC 504
#define TOKEN_SYNC 505
#define TOKEN_BALLOT 506
#define TOKEN_F4E 507
#define TOKEN_B4E 508
#define TOKEN_RC8 509
#define TOKEN_ECL 510
#define TOKEN_ECR 511
#define TOKEN_RC16 512
#define TOKEN_FINITE 513
#define TOKEN_INFINITE 514
#define TOKEN_NUMBER 515
#define TOKEN_NOT_A_NUMBER 516
#define TOKEN_NORMAL 517
#define TOKEN_SUBNORMAL 518
#define TOKEN_DECIMAL_CONSTANT 519
#define TOKEN_UNSIGNED_DECIMAL_CONSTANT 520
#define TOKEN_SINGLE_CONSTANT 521
#define TOKEN_DOUBLE_CONSTANT 522




/* Copy the first part of user declarations.  */
#line 7 "ocelot/parser/implementation/ptxgrammar.yy"

	#include <iostream>
	#include <ocelot/parser/interface/PTXParser.h>
	#include <ocelot/parser/interface/PTXLexer.h>
	#include <hydrazine/interface/debug.h>
	#include <cassert>
	#include <cstring>

	#define YYERROR_VERBOSE 1

	#ifdef REPORT_BASE
	#undef REPORT_BASE
	#endif

	#define REPORT_BASE 0

	namespace ptx
	{
	
	int yylex( YYSTYPE* token, YYLTYPE* location, parser::PTXLexer& lexer, 
		parser::PTXParser::State& state );
	void yyerror( YYLTYPE* location, parser::PTXLexer& lexer, 
		parser::PTXParser::State& state, char const* message );
	
	std::string yyTypeToString( int );
	


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 36 "ocelot/parser/implementation/ptxgrammar.yy"
{
	char text[1024];
	long long int value;
	long long unsigned int uvalue;

	double doubleFloat;
	float singleFloat;
}
/* Line 187 of yacc.c.  */
#line 667 ".release_build/ptxgrammar.cpp"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 692 ".release_build/ptxgrammar.cpp"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
	     && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
    YYLTYPE yyls;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  85
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   3267

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  285
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  271
/* YYNRULES -- Number of rules.  */
#define YYNRULES  710
/* YYNRULES -- Number of states.  */
#define YYNSTATES  1320

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   522

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint16 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   281,     2,     2,     2,     2,     2,     2,
     278,   279,     2,   282,   269,   283,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   280,   277,
     275,   274,   276,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   272,     2,   273,     2,   268,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   270,   284,   271,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,   234,
     235,   236,   237,   238,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     5,     7,     9,    11,    13,    15,    17,
      19,    21,    23,    25,    27,    29,    31,    33,    35,    37,
      39,    42,    44,    46,    48,    50,    52,    54,    56,    58,
      60,    63,    65,    67,    69,    70,    72,    74,    78,    80,
      84,    86,    87,    89,    93,   101,   109,   111,   115,   119,
     121,   123,   127,   135,   143,   145,   149,   153,   155,   157,
     161,   169,   177,   179,   183,   187,   189,   191,   193,   195,
     197,   199,   201,   203,   205,   207,   209,   211,   213,   215,
     217,   219,   220,   222,   226,   229,   232,   234,   236,   238,
     240,   242,   244,   246,   247,   249,   251,   253,   255,   257,
     259,   261,   263,   265,   267,   269,   271,   273,   275,   277,
     279,   281,   283,   285,   287,   289,   291,   293,   295,   297,
     298,   301,   303,   305,   307,   309,   312,   315,   318,   320,
     324,   328,   332,   336,   340,   344,   347,   350,   354,   358,
     363,   366,   367,   369,   370,   372,   375,   378,   381,   383,
     388,   390,   394,   397,   399,   404,   405,   410,   415,   422,
     429,   436,   438,   440,   446,   452,   458,   460,   462,   467,
     469,   471,   473,   475,   477,   480,   482,   483,   487,   491,
     495,   497,   498,   500,   502,   504,   505,   512,   518,   523,
     527,   529,   530,   534,   539,   543,   546,   548,   550,   552,
     554,   556,   558,   560,   562,   566,   570,   572,   575,   578,
     581,   586,   593,   597,   599,   603,   606,   609,   612,   615,
     617,   619,   621,   623,   625,   628,   629,   631,   633,   635,
     637,   638,   640,   642,   644,   646,   648,   651,   654,   656,
     658,   660,   662,   664,   666,   668,   670,   672,   674,   676,
     678,   680,   682,   684,   686,   688,   690,   692,   694,   696,
     698,   700,   702,   704,   706,   708,   710,   712,   714,   716,
     718,   720,   722,   724,   726,   728,   730,   732,   734,   736,
     738,   740,   742,   744,   746,   748,   750,   752,   754,   756,
     758,   760,   762,   764,   766,   768,   770,   772,   774,   776,
     778,   780,   782,   784,   786,   788,   790,   792,   794,   796,
     802,   807,   810,   812,   816,   818,   822,   826,   829,   830,
     831,   833,   839,   841,   845,   849,   852,   859,   864,   867,
     871,   873,   875,   877,   879,   881,   882,   885,   888,   890,
     892,   894,   896,   898,   902,   906,   908,   910,   912,   914,
     916,   918,   920,   922,   926,   928,   930,   931,   933,   935,
     937,   939,   941,   943,   945,   947,   949,   951,   953,   954,
     956,   958,   960,   962,   964,   966,   968,   970,   972,   974,
     976,   978,   980,   982,   984,   986,   988,   990,   992,   994,
     996,   998,  1000,  1002,  1004,  1006,  1008,  1010,  1012,  1014,
    1016,  1018,  1020,  1022,  1024,  1026,  1028,  1030,  1032,  1034,
    1036,  1038,  1040,  1042,  1044,  1046,  1048,  1050,  1052,  1054,
    1056,  1058,  1060,  1062,  1064,  1066,  1068,  1070,  1072,  1074,
    1076,  1078,  1087,  1089,  1091,  1093,  1095,  1097,  1099,  1108,
    1110,  1112,  1113,  1115,  1117,  1118,  1120,  1122,  1130,  1132,
    1134,  1144,  1145,  1147,  1152,  1154,  1156,  1160,  1165,  1166,
    1168,  1172,  1179,  1185,  1188,  1193,  1197,  1198,  1200,  1202,
    1204,  1211,  1213,  1214,  1216,  1220,  1222,  1224,  1234,  1236,
    1238,  1240,  1250,  1252,  1254,  1256,  1258,  1260,  1262,  1264,
    1266,  1268,  1270,  1272,  1274,  1275,  1288,  1303,  1305,  1306,
    1317,  1330,  1338,  1340,  1342,  1344,  1347,  1348,  1351,  1352,
    1358,  1365,  1368,  1375,  1377,  1379,  1381,  1383,  1387,  1390,
    1393,  1395,  1404,  1406,  1408,  1416,  1425,  1428,  1431,  1434,
    1435,  1437,  1439,  1441,  1451,  1454,  1461,  1463,  1465,  1466,
    1471,  1481,  1491,  1493,  1495,  1497,  1499,  1501,  1505,  1508,
    1511,  1513,  1515,  1527,  1529,  1532,  1544,  1547,  1559,  1561,
    1563,  1565,  1567,  1571,  1576,  1578,  1580,  1582,  1589,  1590,
    1592,  1602,  1612,  1614,  1616,  1623,  1626,  1633,  1635,  1637,
    1639,  1641,  1643,  1645,  1647,  1648,  1650,  1652,  1660,  1667,
    1679,  1682,  1683,  1686,  1688,  1690,  1698,  1700,  1702,  1704,
    1706,  1708,  1710,  1712,  1714,  1716,  1725,  1729,  1734,  1736,
    1738,  1740,  1742,  1744,  1746,  1748,  1750,  1752,  1754,  1756,
    1758,  1760,  1762,  1764,  1766,  1768,  1770,  1772,  1774,  1776,
    1778,  1780,  1791,  1802,  1805,  1808,  1819,  1834,  1837,  1840,
    1844,  1846,  1856,  1866,  1880,  1882,  1884,  1886,  1888,  1890,
    1902,  1915,  1925,  1927,  1929,  1931,  1933,  1935,  1937,  1939,
    1941,  1943,  1945,  1947,  1949,  1951,  1953,  1955,  1963,  1977,
    1979,  1981,  1983,  1985,  1987,  2002,  2004,  2006,  2008,  2010,
    2012,  2014,  2016,  2018,  2020,  2022,  2032,  2042,  2044,  2046,
    2048,  2050,  2052,  2054,  2055,  2057,  2059,  2061,  2063,  2065,
    2081,  2097,  2112,  2115,  2117,  2119,  2121,  2123,  2125,  2127,
    2129
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     289,     0,    -1,   292,    -1,   312,    -1,   336,    -1,   338,
      -1,   291,    -1,   344,    -1,   345,    -1,   343,    -1,   339,
      -1,   341,    -1,   313,    -1,   286,    -1,   340,    -1,   287,
      -1,   367,    -1,   363,    -1,   361,    -1,   288,    -1,   289,
     288,    -1,    83,    -1,    84,    -1,    85,    -1,    86,    -1,
      87,    -1,    88,    -1,    89,    -1,    90,    -1,   290,    -1,
     100,   267,    -1,   268,    -1,     4,    -1,   385,    -1,    -1,
     293,    -1,   293,    -1,   295,   269,   293,    -1,   293,    -1,
     296,   269,   293,    -1,   264,    -1,    -1,     6,    -1,   296,
     269,   264,    -1,   270,   298,   271,   269,   270,   298,   271,
      -1,   270,   296,   271,   269,   270,   296,   271,    -1,   298,
      -1,   270,   298,   271,    -1,   270,   296,   271,    -1,   296,
      -1,   267,    -1,   300,   269,   267,    -1,   270,   301,   271,
     269,   270,   301,   271,    -1,   270,   300,   271,   269,   270,
     300,   271,    -1,   301,    -1,   270,   301,   271,    -1,   270,
     300,   271,    -1,   300,    -1,   266,    -1,   303,   269,   266,
      -1,   270,   304,   271,   269,   270,   304,   271,    -1,   270,
     303,   271,   269,   270,   303,   271,    -1,   304,    -1,   270,
     304,   271,    -1,   270,   303,   271,    -1,   303,    -1,   114,
      -1,   108,    -1,   109,    -1,   110,    -1,   111,    -1,   113,
      -1,   115,    -1,   116,    -1,   112,    -1,   117,    -1,   118,
      -1,   306,    -1,   307,    -1,   308,    -1,   309,    -1,    -1,
     310,    -1,   311,   269,   310,    -1,    99,   311,    -1,   102,
     264,    -1,   119,    -1,   120,    -1,   121,    -1,   122,    -1,
     126,    -1,   314,    -1,   315,    -1,    -1,   140,    -1,   133,
      -1,   138,    -1,   139,    -1,   133,    -1,   140,    -1,   135,
      -1,   136,    -1,   134,    -1,   137,    -1,   141,    -1,   142,
      -1,   143,    -1,   144,    -1,   145,    -1,   147,    -1,   146,
      -1,   148,    -1,   318,    -1,   317,    -1,   197,    -1,   198,
      -1,   321,    -1,   321,    -1,   323,    -1,    -1,   178,   264,
      -1,   120,    -1,   119,    -1,   121,    -1,   126,    -1,   124,
     326,    -1,   319,   322,    -1,   322,   319,    -1,   319,    -1,
     325,   319,   322,    -1,   325,   322,   319,    -1,   319,   325,
     322,    -1,   319,   322,   325,    -1,   322,   319,   325,    -1,
     322,   325,   319,    -1,   319,   325,    -1,   325,   319,    -1,
     319,   327,   325,    -1,   272,   264,   273,    -1,   329,   272,
     264,   273,    -1,   272,   273,    -1,    -1,   329,    -1,    -1,
     274,    -1,   332,   299,    -1,   332,   302,    -1,   332,   305,
      -1,   293,    -1,   293,   275,   264,   276,    -1,   269,    -1,
     333,   334,   293,    -1,   322,   319,    -1,   319,    -1,   125,
     335,   333,   277,    -1,    -1,   269,   264,   269,   264,    -1,
      93,   264,     5,   337,    -1,   380,   126,   328,   293,   330,
     277,    -1,   384,   328,   293,   330,   331,   277,    -1,   380,
     121,   328,   293,   330,   277,    -1,   122,    -1,   120,    -1,
     380,   342,   127,   293,   277,    -1,   380,   342,   132,   293,
     277,    -1,   380,   342,   129,   293,   277,    -1,   122,    -1,
     125,    -1,   346,   328,   293,   330,    -1,   278,    -1,   279,
      -1,   278,    -1,   279,    -1,   270,    -1,   271,   297,    -1,
     347,    -1,    -1,   354,   269,   347,    -1,   348,   354,   349,
      -1,   350,   354,   351,    -1,   355,    -1,    -1,    96,    -1,
     293,    -1,   277,    -1,    -1,   380,   358,   357,   359,   356,
     360,    -1,   380,   358,   357,   359,   356,    -1,   362,   352,
     370,   353,    -1,   380,    91,   293,    -1,   356,    -1,    -1,
     364,   365,   379,    -1,   366,   352,   370,   353,    -1,   366,
     352,   353,    -1,   366,   277,    -1,   336,    -1,   387,    -1,
     388,    -1,   399,    -1,   397,    -1,   398,    -1,   386,    -1,
     368,    -1,   411,   417,   297,    -1,   352,   370,   353,    -1,
     369,    -1,   370,   369,    -1,   104,   264,    -1,   105,   264,
      -1,   105,   264,   269,   264,    -1,   105,   264,   269,   264,
     269,   264,    -1,   306,   280,   264,    -1,   373,    -1,   374,
     269,   373,    -1,   107,   264,    -1,   107,   374,    -1,   106,
     264,    -1,   106,   374,    -1,   371,    -1,   372,    -1,   376,
      -1,   375,    -1,   377,    -1,   378,   377,    -1,    -1,   378,
      -1,   103,    -1,    92,    -1,    94,    -1,    -1,   121,    -1,
     126,    -1,   122,    -1,   119,    -1,   120,    -1,   380,   381,
      -1,   380,   382,    -1,    10,    -1,    11,    -1,    12,    -1,
      13,    -1,    19,    -1,    14,    -1,    15,    -1,    16,    -1,
      17,    -1,    18,    -1,    20,    -1,    21,    -1,    22,    -1,
      23,    -1,    24,    -1,    25,    -1,    26,    -1,    27,    -1,
      28,    -1,    29,    -1,    30,    -1,    31,    -1,    32,    -1,
      33,    -1,    34,    -1,    35,    -1,    36,    -1,    37,    -1,
       9,    -1,    82,    -1,    38,    -1,    42,    -1,    43,    -1,
      39,    -1,    40,    -1,    41,    -1,    50,    -1,    51,    -1,
      52,    -1,    53,    -1,    54,    -1,    55,    -1,    56,    -1,
      57,    -1,    58,    -1,    44,    -1,    59,    -1,    45,    -1,
      46,    -1,    47,    -1,    48,    -1,    49,    -1,    60,    -1,
      61,    -1,    62,    -1,    63,    -1,    64,    -1,    65,    -1,
      66,    -1,    68,    -1,    67,    -1,    69,    -1,    70,    -1,
      72,    -1,    73,    -1,    74,    -1,    75,    -1,    77,    -1,
      78,    -1,    80,    -1,    81,    -1,   383,   328,   293,   330,
     277,    -1,    95,   264,   264,   264,    -1,     3,   297,    -1,
     293,    -1,   346,   318,   294,    -1,   390,    -1,   391,   269,
     390,    -1,   278,   391,   279,    -1,   278,   279,    -1,    -1,
      -1,   325,    -1,   346,   393,   318,   294,   330,    -1,   394,
      -1,   395,   269,   394,    -1,   278,   395,   279,    -1,   278,
     279,    -1,     3,   195,   392,   293,   396,   277,    -1,     3,
     196,   295,   277,    -1,   123,     5,    -1,   123,     5,   277,
      -1,   199,    -1,   200,    -1,   201,    -1,   202,    -1,   400,
      -1,    -1,   293,   401,    -1,   281,   293,    -1,   264,    -1,
     265,    -1,   267,    -1,   266,    -1,   293,    -1,   293,   282,
     264,    -1,   293,   283,   264,    -1,   407,    -1,   403,    -1,
     402,    -1,   403,    -1,   404,    -1,   405,    -1,   389,    -1,
     407,    -1,   270,   295,   271,    -1,     8,    -1,     7,    -1,
      -1,   170,    -1,   171,    -1,   173,    -1,   172,    -1,   182,
      -1,   183,    -1,   185,    -1,   184,    -1,   412,    -1,   413,
      -1,   414,    -1,    -1,   428,    -1,   430,    -1,   422,    -1,
     419,    -1,   452,    -1,   453,    -1,   454,    -1,   459,    -1,
     432,    -1,   443,    -1,   446,    -1,   450,    -1,   458,    -1,
     460,    -1,   461,    -1,   466,    -1,   468,    -1,   475,    -1,
     473,    -1,   474,    -1,   479,    -1,   480,    -1,   485,    -1,
     487,    -1,   489,    -1,   492,    -1,   495,    -1,   497,    -1,
     498,    -1,   500,    -1,   501,    -1,   502,    -1,   506,    -1,
     507,    -1,   508,    -1,   511,    -1,   514,    -1,   515,    -1,
     520,    -1,   521,    -1,   523,    -1,   526,    -1,   530,    -1,
     531,    -1,   548,    -1,   543,    -1,   550,    -1,   549,    -1,
     536,    -1,   537,    -1,   540,    -1,   551,    -1,   542,    -1,
     555,    -1,   529,    -1,    39,    -1,    41,    -1,    22,    -1,
      66,    -1,    65,    -1,    40,    -1,     9,    -1,   418,   319,
     407,   269,   407,   269,   407,   277,    -1,    13,    -1,    21,
      -1,    10,    -1,    18,    -1,    17,    -1,   187,    -1,   420,
     421,   424,   319,   407,   269,   407,   277,    -1,   186,    -1,
     423,    -1,    -1,   174,    -1,   425,    -1,    -1,    26,    -1,
      27,    -1,   427,   424,   319,   407,   269,   407,   277,    -1,
      29,    -1,    28,    -1,   429,   424,   319,   407,   269,   407,
     269,   407,   277,    -1,    -1,   177,    -1,    50,   431,   409,
     277,    -1,   406,    -1,   433,    -1,   434,   269,   433,    -1,
     278,   434,   279,   269,    -1,    -1,   406,    -1,   436,   269,
     406,    -1,   269,   278,   436,   279,   269,   293,    -1,   269,
     278,   279,   269,   293,    -1,   269,   293,    -1,   269,   278,
     436,   279,    -1,   269,   278,   279,    -1,    -1,   176,    -1,
     431,    -1,   439,    -1,    51,   438,   435,   293,   437,   277,
      -1,   181,    -1,    -1,   181,    -1,   416,   424,   426,    -1,
      12,    -1,    16,    -1,   442,   441,   319,   407,   269,   407,
     269,   407,   277,    -1,    19,    -1,    56,    -1,   440,    -1,
     444,   445,   319,   407,   269,   407,   269,   407,   277,    -1,
     167,    -1,   168,    -1,   169,    -1,   214,    -1,   215,    -1,
     213,    -1,   212,    -1,   211,    -1,   209,    -1,   210,    -1,
     447,    -1,   315,    -1,    -1,    60,   449,   448,   319,   407,
     269,   272,   408,   273,   269,   407,   277,    -1,    60,   449,
     448,   319,   407,   269,   272,   408,   273,   269,   407,   269,
     407,   277,    -1,   189,    -1,    -1,    76,   319,   407,   269,
     407,   269,   407,   269,   407,   277,    -1,    75,   319,   407,
     269,   407,   269,   407,   269,   407,   269,   407,   277,    -1,
      73,   451,   319,   407,   269,   407,   277,    -1,   247,    -1,
     248,    -1,   250,    -1,   513,   319,    -1,    -1,   407,   457,
      -1,    -1,    79,   455,   456,   457,   277,    -1,    74,   319,
     407,   269,   407,   277,    -1,    55,   277,    -1,    72,   319,
     407,   269,   407,   277,    -1,   414,    -1,   415,    -1,   463,
      -1,   462,    -1,   464,   424,   425,    -1,   464,   424,    -1,
     424,   425,    -1,   424,    -1,    38,   465,   319,   319,   407,
     269,   407,   277,    -1,   407,    -1,   405,    -1,    42,   315,
     320,   407,   269,   467,   277,    -1,    42,   194,   315,   320,
     407,   269,   467,   277,    -1,   188,   424,    -1,   187,   424,
      -1,   170,   424,    -1,    -1,   469,    -1,   470,    -1,   471,
      -1,    25,   472,   319,   407,   269,   407,   269,   407,   277,
      -1,    53,   277,    -1,    43,   315,   407,   269,   407,   277,
      -1,   175,    -1,   476,    -1,    -1,   477,   316,   545,   324,
      -1,    58,   478,   319,   410,   269,   272,   408,   273,   277,
      -1,    44,   478,   319,   410,   269,   272,   408,   273,   277,
      -1,   165,    -1,   166,    -1,   414,    -1,   481,    -1,   180,
      -1,   482,   424,   426,    -1,   481,   181,    -1,   424,   426,
      -1,    30,    -1,    67,    -1,   484,   483,   319,   407,   269,
     407,   269,   407,   269,   407,   277,    -1,   426,    -1,   481,
     426,    -1,    24,   486,   319,   407,   269,   407,   269,   407,
     269,   407,   277,    -1,   481,   440,    -1,    31,   488,   319,
     407,   269,   407,   269,   407,   269,   407,   277,    -1,   130,
      -1,   128,    -1,   131,    -1,   490,    -1,    68,   491,   277,
      -1,   293,   272,   264,   273,    -1,   410,    -1,   405,    -1,
     493,    -1,    36,   319,   410,   269,   494,   277,    -1,    -1,
     481,    -1,    23,   496,   319,   407,   269,   407,   269,   407,
     277,    -1,    14,   483,   319,   407,   269,   407,   269,   407,
     277,    -1,    63,    -1,    62,    -1,   499,   319,   407,   269,
     407,   277,    -1,    69,   407,    -1,    70,   319,   407,   269,
     407,   277,    -1,   252,    -1,   253,    -1,   254,    -1,   255,
      -1,   256,    -1,   257,    -1,   503,    -1,    -1,   231,    -1,
     232,    -1,    80,   315,   505,   272,   408,   273,   277,    -1,
      81,   505,   272,   408,   273,   277,    -1,    71,   319,   504,
     407,   269,   407,   269,   407,   269,   407,   277,    -1,   187,
     424,    -1,    -1,   170,   424,    -1,    20,    -1,    11,    -1,
     510,   509,   319,   407,   269,   407,   277,    -1,   167,    -1,
     169,    -1,   168,    -1,   213,    -1,   212,    -1,   211,    -1,
     209,    -1,   210,    -1,   512,    -1,    61,   315,   513,   319,
     407,   269,   407,   277,    -1,    52,   431,   277,    -1,    52,
     431,   407,   277,    -1,   149,    -1,   150,    -1,   151,    -1,
     152,    -1,   153,    -1,   154,    -1,   155,    -1,   156,    -1,
     157,    -1,   158,    -1,   159,    -1,   160,    -1,   161,    -1,
     162,    -1,   163,    -1,   164,    -1,   166,    -1,   165,    -1,
     516,    -1,   167,    -1,   168,    -1,   169,    -1,   518,    -1,
      15,   319,   407,   269,   407,   269,   407,   269,   407,   277,
      -1,    34,   319,   407,   269,   407,   269,   407,   269,   407,
     277,    -1,   517,   424,    -1,   423,   517,    -1,    32,   522,
     319,   319,   407,   269,   407,   269,   407,   277,    -1,    32,
     517,   519,   424,   319,   319,   407,   269,   407,   269,   407,
     269,   407,   277,    -1,   517,   424,    -1,   423,   517,    -1,
     407,   284,   407,    -1,   407,    -1,    33,   524,   319,   525,
     269,   407,   269,   407,   277,    -1,    33,   319,   524,   525,
     269,   407,   269,   407,   277,    -1,    33,   517,   519,   424,
     319,   525,   269,   407,   269,   407,   269,   407,   277,    -1,
     205,    -1,   206,    -1,   207,    -1,   208,    -1,   527,    -1,
      82,   528,   319,   525,   269,   407,   269,   407,   269,   407,
     277,    -1,    35,   424,   319,   319,   407,   269,   407,   269,
     407,   269,   407,   277,    -1,    37,   478,   319,   272,   408,
     273,   269,   410,   277,    -1,   216,    -1,   217,    -1,   218,
      -1,   219,    -1,   220,    -1,   221,    -1,   222,    -1,   532,
      -1,   258,    -1,   259,    -1,   260,    -1,   261,    -1,   262,
      -1,   263,    -1,   534,    -1,    77,   535,   319,   407,   269,
     407,   277,    -1,    57,   533,   198,   319,   319,   410,   269,
     272,   407,   269,   410,   273,   277,    -1,   193,    -1,   192,
      -1,   190,    -1,   191,    -1,   538,    -1,    78,   539,   217,
     198,   319,   319,   410,   269,   272,   407,   269,   410,   273,
     277,    -1,   234,    -1,   236,    -1,   235,    -1,   242,    -1,
     243,    -1,   237,    -1,   238,    -1,   239,    -1,   240,    -1,
     241,    -1,    46,   541,   319,   407,   269,   272,   407,   273,
     277,    -1,    49,   541,   319,   407,   269,   272,   407,   273,
     277,    -1,   223,    -1,   225,    -1,   226,    -1,   228,    -1,
     230,    -1,   544,    -1,    -1,   245,    -1,   246,    -1,   244,
      -1,   192,    -1,   233,    -1,    45,   547,   533,   545,   323,
     319,   546,   410,   269,   272,   407,   269,   410,   273,   277,
      -1,    47,   547,   533,   545,   323,   319,   546,   272,   407,
     269,   410,   273,   269,   410,   277,    -1,    48,   547,   513,
     533,   319,   546,   272,   407,   269,   410,   273,   269,   410,
     277,    -1,    54,   277,    -1,   203,    -1,   204,    -1,   177,
      -1,   251,    -1,   552,    -1,   148,    -1,   143,    -1,    64,
     553,   554,   407,   269,   407,   277,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   151,   151,   151,   151,   151,   152,   152,   152,   152,
     153,   153,   153,   155,   160,   160,   160,   160,   161,   163,
     163,   165,   165,   166,   166,   166,   167,   167,   167,   169,
     174,   179,   179,   179,   180,   180,   182,   187,   192,   198,
     204,   210,   214,   219,   224,   225,   227,   227,   228,   228,
     230,   235,   240,   241,   243,   243,   243,   244,   246,   251,
     256,   257,   259,   259,   259,   260,   262,   262,   262,   262,
     262,   263,   263,   263,   265,   266,   266,   268,   268,   268,
     269,   274,   274,   275,   277,   282,   287,   287,   287,   288,
     288,   290,   295,   297,   301,   301,   303,   303,   303,   303,
     303,   304,   304,   304,   304,   304,   304,   305,   305,   305,
     305,   305,   307,   312,   317,   317,   319,   324,   329,   330,
     332,   334,   334,   334,   334,   335,   340,   341,   342,   343,
     344,   345,   346,   347,   348,   349,   350,   351,   353,   358,
     363,   369,   373,   375,   377,   382,   382,   383,   385,   390,
     395,   400,   405,   406,   408,   410,   411,   414,   420,   427,
     434,   441,   441,   443,   448,   453,   458,   463,   468,   475,
     480,   485,   490,   495,   500,   505,   506,   507,   509,   511,
     513,   513,   515,   520,   525,   526,   528,   534,   540,   542,
     547,   548,   550,   555,   557,   559,   564,   564,   564,   564,
     565,   565,   567,   569,   574,   580,   582,   583,   585,   590,
     595,   600,   606,   611,   611,   613,   618,   623,   628,   633,
     633,   633,   633,   634,   635,   636,   636,   638,   643,   648,
     654,   658,   658,   658,   659,   659,   661,   666,   671,   671,
     671,   671,   671,   672,   672,   672,   672,   672,   673,   673,
     673,   673,   673,   674,   674,   674,   674,   674,   675,   675,
     675,   675,   675,   676,   676,   676,   676,   676,   677,   677,
     677,   678,   678,   678,   679,   679,   679,   679,   679,   680,
     680,   680,   680,   680,   681,   681,   681,   681,   681,   682,
     682,   682,   682,   682,   683,   683,   683,   683,   683,   684,
     684,   684,   684,   684,   685,   685,   685,   686,   686,   688,
     695,   701,   706,   711,   716,   717,   718,   718,   718,   720,
     720,   722,   728,   729,   730,   730,   732,   738,   743,   748,
     753,   753,   753,   753,   755,   761,   765,   770,   775,   780,
     785,   790,   795,   800,   805,   810,   811,   811,   813,   813,
     813,   815,   817,   818,   823,   828,   834,   838,   838,   838,
     838,   839,   839,   839,   839,   841,   846,   851,   851,   853,
     853,   853,   854,   854,   854,   854,   854,   854,   854,   855,
     855,   855,   855,   855,   855,   855,   855,   855,   855,   856,
     856,   856,   856,   856,   856,   856,   856,   856,   856,   857,
     857,   857,   857,   857,   857,   857,   858,   858,   858,   858,
     858,   858,   858,   858,   858,   858,   858,   859,   859,   859,
     859,   859,   859,   859,   861,   861,   862,   862,   862,   862,
     862,   864,   870,   870,   870,   870,   871,   873,   878,   884,
     889,   889,   891,   896,   896,   898,   898,   900,   906,   906,
     908,   915,   919,   924,   929,   934,   935,   937,   937,   939,
     940,   942,   947,   952,   957,   958,   958,   960,   965,   970,
     972,   978,   984,   988,   993,   998,   998,  1000,  1006,  1006,
    1008,  1010,  1017,  1017,  1017,  1017,  1017,  1018,  1018,  1018,
    1018,  1018,  1020,  1025,  1027,  1031,  1037,  1043,  1049,  1053,
    1059,  1065,  1070,  1070,  1070,  1075,  1075,  1077,  1077,  1079,
    1084,  1089,  1094,  1099,  1104,  1109,  1109,  1111,  1112,  1113,
    1114,  1116,  1122,  1122,  1124,  1129,  1140,  1145,  1150,  1155,
    1157,  1157,  1158,  1160,  1165,  1170,  1175,  1180,  1183,  1187,
    1190,  1195,  1200,  1200,  1202,  1202,  1202,  1204,  1210,  1215,
    1220,  1220,  1222,  1228,  1230,  1235,  1241,  1246,  1252,  1252,
    1252,  1254,  1259,  1264,  1269,  1269,  1269,  1271,  1276,  1278,
    1283,  1288,  1293,  1293,  1295,  1300,  1305,  1310,  1310,  1310,
    1311,  1311,  1311,  1313,  1319,  1323,  1323,  1328,  1333,  1338,
    1344,  1349,  1350,  1355,  1355,  1357,  1363,  1363,  1363,  1363,
    1363,  1364,  1364,  1364,  1366,  1371,  1377,  1382,  1387,  1387,
    1387,  1387,  1387,  1387,  1388,  1388,  1388,  1388,  1388,  1388,
    1389,  1389,  1389,  1389,  1389,  1389,  1391,  1396,  1396,  1396,
    1398,  1403,  1408,  1416,  1417,  1419,  1426,  1434,  1435,  1437,
    1437,  1439,  1445,  1451,  1457,  1457,  1457,  1457,  1459,  1464,
    1470,  1477,  1482,  1482,  1482,  1482,  1482,  1483,  1483,  1485,
    1490,  1490,  1490,  1491,  1491,  1491,  1493,  1498,  1503,  1510,
    1510,  1510,  1510,  1512,  1517,  1527,  1527,  1527,  1528,  1528,
    1528,  1529,  1529,  1529,  1530,  1535,  1541,  1547,  1547,  1547,
    1547,  1547,  1552,  1552,  1554,  1554,  1554,  1559,  1559,  1564,
    1573,  1582,  1594,  1599,  1599,  1599,  1599,  1601,  1606,  1606,
    1608
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "TOKEN_LABEL", "TOKEN_IDENTIFIER",
  "TOKEN_STRING", "TOKEN_METADATA", "TOKEN_INV_PREDICATE_IDENTIFIER",
  "TOKEN_PREDICATE_IDENTIFIER", "OPCODE_COPYSIGN", "OPCODE_COS",
  "OPCODE_SQRT", "OPCODE_ADD", "OPCODE_RSQRT", "OPCODE_MUL", "OPCODE_SAD",
  "OPCODE_SUB", "OPCODE_EX2", "OPCODE_LG2", "OPCODE_ADDC", "OPCODE_RCP",
  "OPCODE_SIN", "OPCODE_REM", "OPCODE_MUL24", "OPCODE_MAD24", "OPCODE_DIV",
  "OPCODE_ABS", "OPCODE_NEG", "OPCODE_MIN", "OPCODE_MAX", "OPCODE_MAD",
  "OPCODE_MADC", "OPCODE_SET", "OPCODE_SETP", "OPCODE_SELP", "OPCODE_SLCT",
  "OPCODE_MOV", "OPCODE_ST", "OPCODE_CVT", "OPCODE_AND", "OPCODE_XOR",
  "OPCODE_OR", "OPCODE_CVTA", "OPCODE_ISSPACEP", "OPCODE_LDU",
  "OPCODE_SULD", "OPCODE_TXQ", "OPCODE_SUST", "OPCODE_SURED", "OPCODE_SUQ",
  "OPCODE_BRA", "OPCODE_CALL", "OPCODE_RET", "OPCODE_EXIT", "OPCODE_TRAP",
  "OPCODE_BRKPT", "OPCODE_SUBC", "OPCODE_TEX", "OPCODE_LD",
  "OPCODE_BARSYNC", "OPCODE_ATOM", "OPCODE_RED", "OPCODE_NOT",
  "OPCODE_CNOT", "OPCODE_VOTE", "OPCODE_SHR", "OPCODE_SHL", "OPCODE_FMA",
  "OPCODE_MEMBAR", "OPCODE_PMEVENT", "OPCODE_POPC", "OPCODE_PRMT",
  "OPCODE_CLZ", "OPCODE_BFIND", "OPCODE_BREV", "OPCODE_BFI", "OPCODE_BFE",
  "OPCODE_TESTP", "OPCODE_TLD4", "OPCODE_BAR", "OPCODE_PREFETCH",
  "OPCODE_PREFETCHU", "OPCODE_SHFL", "PREPROCESSOR_INCLUDE",
  "PREPROCESSOR_DEFINE", "PREPROCESSOR_IF", "PREPROCESSOR_IFDEF",
  "PREPROCESSOR_ELSE", "PREPROCESSOR_ENDIF", "PREPROCESSOR_LINE",
  "PREPROCESSOR_FILE", "TOKEN_ENTRY", "TOKEN_EXTERN", "TOKEN_FILE",
  "TOKEN_VISIBLE", "TOKEN_LOC", "TOKEN_FUNCTION", "TOKEN_STRUCT",
  "TOKEN_UNION", "TOKEN_TARGET", "TOKEN_VERSION", "TOKEN_SECTION",
  "TOKEN_ADDRESS_SIZE", "TOKEN_WEAK", "TOKEN_MAXNREG", "TOKEN_MAXNTID",
  "TOKEN_MAXNCTAPERSM", "TOKEN_MINNCTAPERSM", "TOKEN_SM11", "TOKEN_SM12",
  "TOKEN_SM13", "TOKEN_SM20", "TOKEN_MAP_F64_TO_F32", "TOKEN_SM21",
  "TOKEN_SM10", "TOKEN_SM30", "TOKEN_SM35", "TOKEN_TEXMODE_INDEPENDENT",
  "TOKEN_TEXMODE_UNIFIED", "TOKEN_CONST", "TOKEN_GLOBAL", "TOKEN_LOCAL",
  "TOKEN_PARAM", "TOKEN_PRAGMA", "TOKEN_PTR", "TOKEN_REG", "TOKEN_SHARED",
  "TOKEN_TEXREF", "TOKEN_CTA", "TOKEN_SURFREF", "TOKEN_GL", "TOKEN_SYS",
  "TOKEN_SAMPLERREF", "TOKEN_U32", "TOKEN_S32", "TOKEN_S8", "TOKEN_S16",
  "TOKEN_S64", "TOKEN_U8", "TOKEN_U16", "TOKEN_U64", "TOKEN_B8",
  "TOKEN_B16", "TOKEN_B32", "TOKEN_B64", "TOKEN_F16", "TOKEN_F64",
  "TOKEN_F32", "TOKEN_PRED", "TOKEN_EQ", "TOKEN_NE", "TOKEN_LT",
  "TOKEN_LE", "TOKEN_GT", "TOKEN_GE", "TOKEN_LS", "TOKEN_HS", "TOKEN_EQU",
  "TOKEN_NEU", "TOKEN_LTU", "TOKEN_LEU", "TOKEN_GTU", "TOKEN_GEU",
  "TOKEN_NUM", "TOKEN_NAN", "TOKEN_HI", "TOKEN_LO", "TOKEN_AND",
  "TOKEN_OR", "TOKEN_XOR", "TOKEN_RN", "TOKEN_RM", "TOKEN_RZ", "TOKEN_RP",
  "TOKEN_SAT", "TOKEN_VOLATILE", "TOKEN_TAIL", "TOKEN_UNI", "TOKEN_ALIGN",
  "TOKEN_BYTE", "TOKEN_WIDE", "TOKEN_CARRY", "TOKEN_RNI", "TOKEN_RMI",
  "TOKEN_RZI", "TOKEN_RPI", "TOKEN_FTZ", "TOKEN_APPROX", "TOKEN_FULL",
  "TOKEN_SHIFT_AMOUNT", "TOKEN_R", "TOKEN_G", "TOKEN_B", "TOKEN_A",
  "TOKEN_TO", "TOKEN_CALL_PROTOTYPE", "TOKEN_CALL_TARGETS", "TOKEN_V2",
  "TOKEN_V4", "TOKEN_X", "TOKEN_Y", "TOKEN_Z", "TOKEN_W", "TOKEN_ANY",
  "TOKEN_ALL", "TOKEN_UP", "TOKEN_DOWN", "TOKEN_BFLY", "TOKEN_IDX",
  "TOKEN_MIN", "TOKEN_MAX", "TOKEN_DEC", "TOKEN_INC", "TOKEN_ADD",
  "TOKEN_CAS", "TOKEN_EXCH", "TOKEN_1D", "TOKEN_2D", "TOKEN_3D",
  "TOKEN_A1D", "TOKEN_A2D", "TOKEN_CUBE", "TOKEN_ACUBE", "TOKEN_CA",
  "TOKEN_WB", "TOKEN_CG", "TOKEN_CS", "TOKEN_LU", "TOKEN_CV", "TOKEN_WT",
  "TOKEN_NC", "TOKEN_L1", "TOKEN_L2", "TOKEN_P", "TOKEN_WIDTH",
  "TOKEN_DEPTH", "TOKEN_HEIGHT", "TOKEN_NORMALIZED_COORDS",
  "TOKEN_FILTER_MODE", "TOKEN_ADDR_MODE_0", "TOKEN_ADDR_MODE_1",
  "TOKEN_ADDR_MODE_2", "TOKEN_CHANNEL_DATA_TYPE", "TOKEN_CHANNEL_ORDER",
  "TOKEN_TRAP", "TOKEN_CLAMP", "TOKEN_ZERO", "TOKEN_ARRIVE", "TOKEN_RED",
  "TOKEN_POPC", "TOKEN_SYNC", "TOKEN_BALLOT", "TOKEN_F4E", "TOKEN_B4E",
  "TOKEN_RC8", "TOKEN_ECL", "TOKEN_ECR", "TOKEN_RC16", "TOKEN_FINITE",
  "TOKEN_INFINITE", "TOKEN_NUMBER", "TOKEN_NOT_A_NUMBER", "TOKEN_NORMAL",
  "TOKEN_SUBNORMAL", "TOKEN_DECIMAL_CONSTANT",
  "TOKEN_UNSIGNED_DECIMAL_CONSTANT", "TOKEN_SINGLE_CONSTANT",
  "TOKEN_DOUBLE_CONSTANT", "'_'", "','", "'{'", "'}'", "'['", "']'", "'='",
  "'<'", "'>'", "';'", "'('", "')'", "':'", "'!'", "'+'", "'-'", "'|'",
  "$accept", "nonEntryStatements", "nonEntryStatement", "statement",
  "statements", "preprocessorCommand", "preprocessor", "version",
  "identifier", "optionalIdentifier", "identifierList",
  "decimalListSingle", "optionalMetadata", "decimalList",
  "decimalInitializer", "floatListSingle", "floatList", "floatInitializer",
  "singleListSingle", "singleList", "singleInitializer", "shaderModel",
  "floatingPointOption", "textureOption", "targetOption", "targetElement",
  "targetElementList", "target", "addressSize", "addressSpaceIdentifier",
  "addressSpace", "optionalAddressSpace", "pointerDataTypeId",
  "dataTypeId", "dataType", "pointerDataType", "vectorToken",
  "statementVectorType", "instructionVectorType",
  "optionalInstructionVectorType", "alignment", "kernelParameterPtrSpace",
  "parameterAttribute", "addressableVariablePrefix", "arrayDimensionSet",
  "arrayDimensions", "initializer", "assignment", "registerIdentifierList",
  "registerSeperator", "registerPrefix", "registerDeclaration",
  "optionalTimestampAndSize", "fileDeclaration", "globalSharedDeclaration",
  "initializableDeclaration", "globalLocalDeclaration", "textureSpace",
  "textureDeclaration", "samplerDeclaration", "surfaceDeclaration",
  "parameter", "argumentDeclaration", "returnArgumentListBegin",
  "returnArgumentListEnd", "argumentListBegin", "argumentListEnd",
  "openBrace", "closeBrace", "argumentListBody", "returnArgumentList",
  "argumentList", "optionalReturnArgumentList", "functionBegin",
  "functionName", "optionalSemicolon", "functionDeclaration",
  "functionBodyDefinition", "functionBody", "entryName",
  "optionalArgumentList", "entryDeclaration", "entry", "entryStatement",
  "completeEntryStatement", "entryStatements", "maxnreg", "maxntid",
  "ctapersm", "ctapersmList", "minnctapersm", "maxnctapersm",
  "performanceDirective", "performanceDirectiveList",
  "performanceDirectives", "externOrVisible", "uninitializableAddress",
  "initializableAddress", "uninitializable", "initializable", "opcode",
  "uninitializableDeclaration", "location", "label", "labelOperand",
  "returnType", "returnTypeListBody", "returnTypeList",
  "optionalAlignment", "argumentType", "argumentTypeListBody",
  "argumentTypeList", "callprototype", "calltargets", "pragma",
  "vectorIndex", "optionalVectorIndex", "nonLabelOperand",
  "constantOperand", "addressableOperand", "offsetAddressableOperand",
  "callOperand", "operand", "memoryOperand", "branchOperand",
  "arrayOperand", "guard", "floatRoundingToken", "intRoundingToken",
  "floatRounding", "intRounding", "optionalFloatRounding", "instruction",
  "basicInstruction3Opcode", "basicInstruction3",
  "approxInstruction2Opcode", "approximate", "approxInstruction2", "ftz",
  "optionalFtz", "sat", "optionalSaturate", "ftzInstruction2Opcode",
  "ftzInstruction2", "ftzInstruction3Opcode", "ftzInstruction3",
  "optionalUni", "branch", "returnOperand", "returnOperandList",
  "optionalReturnOperandList", "callArgumentList", "optionalPrototypeName",
  "optionalUniOrTail", "call", "optionalCarry", "addModifier",
  "addOrSubOpcode", "addOrSub", "addCOrSubCOpcode", "addCModifier",
  "addCOrSubC", "atomicOperationId", "atomicOperation", "atomModifier",
  "atom", "shiftAmount", "bfe", "bfi", "bfind", "barrierOperation",
  "optionalBarrierOperator", "operandSequence", "bar", "brev", "brkpt",
  "clz", "floatRoundingModifier", "intRoundingModifier",
  "cvtRoundingModifier", "cvtModifier", "cvt", "cvtaOperand", "cvta",
  "divFullModifier", "divApproxModifier", "divRnModifier", "divModifier",
  "div", "exit", "isspacep", "volatileModifier", "optionalVolatile",
  "ldModifier", "ld", "ldu", "hiOrLo", "roundHiLoWide", "mulModifier",
  "madOpcode", "mad", "mad24Modifier", "mad24", "madCModifier", "madc",
  "membarSpaceType", "membarSpace", "membar", "movIndexedOperand",
  "movSourceOperand", "mov", "mul24Modifier", "mul24", "mul", "notOpcode",
  "notInstruction", "pmevent", "popc", "permuteModeType", "permuteMode",
  "cacheLevel", "prefetch", "prefetchu", "prmt", "rcpSqrtModifier",
  "rcpSqrtOpcode", "rcpSqrtInstruction", "reductionOperationId",
  "reductionOperation", "red", "ret", "comparisonId", "comparison",
  "boolOperatorId", "boolOperator", "sad", "selp", "setModifier", "set",
  "setpModifier", "predicatePair", "setp", "shuffleModifierId",
  "shuffleModifier", "shfl", "slct", "st", "geometryId", "geometry",
  "floatingPointModeType", "floatingPointMode", "testp", "tex",
  "colorComponentId", "colorComponent", "tld4", "surfaceQuery", "txq",
  "suq", "cacheOperation", "optionalCacheOperation", "clampOperation",
  "formatMode", "suld", "sust", "sured", "trap", "voteOperationId",
  "voteOperation", "voteDataType", "vote", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,   460,   461,   462,   463,   464,
     465,   466,   467,   468,   469,   470,   471,   472,   473,   474,
     475,   476,   477,   478,   479,   480,   481,   482,   483,   484,
     485,   486,   487,   488,   489,   490,   491,   492,   493,   494,
     495,   496,   497,   498,   499,   500,   501,   502,   503,   504,
     505,   506,   507,   508,   509,   510,   511,   512,   513,   514,
     515,   516,   517,   518,   519,   520,   521,   522,    95,    44,
     123,   125,    91,    93,    61,    60,    62,    59,    40,    41,
      58,    33,    43,    45,   124
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   285,   286,   286,   286,   286,   286,   286,   286,   286,
     286,   286,   286,   287,   288,   288,   288,   288,   288,   289,
     289,   290,   290,   290,   290,   290,   290,   290,   290,   291,
     292,   293,   293,   293,   294,   294,   295,   295,   296,   296,
     296,   297,   297,   296,   298,   298,   299,   299,   299,   299,
     300,   300,   301,   301,   302,   302,   302,   302,   303,   303,
     304,   304,   305,   305,   305,   305,   306,   306,   306,   306,
     306,   306,   306,   306,   307,   308,   308,   309,   309,   309,
     310,   311,   311,   311,   312,   313,   314,   314,   314,   314,
     314,   315,   316,   316,   317,   317,   318,   318,   318,   318,
     318,   318,   318,   318,   318,   318,   318,   318,   318,   318,
     318,   318,   319,   320,   321,   321,   322,   323,   324,   324,
     325,   326,   326,   326,   326,   327,   328,   328,   328,   328,
     328,   328,   328,   328,   328,   328,   328,   328,   329,   329,
     329,   330,   330,   331,   332,   331,   331,   331,   333,   333,
     334,   333,   335,   335,   336,   337,   337,   338,   339,   340,
     341,   342,   342,   343,   344,   345,   346,   346,   347,   348,
     349,   350,   351,   352,   353,   354,   354,   354,   355,   356,
     357,   357,   358,   359,   360,   360,   361,   362,   363,   364,
     365,   365,   366,   367,   367,   367,   368,   368,   368,   368,
     368,   368,   369,   369,   369,   369,   370,   370,   371,   372,
     372,   372,   373,   374,   374,   375,   375,   376,   376,   377,
     377,   377,   377,   378,   378,   379,   379,   380,   380,   380,
     380,   381,   381,   381,   382,   382,   383,   384,   385,   385,
     385,   385,   385,   385,   385,   385,   385,   385,   385,   385,
     385,   385,   385,   385,   385,   385,   385,   385,   385,   385,
     385,   385,   385,   385,   385,   385,   385,   385,   385,   385,
     385,   385,   385,   385,   385,   385,   385,   385,   385,   385,
     385,   385,   385,   385,   385,   385,   385,   385,   385,   385,
     385,   385,   385,   385,   385,   385,   385,   385,   385,   385,
     385,   385,   385,   385,   385,   385,   385,   385,   385,   386,
     387,   388,   389,   390,   391,   391,   392,   392,   392,   393,
     393,   394,   395,   395,   396,   396,   397,   398,   399,   399,
     400,   400,   400,   400,   401,   401,   402,   402,   403,   403,
     403,   403,   404,   405,   405,   406,   407,   407,   408,   408,
     408,   409,   410,   410,   411,   411,   411,   412,   412,   412,
     412,   413,   413,   413,   413,   414,   415,   416,   416,   417,
     417,   417,   417,   417,   417,   417,   417,   417,   417,   417,
     417,   417,   417,   417,   417,   417,   417,   417,   417,   417,
     417,   417,   417,   417,   417,   417,   417,   417,   417,   417,
     417,   417,   417,   417,   417,   417,   417,   417,   417,   417,
     417,   417,   417,   417,   417,   417,   417,   417,   417,   417,
     417,   417,   417,   417,   418,   418,   418,   418,   418,   418,
     418,   419,   420,   420,   420,   420,   420,   421,   422,   423,
     424,   424,   425,   426,   426,   427,   427,   428,   429,   429,
     430,   431,   431,   432,   433,   434,   434,   435,   435,   436,
     436,   437,   437,   437,   437,   437,   437,   438,   438,   432,
     439,   440,   440,   441,   441,   442,   442,   443,   444,   444,
     445,   446,   447,   447,   447,   447,   447,   447,   447,   447,
     447,   447,   448,   449,   449,   450,   450,   451,   451,   452,
     453,   454,   455,   455,   455,   456,   456,   457,   457,   458,
     459,   460,   461,   462,   463,   464,   464,   465,   465,   465,
     465,   466,   467,   467,   468,   468,   469,   470,   471,   471,
     472,   472,   472,   473,   474,   475,   476,   477,   477,   478,
     479,   480,   481,   481,   482,   482,   482,   483,   483,   483,
     484,   484,   485,   486,   486,   487,   488,   489,   490,   490,
     490,   491,   492,   493,   494,   494,   494,   495,   496,   496,
     497,   498,   499,   499,   500,   501,   502,   503,   503,   503,
     503,   503,   503,   504,   504,   505,   505,   506,   507,   508,
     509,   509,   509,   510,   510,   511,   512,   512,   512,   512,
     512,   512,   512,   512,   513,   514,   515,   515,   516,   516,
     516,   516,   516,   516,   516,   516,   516,   516,   516,   516,
     516,   516,   516,   516,   516,   516,   517,   518,   518,   518,
     519,   520,   521,   522,   522,   523,   523,   524,   524,   525,
     525,   526,   526,   526,   527,   527,   527,   527,   528,   529,
     530,   531,   532,   532,   532,   532,   532,   532,   532,   533,
     534,   534,   534,   534,   534,   534,   535,   536,   537,   538,
     538,   538,   538,   539,   540,   541,   541,   541,   541,   541,
     541,   541,   541,   541,   541,   542,   543,   544,   544,   544,
     544,   544,   545,   545,   546,   546,   546,   547,   547,   548,
     549,   550,   551,   552,   552,   552,   552,   553,   554,   554,
     555
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       2,     1,     1,     1,     0,     1,     1,     3,     1,     3,
       1,     0,     1,     3,     7,     7,     1,     3,     3,     1,
       1,     3,     7,     7,     1,     3,     3,     1,     1,     3,
       7,     7,     1,     3,     3,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     0,     1,     3,     2,     2,     1,     1,     1,     1,
       1,     1,     1,     0,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       2,     1,     1,     1,     1,     2,     2,     2,     1,     3,
       3,     3,     3,     3,     3,     2,     2,     3,     3,     4,
       2,     0,     1,     0,     1,     2,     2,     2,     1,     4,
       1,     3,     2,     1,     4,     0,     4,     4,     6,     6,
       6,     1,     1,     5,     5,     5,     1,     1,     4,     1,
       1,     1,     1,     1,     2,     1,     0,     3,     3,     3,
       1,     0,     1,     1,     1,     0,     6,     5,     4,     3,
       1,     0,     3,     4,     3,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     3,     3,     1,     2,     2,     2,
       4,     6,     3,     1,     3,     2,     2,     2,     2,     1,
       1,     1,     1,     1,     2,     0,     1,     1,     1,     1,
       0,     1,     1,     1,     1,     1,     2,     2,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     5,
       4,     2,     1,     3,     1,     3,     3,     2,     0,     0,
       1,     5,     1,     3,     3,     2,     6,     4,     2,     3,
       1,     1,     1,     1,     1,     0,     2,     2,     1,     1,
       1,     1,     1,     3,     3,     1,     1,     1,     1,     1,
       1,     1,     1,     3,     1,     1,     0,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     0,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     8,     1,     1,     1,     1,     1,     1,     8,     1,
       1,     0,     1,     1,     0,     1,     1,     7,     1,     1,
       9,     0,     1,     4,     1,     1,     3,     4,     0,     1,
       3,     6,     5,     2,     4,     3,     0,     1,     1,     1,
       6,     1,     0,     1,     3,     1,     1,     9,     1,     1,
       1,     9,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     0,    12,    14,     1,     0,    10,
      12,     7,     1,     1,     1,     2,     0,     2,     0,     5,
       6,     2,     6,     1,     1,     1,     1,     3,     2,     2,
       1,     8,     1,     1,     7,     8,     2,     2,     2,     0,
       1,     1,     1,     9,     2,     6,     1,     1,     0,     4,
       9,     9,     1,     1,     1,     1,     1,     3,     2,     2,
       1,     1,    11,     1,     2,    11,     2,    11,     1,     1,
       1,     1,     3,     4,     1,     1,     1,     6,     0,     1,
       9,     9,     1,     1,     6,     2,     6,     1,     1,     1,
       1,     1,     1,     1,     0,     1,     1,     7,     6,    11,
       2,     0,     2,     1,     1,     7,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     8,     3,     4,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,    10,    10,     2,     2,    10,    14,     2,     2,     3,
       1,     9,     9,    13,     1,     1,     1,     1,     1,    11,
      12,     9,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     7,    13,     1,
       1,     1,     1,     1,    14,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     9,     9,     1,     1,     1,
       1,     1,     1,     0,     1,     1,     1,     1,     1,    15,
      15,    14,     2,     1,     1,     1,     1,     1,     1,     1,
       7
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
     230,    21,    22,    23,    24,    25,    26,    27,    28,   228,
       0,   229,    81,     0,     0,   227,     0,    13,    15,    19,
     230,    29,     6,     2,     3,    12,     4,     5,    10,    14,
      11,     9,     7,     8,    18,     0,    17,   191,     0,    16,
       0,     0,     0,    67,    68,    69,    70,    74,    71,    66,
      72,    73,    75,    76,    77,    78,    79,    80,    82,    84,
      30,    85,    98,   102,   100,   101,   103,    96,    97,    99,
     104,   105,   106,   107,   108,   110,   109,   111,   114,   115,
     112,   153,   116,     0,     0,     1,    20,   173,   356,   171,
     176,   190,   225,   195,   356,     0,   182,   234,   235,     0,
     161,     0,     0,   181,   237,     0,   128,     0,     0,     0,
     155,     0,   152,    32,   266,   238,   239,   240,   241,   243,
     244,   245,   246,   247,   242,   248,   249,   250,   251,   252,
     253,   254,   255,   256,   257,   258,   259,   260,   261,   262,
     263,   264,   265,   268,   271,   272,   273,   269,   270,   283,
     285,   286,   287,   288,   289,   274,   275,   276,   277,   278,
     279,   280,   281,   282,   284,   290,   291,   292,   293,   294,
     295,   296,   298,   297,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   267,    31,   148,     0,    33,    41,
     355,   354,     0,     0,   196,   356,   203,   206,   356,     0,
       0,   202,   197,   198,   200,   201,   199,     0,   166,   167,
       0,   175,     0,     0,     0,     0,     0,   219,   220,   222,
     221,   223,   226,   192,    41,   194,   356,   189,     0,     0,
       0,     0,     0,   169,   176,   180,     0,   120,     0,   126,
     135,     0,   127,     0,   136,     0,   141,     0,   157,    83,
       0,   150,   154,     0,    42,   318,     0,   311,     0,   328,
     356,   188,   207,   231,   233,   232,   236,     0,   430,   434,
     594,   475,   432,   441,     0,   476,   436,   435,   478,   593,
     433,   426,   568,   444,   529,   445,   446,   449,   448,   550,
       0,     0,     0,     0,   441,     0,   538,   441,   424,   429,
     425,     0,     0,   538,     0,     0,     0,     0,     0,   451,
     451,   451,     0,     0,     0,   479,     0,   538,   494,     0,
     573,   572,     0,   428,   427,   551,     0,     0,     0,     0,
       0,   498,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    41,     0,   372,     0,   371,   441,   369,   441,   370,
     377,   469,   368,   378,   472,   379,   380,   373,   374,   375,
     381,   376,   382,   383,   384,   385,   387,   388,   386,   389,
     390,   441,   391,   392,   393,   394,   395,   396,   397,     0,
     398,   399,   400,   401,   402,   403,   591,   404,   405,   406,
     407,   408,   409,   410,   423,   411,   412,   417,   418,   419,
     421,   414,   413,   416,   415,   420,   422,     0,     0,   172,
     179,   208,   209,   217,     0,   213,   218,   215,   216,   224,
     174,   193,   141,   141,     0,     0,     0,     0,   183,     0,
     122,   121,   123,   124,   125,   132,   131,   137,   133,   134,
     129,   130,     0,   142,   143,     0,     0,   151,     0,     0,
      36,     0,     0,   329,   205,   141,   542,   543,   357,   358,
     360,   359,   546,   439,   365,   544,   440,   444,   545,   441,
       0,     0,   569,     0,   442,   443,   553,   444,     0,   441,
     441,   441,   530,   531,   532,     0,   472,     0,   608,   609,
     610,   611,   612,   613,   614,   615,   616,   617,   618,   619,
     620,   621,   622,   623,   625,   624,     0,   626,   441,     0,
       0,     0,   441,     0,     0,     0,     0,   536,   537,    93,
       0,   361,   362,   364,   363,   366,   513,   514,   520,   516,
     515,   441,     0,    86,    87,    88,    89,    90,     0,    91,
       0,     0,     0,   697,   698,     0,   675,   677,   676,   680,
     681,   682,   683,   684,   678,   679,     0,     0,     0,     0,
     452,     0,   467,   468,   458,     0,   534,   702,   511,   652,
     653,   654,   655,   656,   657,   658,   659,     0,     0,   493,
       0,     0,   705,   703,   704,   706,   707,     0,   559,   558,
     560,   561,     0,   338,   339,   341,   340,     0,   335,   347,
     346,   575,     0,   584,     0,   497,     0,     0,     0,     0,
     660,   661,   662,   663,   664,   665,   666,     0,   671,   672,
     670,   669,   673,     0,   502,   503,   504,   506,     0,   585,
     586,     0,   644,   645,   646,   647,   648,     0,   204,     0,
     437,   441,     0,     0,   473,   367,   441,     0,   471,   480,
       0,     0,     0,   441,   441,     0,   141,   177,     0,     0,
       0,     0,     0,   163,   165,   164,   170,   178,   185,     0,
     140,     0,   144,     0,     0,     0,   149,   317,     0,   314,
       0,     0,     0,   327,   310,     0,   549,   548,   444,     0,
       0,     0,   554,     0,   528,   527,   526,     0,   556,     0,
     634,   627,   628,   629,   633,   630,   441,     0,   441,     0,
     638,   637,   441,     0,     0,     0,     0,   352,     0,    92,
     693,     0,   519,   518,     0,     0,    95,    94,   113,     0,
       0,     0,   693,     0,   693,   596,   598,   597,   602,   603,
     601,   600,   599,   604,     0,     0,   312,   351,     0,     0,
       0,   606,     0,     0,     0,   482,   483,   484,   490,   491,
     489,   488,   487,   485,   486,   492,     0,     0,   709,   708,
       0,   562,   337,   330,   331,   332,   333,   334,   336,     0,
     577,   578,   579,   580,   581,   582,   583,     0,     0,     0,
       0,     0,     0,     0,     0,   508,     0,     0,     0,     0,
       0,     0,     0,     0,   444,     0,     0,     0,     0,   592,
     590,     0,   168,   210,   212,   214,   160,   158,   184,   186,
     138,     0,   159,    40,    58,    50,     0,    38,    49,    46,
     145,    57,    54,   146,    65,    62,   147,   156,    34,     0,
     316,     0,     0,    37,   309,   547,     0,     0,     0,     0,
       0,     0,     0,     0,   640,     0,     0,     0,     0,     0,
       0,     0,   687,   688,   689,   690,   691,   692,   119,     0,
     517,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   453,   454,   345,   455,     0,   466,   607,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   508,     0,   505,     0,   342,   348,   349,   350,
       0,     0,     0,     0,     0,     0,   474,     0,     0,     0,
       0,     0,     0,   139,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    35,   313,   315,   325,   319,   322,
       0,   326,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   353,   335,   565,   564,
     566,     0,   117,   118,   539,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   507,   509,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     211,     0,     0,     0,     0,     0,     0,    48,    47,    56,
      55,    64,    63,    43,    39,    51,    59,   320,     0,     0,
     324,     0,     0,     0,     0,     0,     0,     0,     0,   639,
       0,     0,     0,     0,     0,     0,   567,     0,     0,     0,
     335,   523,   522,     0,   535,     0,     0,     0,     0,   696,
     694,   695,     0,     0,   456,   457,     0,   463,   470,     0,
       0,     0,     0,     0,   576,     0,   512,     0,   510,     0,
       0,     0,     0,     0,   343,   344,   588,     0,     0,     0,
       0,     0,     0,     0,     0,   574,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    34,
     323,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   524,     0,
       0,     0,     0,     0,     0,   465,   459,     0,     0,     0,
       0,     0,   710,     0,   501,     0,     0,   667,     0,   587,
       0,     0,     0,   447,     0,     0,     0,     0,   595,     0,
       0,     0,     0,     0,     0,   141,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   563,
       0,   521,   525,     0,     0,     0,     0,     0,     0,     0,
       0,   464,     0,     0,     0,   605,     0,     0,     0,     0,
       0,   431,   438,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   321,   571,     0,   570,
       0,   533,     0,     0,     0,   642,     0,   641,     0,     0,
     651,   541,     0,   685,     0,     0,   686,   462,   460,     0,
       0,   540,     0,     0,     0,     0,     0,     0,   450,   477,
     481,     0,    45,    44,    53,    52,    61,    60,   631,     0,
       0,     0,   635,     0,   632,     0,     0,     0,     0,   461,
       0,     0,     0,     0,   499,     0,     0,     0,   555,   557,
       0,     0,     0,     0,     0,     0,     0,     0,   589,     0,
       0,   649,   552,     0,     0,   650,     0,     0,     0,     0,
       0,   495,   500,     0,     0,   643,     0,     0,     0,   668,
       0,     0,   636,     0,     0,   701,   496,   674,   699,   700
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    17,    18,    19,    20,    21,    22,    23,   598,   935,
     451,  1011,   257,  1012,   830,  1013,  1014,   833,  1015,  1016,
     836,   414,    55,    56,    57,    58,    59,    24,    25,   539,
     540,   720,   728,    80,   106,   729,    82,   107,   963,   964,
     108,   434,   241,   109,   443,   444,   673,   674,   187,   253,
      84,   194,   248,    27,    28,    29,    30,   102,    31,    32,
      33,   210,   211,   234,   667,    90,   410,   195,   225,   212,
     235,    91,   236,   103,   429,   819,    34,    35,    36,    37,
      92,    38,    39,   196,   197,   198,   217,   218,   415,   416,
     219,   220,   221,   222,   223,   199,   266,   104,   200,    41,
     188,   201,   202,   203,   747,   679,   680,   449,  1028,   939,
     940,   842,   204,   205,   206,   777,   778,   599,   600,   908,
     909,   882,   717,   910,   748,   718,   207,   464,   525,   465,
     527,   646,   341,   342,   343,   344,   641,   345,   466,   467,
     475,   476,   346,   347,   348,   349,   561,   350,   884,   885,
     750,  1137,   979,   564,   351,   649,   647,   352,   353,   354,
     650,   355,   765,   766,   580,   356,   606,   357,   358,   359,
     627,   795,   903,   360,   361,   362,   363,   529,   530,   531,
     532,   364,  1053,   365,   482,   483,   484,   485,   366,   367,
     368,   518,   519,   520,   369,   370,   468,   469,   470,   371,
     372,   478,   373,   487,   374,   591,   592,   375,   960,   961,
     376,   473,   377,   378,   379,   380,   381,   382,   786,   787,
     631,   383,   384,   385,   655,   386,   387,   743,   744,   388,
     389,   507,   508,   705,   706,   390,   391,   509,   392,   513,
     855,   393,   636,   637,   394,   395,   396,   576,   577,   616,
     617,   397,   398,   622,   623,   399,   556,   400,   401,   867,
     868,  1062,   545,   402,   403,   404,   405,   586,   587,   770,
     406
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -1038
static const yytype_int16 yypact[] =
{
     438, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
    -165, -1038,   714,  -110,  -101, -1038,   310, -1038, -1038, -1038,
     210, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038,   -88, -1038,   -90,    -2, -1038,
     223,   244,   192, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,   -49,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038,   781,  2999, -1038, -1038, -1038,    86, -1038,
      29, -1038,   179, -1038,    70,  2999, -1038, -1038,   205,   244,
   -1038,   244,   221,   -35, -1038,     5,   -23,   482,   310,  2999,
      32,   714, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038,    12,  -186, -1038,    20,
   -1038, -1038,    47,   316, -1038,    86, -1038, -1038,    70,   286,
     244, -1038, -1038, -1038, -1038, -1038, -1038,  3184, -1038, -1038,
     244, -1038,  -209,    60,    69,   -61,    10, -1038, -1038, -1038,
   -1038, -1038,   179, -1038,   322, -1038,    70, -1038,  2999,  2999,
    2999,  2999,  2999, -1038,    29, -1038,  2999, -1038,   136,   160,
      33,   160,   160,   781,    33,   781,    79,    91, -1038, -1038,
      96, -1038, -1038,  2999, -1038,   115,  2999, -1038,   167,   163,
      70, -1038, -1038, -1038, -1038, -1038, -1038,  2999, -1038, -1038,
   -1038, -1038, -1038,   248,   781, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038,   303,    58,   -60, -1038, -1038, -1038, -1038, -1038,
     303,   573,   534,   781,   278,   781,   301,   253, -1038, -1038,
   -1038,   -24,   340,   301,  -149,   370,  -149,  -149,   370,   306,
     233,   306,   212,   222,   227, -1038,   348,   301,   340,   340,
   -1038, -1038,  -116, -1038, -1038, -1038,   142,  2019,   781,   781,
     781,   330,   781,   781,   781,   284,   180,   147,   340,   283,
     266,   322,   781, -1038,   362, -1038,   278, -1038,   278, -1038,
   -1038, -1038,   135, -1038,   392, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038,   248, -1038, -1038, -1038, -1038, -1038, -1038, -1038,   781,
   -1038, -1038, -1038, -1038, -1038, -1038,   -95, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038,  2999,    29, -1038,
   -1038, -1038,   315, -1038,   307, -1038,   320, -1038,   320, -1038,
   -1038, -1038,    79,    79,   308,   314,   317,  -193, -1038,   -90,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038,  -124,   321,   318,   327,   319, -1038,   -81,  2999,
   -1038,  -166,   333, -1038, -1038,    79, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038,   425,   419,   278,
     781,  2019, -1038,   781, -1038, -1038, -1038,   425,   781,   278,
     278,   278, -1038, -1038, -1038,   781,   392,   781, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038,   650, -1038,    18,   781,
     573,   650,    18,   781,  2019,   781,  1596, -1038, -1038,   340,
     781, -1038, -1038, -1038, -1038, -1038, -1038, -1038,   425, -1038,
   -1038,   278,   781, -1038, -1038, -1038, -1038, -1038,   340, -1038,
     113,  2019,   781, -1038, -1038,   348, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038,   781,   348,    67,   781,
   -1038,  2999, -1038, -1038,   323,  1671, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038,   404,   781, -1038,
     190,    67, -1038, -1038, -1038, -1038, -1038,   111, -1038, -1038,
   -1038, -1038,   356, -1038, -1038, -1038, -1038,  2999,   279, -1038,
   -1038, -1038,  2019,   305,  2019, -1038,   781,  2019,  2019,  2019,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038,   781, -1038, -1038,
   -1038, -1038, -1038,   418, -1038, -1038, -1038,    67,   283, -1038,
   -1038,   364, -1038, -1038, -1038, -1038, -1038,   781, -1038,  2019,
   -1038,   278,   781,   781, -1038, -1038,   278,   781, -1038, -1038,
     781,   781,  2019,   278,   278,   781,    79, -1038,   373,   374,
     377,   365,   366, -1038, -1038, -1038, -1038, -1038,    11,   368,
   -1038,   381, -1038,   369,  2169,   385, -1038, -1038,   781, -1038,
    -179,   372,  2999, -1038, -1038,   376, -1038, -1038,   425,  2019,
     382,  2019, -1038,  2019, -1038, -1038, -1038,  2019, -1038,  2019,
   -1038, -1038, -1038, -1038, -1038, -1038,   278,   781,   278,  2019,
   -1038, -1038,   278,  2019,   386,   781,  2999, -1038,   387, -1038,
      92,   389, -1038,   425,   781,   113, -1038, -1038, -1038,  2019,
     388,  1596,    92,  2019,    92, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038,   348,  2019, -1038, -1038,   424,  2019,
    2999, -1038,   426,   781,  1596, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038,   781,   781, -1038, -1038,
    2019, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,   390,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038,  2019,   393,  2019,
     394,   395,   396,  2019,   456,  2019,   781,   430,  2584,  2019,
     436,   781,  2019,  2019,   425,  2019,  2019,  2019,   437, -1038,
   -1038,  2019, -1038,   439, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038,   434, -1038, -1038, -1038, -1038,  2434, -1038,   440, -1038,
   -1038,   441, -1038, -1038,   442, -1038, -1038, -1038,  2999,    29,
   -1038,   -80,   463, -1038, -1038, -1038,   444,  2019,   446,   447,
     448,   449,   781,  2019,   435,   472,   781,   474,  2019,  2019,
    -155,  1596, -1038, -1038, -1038, -1038, -1038, -1038,    33,  2584,
   -1038,  2019,  2019,   475,  2019,   476,    33,   478,    33,   781,
     479, -1038, -1038, -1038, -1038,  -157,   480, -1038,   781,   483,
    2019,  2019,   484,  2019,   486,  2019,   487,  2019,  2019,  2019,
     488,   781,  2019,   481, -1038,  2584,   272, -1038, -1038, -1038,
     489,   491,  2019,  2019,   492,   496, -1038,   497,   498,   499,
    2019,   500,   506, -1038,  2434,    97,   501,   161,   502,   194,
     503,  2849,   391,   485, -1038, -1038, -1038, -1038,   160, -1038,
    -140, -1038,  2019,   507,  2019,  2019,  2019,  2019,   781,   509,
    2019,  2019,  2019,  2019,   510,   511, -1038,    64, -1038, -1038,
   -1038,   494, -1038, -1038, -1038,   508,   513,   514,  2019,   512,
     515,   781,   516,   781,   171,   518,  2019,   517,  2094,   519,
    1596,   520,   522,   525,  2019,   540,  2019,   542,  2019,   543,
     526,   528,  2019,   781, -1038, -1038,   561,   571,   572,   560,
    2019,   569,   570,  2019,  2019,  2019,  2019,  2019,   564,  2019,
   -1038,   229,   575,   234,   577,   240,   579,   574,   582,   583,
     585,   586,   587, -1038, -1038, -1038, -1038, -1038,   781,    29,
   -1038,   588,  2019,   589,   590,   591,   592,  2019,  2019, -1038,
     593,   594,   595,  2019,  2019,   578, -1038,   596,  2019,  2019,
     -69, -1038, -1038,   567, -1038,  2584,   171,  2019,   171, -1038,
   -1038, -1038,   598,  2019, -1038, -1038,  1746, -1038, -1038,   597,
    2584,   599,  2019,   600, -1038,   603, -1038,   601, -1038,  2019,
    2019,   602,  1596,   604, -1038, -1038, -1038,   605,  2019,  2019,
     606,   611,   613,   617,   618, -1038,   612,   574,   582,   583,
     585,   586,   587,   620,   621,   622,   623,   625,   626,  2999,
   -1038,  2019,   619,  2019,  2019,  2019,  2019,   628,   630,  2019,
    2019,  2019,   631,   632,   629,  1596,   627,   633, -1038,   634,
    1596,   636,   658,  2019,   659,   662, -1038,   -85,   661,   663,
    2584,   657, -1038,  2019, -1038,   666,   668, -1038,   669, -1038,
    2019,   664,   665, -1038,  2019,  2019,  2019,  2019, -1038,  2924,
     635,   608,   670,   607,   674,    79,   671,  2019,   672,   677,
     673,   678,  2019,  2019,   675,   682,   676,  2019,  2019, -1038,
     680, -1038, -1038,   681,   685,   683,  2019,   686,   684,  2999,
    2019,   690,  2019,   687,   689, -1038,   694,  2019,  2019,   667,
     697, -1038, -1038,   692,   695,   696,   698,   249,  2509,   703,
     264,   -91,   704,   265,   -25,   705, -1038, -1038,   700, -1038,
    2019, -1038,  2019,   709,   702, -1038,  2019, -1038,   706,   711,
   -1038, -1038,   710, -1038,   712,  1596, -1038, -1038, -1038,  2999,
     715, -1038,   716,  2019,   718,   717,  2019,  2019, -1038, -1038,
   -1038,  2019, -1038, -1038, -1038, -1038, -1038, -1038, -1038,   719,
     721,  2019, -1038,   720, -1038,  2019,  2019,  1596,   726, -1038,
    1596,  2019,   723,  2019, -1038,   722,   725,   727, -1038, -1038,
     736,  2019,   729,   738,   735,   740,   737,   -52, -1038,   734,
    1596, -1038, -1038,  2019,   739, -1038,  1596,   743,  1596,   741,
    2019, -1038, -1038,   742,   744, -1038,   746,  1596,   745, -1038,
     747,   749, -1038,   750,   751, -1038, -1038, -1038, -1038, -1038
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
   -1038, -1038, -1038,   764, -1038, -1038, -1038, -1038,   -84,  -242,
     152,  -665,  -191,  -658, -1038,  -660,  -667, -1038,  -666,  -668,
   -1038,    45, -1038, -1038, -1038,   792, -1038, -1038, -1038, -1038,
    -280, -1038, -1038,  -659,    -3,   263,  -650,     7,  -326, -1038,
    -105, -1038, -1038,   -33, -1038,  -419, -1038, -1038, -1038, -1038,
   -1038,    59, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038,  -443,   584, -1038, -1038, -1038, -1038,   326,  -152,   780,
   -1038,   609, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038,   -55,   -57, -1038, -1038,   360,   807,
   -1038, -1038,   808, -1038, -1038,    62, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038,   193, -1038, -1038, -1038,     0,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,  -788, -1038,
    -827, -1037,   304,  -849, -1038,  -719, -1038, -1038, -1038,  -246,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,  -260,  -279,
    -504,  -450, -1038, -1038, -1038, -1038,   270, -1038,    55, -1038,
   -1038, -1038, -1038, -1038, -1038,   548, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038,   133, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038,   -13, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038,  -232, -1038, -1038,  -218, -1038,   679, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
     411, -1038, -1038, -1038, -1038, -1038, -1038, -1038,  -518, -1038,
   -1038, -1038,  -262, -1038,   529, -1038, -1038, -1038, -1038,   530,
    -692, -1038, -1038, -1038, -1038, -1038, -1038, -1038,  -517, -1038,
   -1038, -1038, -1038, -1038, -1038, -1038,   752, -1038, -1038, -1038,
    -160,  -480,   276, -1038, -1038, -1038, -1038, -1038, -1038, -1038,
   -1038
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -231
static const yytype_int16 yytable[] =
{
     186,   240,   243,   661,   662,   678,   835,   832,   834,   828,
     907,   227,   875,    81,   831,   515,   829,   686,   528,   838,
     965,   857,   541,    83,   722,   246,   254,   692,   732,  1136,
     512,   506,   511,   420,   958,   889,   685,   226,   579,   581,
     734,   208,   208,   543,   209,   209,   261,    43,    44,    45,
      46,   526,    48,    49,    50,    51,   996,    54,   628,    26,
     408,   582,    40,   767,   472,   477,   228,   642,   229,   643,
     409,   542,   486,   189,   421,   653,   408,   190,   191,    26,
     112,   907,    40,   251,   544,   578,   666,   583,   584,   189,
     839,   252,   654,   190,   191,   533,   534,   535,   536,    42,
     840,   238,   537,   682,   242,   244,   645,   911,   454,   796,
     479,   683,   976,   239,   682,   245,   956,   907,    43,    44,
      45,    46,   977,    48,    49,    50,    51,   480,   481,  1029,
     773,   774,   775,   776,   435,   585,   437,   438,   260,  1030,
     669,  1051,   959,   262,   422,   423,   424,   425,   426,   670,
     638,   208,   428,  1238,   209,   105,    54,    60,   930,   928,
     929,   925,     9,    61,    11,   192,   927,   267,   926,   447,
     538,   262,   450,    15,    78,    79,   825,   407,     9,  1211,
      11,   192,    87,   455,  1190,   701,   702,   703,    89,    15,
     688,  -230,  -230,   193,  1191,    16,  -230,   110,   677,   937,
     694,   695,   696,   413,   463,   262,  1129,  -230,  -230,   193,
      85,    16,  -230,   997,   998,   255,   256,  1300,   962,   870,
     111,  1139,  1051,   456,   457,  1301,   962,   879,   962,   704,
      78,    79,   474,   711,   735,   736,   737,   812,   845,   719,
     439,   824,   441,   233,   700,  1214,   726,   436,   708,   710,
     511,   440,   723,   727,   768,   430,   431,   432,   725,   769,
    1041,  1069,   433,   773,   774,   775,   776,   907,    87,   237,
     588,   471,   589,   590,   417,    93,   738,   739,   740,   741,
     742,  -187,   907,   213,   214,   215,   216,   250,   818,   510,
     514,  1194,   516,     1,     2,     3,     4,     5,     6,     7,
       8,   247,     9,    10,    11,   458,   459,   460,   461,    12,
      13,   258,    14,    15,    95,   862,   644,   863,   864,    96,
     865,   259,   866,   656,   411,   602,   603,   604,   254,   607,
     608,   609,  -162,   412,  -162,    16,  1045,  -162,   105,   639,
      87,   224,    97,    98,    99,   100,   997,   998,   230,   101,
     231,   442,   907,   232,   916,   445,    87,   755,   756,   757,
     446,    88,   801,  1148,    94,   681,   931,   804,  1017,  1109,
     618,   619,   620,   621,   809,   810,   652,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,   448,   624,   625,   678,   626,   938,   758,
     759,   760,   761,   762,   763,   764,  1180,   263,   264,   562,
     560,  1184,   265,   456,   457,  1059,  1060,  1061,   458,   459,
     460,   461,   105,   458,   459,   460,   461,   852,   462,   711,
     932,   452,  1019,   856,   463,   521,   522,   523,   524,   463,
     453,    78,    79,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,   533,
     534,   535,   536,   933,   463,  1021,   537,   689,   456,   457,
     691,   632,   633,   634,   635,   693,   517,   746,   773,   774,
     775,   776,   697,   560,   699,    43,    44,    45,    46,   566,
      48,    49,    50,    51,  1207,  1212,  1215,  1213,   931,   567,
    1097,  1210,  1209,   932,   568,  1099,   707,    78,    79,   933,
     713,  1101,   715,   772,   629,   630,  1268,   721,   931,   605,
    1252,     1,     2,     3,     4,     5,     6,     7,     8,   724,
       9,    10,    11,   932,   933,  1254,  1256,    12,    13,   731,
      14,    15,   610,   611,   612,   613,   614,   615,  1284,   640,
     971,  1286,   973,   733,   997,   998,   745,   780,   781,   782,
     783,   784,   785,    16,   569,   570,   571,   572,   573,   574,
     575,  1303,   876,   648,   878,   754,  1130,  1306,  1132,  1308,
     563,   565,   557,   558,   658,   663,   938,   659,  1314,   660,
     827,   664,   672,   671,   665,   676,   675,   684,   843,   474,
     687,   749,   753,   789,   546,   547,   548,   549,   550,   551,
     552,   553,   554,   555,   793,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,   601,   450,   771,   799,   794,   798,   813,   814,   802,
     803,   820,   816,   817,   805,   821,   822,   806,   807,   837,
     841,   847,   811,   844,   901,   858,   861,   874,  1025,   893,
     105,   869,   895,   897,   898,   899,   886,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,   488,   489,   490,   491,   492,   493,   494,
     495,   496,   497,   498,   499,   500,   501,   502,   503,   504,
     505,   881,   905,   887,   853,   912,   920,   923,   922,   931,
     932,   933,   859,   942,   906,   944,   945,   946,   947,   950,
     463,   871,   488,   489,   490,   491,   492,   493,   494,   495,
     496,   497,   498,   499,   500,   501,   502,   503,   504,   505,
     941,   951,   827,   953,   968,   970,  1216,   972,   975,   978,
     888,  1026,   981,   984,   934,   986,   988,   992,   995,   463,
    1000,  1003,   999,   890,   891,  1004,  1005,  1006,  1007,  1009,
    1010,  1046,  1018,  1020,  1022,   690,  1032,   957,  1038,  1043,
    1044,  1047,  1048,  1049,    86,   906,  1065,  1055,  1057,  1054,
    1063,  1071,  1070,   904,  1072,  1079,  1068,  1080,   913,   488,
     489,   490,   491,   492,   493,   494,   495,   496,   497,   498,
     499,   500,   501,   502,   503,   504,   505,  1074,   714,  1076,
    1078,   906,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,  1027,  1083,  1084,  1085,  1086,  1088,  1089,
     827,  1095,  1124,  1103,  1128,   730,  1098,  1024,  1100,   948,
    1102,  1104,  1105,   952,  1106,  1107,  1108,  1111,  1113,  1114,
    1115,  1116,  1119,  1120,  1121,  1125,  1138,  1165,   860,   752,
    1133,  1140,  1143,   824,  1150,   825,   974,  1142,  1144,  1147,
    1154,  1149,  1155,  1153,  1050,   980,  1156,  1157,  1167,  1158,
    1159,  1160,  1161,  1162,  1067,  1163,  1164,  1172,   993,  1173,
    1177,  1178,  1179,   249,  1181,  1208,   779,  1183,   788,  1185,
    1182,   790,   791,   792,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
    1186,  1189,  1188,  1192,  1195,  1197,  1193,  1198,  1199,  1246,
    1211,  1201,  1202,   800,  1214,  1037,  1220,  1222,  1217,  1219,
    1221,  1226,  1225,  1227,  1232,  1235,   808,  1230,  1231,  1239,
    1233,  1236,  1242,  1243,  1241,  1050,  1247,  1251,  1056,  1248,
    1058,   906,  1249,  1250,  1253,  1255,  1257,  1258,  1261,  1262,
    1265,  1267,  1266,  1264,  1270,  1271,   906,  1273,   872,  1281,
    1082,  1290,   657,   846,  1274,   848,  1278,   849,  1279,  1285,
    1288,   850,  1291,   851,  1292,  1293,  1295,  1296,  1297,  1298,
    1299,  1302,  1307,   854,   427,  1311,  1305,   854,  1309,  1313,
     815,  1312,  1315,   418,  1316,   934,  1317,  1318,  1319,  1110,
     419,  1064,   936,   873,   698,   994,  1127,   877,   668,   797,
     709,   712,     0,     0,     0,     0,     0,     0,     0,   880,
     651,     0,     0,   883,     0,     0,   906,     0,     0,     0,
     559,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   892,   827,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   894,     0,   896,     0,     0,     0,   900,     0,   902,
       0,     0,     0,   854,     0,  1237,   914,   915,     0,   917,
     918,   919,     0,     0,     0,   921,     0,     0,     0,     0,
       0,     0,     0,     0,   827,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   943,     0,     0,     0,  1269,     0,   949,     0,     0,
       0,     0,   954,   955,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   966,   967,     0,   969,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   982,   983,     0,   985,     0,   987,
       0,   989,   990,   991,     0,     0,   902,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  1001,  1002,     0,     0,
       0,     0,     0,     0,  1008,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  1031,     0,  1033,  1034,
    1035,  1036,     0,     0,  1039,  1040,   854,  1042,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,  1052,     0,     0,     0,     0,     0,     0,     0,
     883,     0,     0,     0,     0,     0,     0,     0,  1073,     0,
    1075,     0,  1077,     0,     0,     0,  1081,     0,     0,     0,
       0,     0,     0,     0,  1087,     0,     0,  1090,  1091,  1092,
    1093,  1094,     0,  1096,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  1112,     0,     0,     0,
       0,  1117,  1118,     0,     0,     0,     0,  1122,  1123,     0,
       0,     0,  1126,  1052,     0,     0,     0,     0,     0,     0,
       0,  1131,     0,     0,     0,     0,     0,  1134,     0,     0,
     883,     0,     0,     0,     0,     0,  1141,     0,     0,     0,
       0,     0,     0,  1145,  1146,     0,     0,     0,     0,     0,
       0,     0,  1151,  1152,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,  1166,     0,  1168,  1169,  1170,
    1171,     0,     0,  1174,  1175,  1176,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,  1187,     0,     0,
       0,     0,     0,     0,     0,     0,     0,  1196,     0,     0,
       0,     0,     0,     0,  1200,     0,     0,     0,  1203,  1204,
    1205,  1206,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  1218,     0,     0,     0,     0,  1223,  1224,     0,     0,
       0,  1228,  1229,     0,     0,     0,     0,     0,     0,     0,
    1234,     0,     0,     0,   883,     0,  1240,     0,     0,     0,
       0,  1244,  1245,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,  1259,     0,  1260,     0,     0,     0,
    1263,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,  1272,     0,     0,
    1275,  1276,     0,     0,     0,  1277,     0,     0,     0,     0,
       0,     0,     0,     0,     0,  1280,     0,     0,     0,  1282,
    1283,     0,     0,     0,     0,  1287,     0,  1289,     0,     0,
       0,     0,     0,     0,     0,  1294,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,  1304,     0,     0,
     113,     0,     0,     0,  1310,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
     169,   170,   171,   172,   173,   174,   175,     0,   176,   177,
     178,   179,     0,   180,   181,   113,   182,   183,   184,     0,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,     0,   176,   177,   178,   179,     0,   180,   181,
     113,   182,   183,   184,     0,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
     169,   170,   171,   172,   173,   174,   175,     0,   176,   177,
     178,   179,     0,   180,   181,     0,   182,   183,   184,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     593,   594,   595,   596,   185,     0,   716,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   597,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   593,   594,   595,   596,   185,
       0,     0,     0,     0,     0,     0,     0,     0,   751,     0,
       0,     0,   597,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     593,   594,   595,   596,   185,     0,     0,     0,     0,     0,
       0,     0,     0,   113,     0,  1135,     0,   597,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
       0,   176,   177,   178,   179,     0,   180,   181,   113,   182,
     183,   184,     0,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   169,   170,
     171,   172,   173,   174,   175,     0,   176,   177,   178,   179,
       0,   180,   181,   113,   182,   183,   184,     0,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
       0,   176,   177,   178,   179,     0,   180,   181,     0,   182,
     183,   184,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   593,   594,   595,   596,   185,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     597,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   185,     0,     0,     0,     0,     0,     0,     0,
       0,     0,  1066,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   823,     0,   824,   825,   185,   113,   826,
       0,     0,     0,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   169,   170,
     171,   172,   173,   174,   175,     0,   176,   177,   178,   179,
       0,   180,   181,   113,   182,   183,   184,     0,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
       0,   176,   177,   178,   179,     0,   180,   181,   113,   182,
     183,   184,     0,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   169,   170,
     171,   172,   173,   174,   175,     0,   176,   177,   178,   179,
       0,   180,   181,     0,   182,   183,   184,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   823,     0,
     824,   825,   185,     0,   924,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   823,     0,     0,     0,   185,     0,  1208,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   593,   594,
     595,   596,   185,   113,     0,     0,     0,     0,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
       0,   176,   177,   178,   179,     0,   180,   181,   113,   182,
     183,   184,     0,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   169,   170,
     171,   172,   173,   174,   175,     0,   176,   177,   178,   179,
       0,   180,   181,   113,   182,   183,   184,     0,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
       0,   176,   177,   178,   179,     0,   180,   181,     0,   182,
     183,   184,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,  1023,     0,     0,     0,   185,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   823,     0,
       0,     0,   185,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,     0,   318,   319,   320,   321,   322,   323,
     324,   325,   326,   327,   328,   329,   330,   331,   332,   333,
     334,   335,   336,   337,   338,   339,   340,   185
};

static const yytype_int16 yycheck[] =
{
      84,   106,   107,   422,   423,   448,   674,   674,   674,   674,
     798,    95,   731,    16,   674,   294,   674,   467,   297,   678,
     869,   713,   302,    16,   528,   109,     6,   477,   545,  1066,
     292,   291,   292,   224,   861,   754,   455,    94,   318,   319,
     557,   122,   122,   192,   125,   125,   198,   108,   109,   110,
     111,   297,   113,   114,   115,   116,   905,    12,   338,     0,
     269,   177,     0,   581,   282,   283,    99,   346,   101,   348,
     279,   303,   290,     3,   226,   170,   269,     7,     8,    20,
      83,   869,    20,   269,   233,   317,   279,   203,   204,     3,
     269,   277,   187,     7,     8,   119,   120,   121,   122,   264,
     279,   124,   126,   269,   107,   108,   352,   799,   260,   627,
     170,   277,   269,   106,   269,   108,   271,   905,   108,   109,
     110,   111,   279,   113,   114,   115,   116,   187,   188,   269,
     199,   200,   201,   202,   239,   251,   241,   242,   195,   279,
     264,   968,   861,   198,   228,   229,   230,   231,   232,   273,
     341,   122,   236,  1190,   125,   178,   111,   267,   826,   826,
     826,   826,    92,   264,    94,    95,   826,   200,   826,   253,
     194,   226,   256,   103,   197,   198,   267,   210,    92,   270,
      94,    95,   270,   267,   269,   167,   168,   169,   278,   103,
     469,   121,   122,   123,   279,   125,   126,     5,   279,   279,
     479,   480,   481,   264,   186,   260,  1055,   121,   122,   123,
       0,   125,   126,   282,   283,   195,   196,   269,   868,   723,
     269,  1070,  1049,   165,   166,   277,   876,   744,   878,   508,
     197,   198,   174,   512,   167,   168,   169,   656,   688,   519,
     243,   266,   245,   278,   506,   270,   133,   240,   510,   511,
     510,   244,   531,   140,   143,   119,   120,   121,   538,   148,
     952,   980,   126,   199,   200,   201,   202,  1055,   270,   264,
     128,   274,   130,   131,   264,   277,   209,   210,   211,   212,
     213,   270,  1070,   104,   105,   106,   107,   275,   277,   292,
     293,  1140,   295,    83,    84,    85,    86,    87,    88,    89,
      90,   269,    92,    93,    94,   170,   171,   172,   173,    99,
     100,   264,   102,   103,    91,   223,   181,   225,   226,    96,
     228,     5,   230,   407,   264,   328,   329,   330,     6,   332,
     333,   334,   127,   264,   129,   125,   272,   132,   178,   342,
     270,   271,   119,   120,   121,   122,   282,   283,   127,   126,
     129,   272,  1140,   132,   804,   264,   270,   167,   168,   169,
     264,    35,   641,  1082,    38,   449,   269,   646,   271,  1028,
     190,   191,   192,   193,   653,   654,   379,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   278,   247,   248,   839,   250,   841,   209,
     210,   211,   212,   213,   214,   215,  1125,   121,   122,   176,
     177,  1130,   126,   165,   166,   244,   245,   246,   170,   171,
     172,   173,   178,   170,   171,   172,   173,   706,   180,   708,
     269,   264,   271,   712,   186,   182,   183,   184,   185,   186,
     277,   197,   198,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   119,
     120,   121,   122,   269,   186,   271,   126,   470,   165,   166,
     473,   205,   206,   207,   208,   478,   175,   561,   199,   200,
     201,   202,   485,   177,   487,   108,   109,   110,   111,   277,
     113,   114,   115,   116,  1159,  1162,  1164,  1163,   269,   277,
     271,  1161,  1160,   269,   277,   271,   509,   197,   198,   269,
     513,   271,   515,   597,   231,   232,  1235,   520,   269,   189,
     271,    83,    84,    85,    86,    87,    88,    89,    90,   532,
      92,    93,    94,   269,   269,   271,   271,    99,   100,   542,
     102,   103,   258,   259,   260,   261,   262,   263,  1267,   187,
     876,  1270,   878,   556,   282,   283,   559,   252,   253,   254,
     255,   256,   257,   125,   216,   217,   218,   219,   220,   221,
     222,  1290,   732,   181,   734,   578,  1056,  1296,  1058,  1298,
     310,   311,   306,   307,   269,   277,  1029,   280,  1307,   269,
     674,   277,   274,   272,   277,   276,   269,   264,   682,   174,
     181,   278,   198,   606,   234,   235,   236,   237,   238,   239,
     240,   241,   242,   243,   617,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   327,   716,   277,   637,   217,   272,   264,   264,   642,
     643,   273,   277,   277,   647,   264,   277,   650,   651,   264,
     278,   269,   655,   277,   198,   269,   269,   269,   267,   269,
     178,   272,   269,   269,   269,   269,   750,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   277,   272,   277,   707,   269,   269,   273,   269,   269,
     269,   269,   715,   269,   798,   269,   269,   269,   269,   284,
     186,   724,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     277,   269,   826,   269,   269,   269,  1165,   269,   269,   269,
     753,   266,   269,   269,   838,   269,   269,   269,   277,   186,
     269,   269,   273,   766,   767,   269,   269,   269,   269,   269,
     264,   277,   271,   271,   271,   471,   269,   861,   269,   269,
     269,   273,   269,   269,    20,   869,   269,   272,   272,   277,
     272,   269,   272,   796,   269,   269,   277,   269,   801,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   277,   514,   277,
     277,   905,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   938,   273,   264,   264,   277,   269,   269,
     924,   277,   264,   269,   277,   541,   271,   931,   271,   852,
     271,   269,   269,   856,   269,   269,   269,   269,   269,   269,
     269,   269,   269,   269,   269,   269,   269,  1109,   716,   565,
     272,   272,   269,   266,   269,   267,   879,   277,   277,   277,
     269,   277,   269,   277,   968,   888,   269,   269,   269,   277,
     270,   270,   270,   270,   978,   270,   270,   269,   901,   269,
     269,   269,   273,   111,   277,   270,   602,   273,   604,   273,
     277,   607,   608,   609,   133,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     272,   269,   273,   272,   277,   269,   273,   269,   269,   272,
     270,   277,   277,   639,   270,   948,   269,   269,   277,   277,
     277,   269,   277,   277,   269,   269,   652,   277,   277,   269,
     277,   277,   273,   269,   277,  1049,   269,   269,   971,   277,
     973,  1055,   277,   277,   271,   271,   271,   277,   269,   277,
     269,   269,   272,   277,   269,   269,  1070,   269,   725,   269,
     993,   269,   408,   689,   277,   691,   277,   693,   277,   273,
     277,   697,   277,   699,   277,   269,   277,   269,   273,   269,
     273,   277,   269,   709,   234,   273,   277,   713,   277,   273,
     660,   277,   277,   216,   277,  1109,   277,   277,   277,  1029,
     222,   976,   839,   729,   486,   902,  1049,   733,   429,   628,
     510,   512,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   745,
     371,    -1,    -1,   749,    -1,    -1,  1140,    -1,    -1,    -1,
     308,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   770,  1159,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   787,    -1,   789,    -1,    -1,    -1,   793,    -1,   795,
      -1,    -1,    -1,   799,    -1,  1189,   802,   803,    -1,   805,
     806,   807,    -1,    -1,    -1,   811,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,  1208,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   847,    -1,    -1,    -1,  1239,    -1,   853,    -1,    -1,
      -1,    -1,   858,   859,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   871,   872,    -1,   874,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   890,   891,    -1,   893,    -1,   895,
      -1,   897,   898,   899,    -1,    -1,   902,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   912,   913,    -1,    -1,
      -1,    -1,    -1,    -1,   920,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   942,    -1,   944,   945,
     946,   947,    -1,    -1,   950,   951,   952,   953,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   968,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     976,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   984,    -1,
     986,    -1,   988,    -1,    -1,    -1,   992,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,  1000,    -1,    -1,  1003,  1004,  1005,
    1006,  1007,    -1,  1009,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1032,    -1,    -1,    -1,
      -1,  1037,  1038,    -1,    -1,    -1,    -1,  1043,  1044,    -1,
      -1,    -1,  1048,  1049,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1057,    -1,    -1,    -1,    -1,    -1,  1063,    -1,    -1,
    1066,    -1,    -1,    -1,    -1,    -1,  1072,    -1,    -1,    -1,
      -1,    -1,    -1,  1079,  1080,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1088,  1089,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1111,    -1,  1113,  1114,  1115,
    1116,    -1,    -1,  1119,  1120,  1121,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,  1133,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,  1143,    -1,    -1,
      -1,    -1,    -1,    -1,  1150,    -1,    -1,    -1,  1154,  1155,
    1156,  1157,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1167,    -1,    -1,    -1,    -1,  1172,  1173,    -1,    -1,
      -1,  1177,  1178,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1186,    -1,    -1,    -1,  1190,    -1,  1192,    -1,    -1,    -1,
      -1,  1197,  1198,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,  1220,    -1,  1222,    -1,    -1,    -1,
    1226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,  1243,    -1,    -1,
    1246,  1247,    -1,    -1,    -1,  1251,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1261,    -1,    -1,    -1,  1265,
    1266,    -1,    -1,    -1,    -1,  1271,    -1,  1273,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1281,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,  1293,    -1,    -1,
       4,    -1,    -1,    -1,  1300,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    -1,    72,    73,
      74,    75,    -1,    77,    78,     4,    80,    81,    82,    -1,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    -1,    72,    73,    74,    75,    -1,    77,    78,
       4,    80,    81,    82,    -1,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    -1,    72,    73,
      74,    75,    -1,    77,    78,    -1,    80,    81,    82,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     264,   265,   266,   267,   268,    -1,   270,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   281,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   264,   265,   266,   267,   268,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   277,    -1,
      -1,    -1,   281,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     264,   265,   266,   267,   268,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,     4,    -1,   279,    -1,   281,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      -1,    72,    73,    74,    75,    -1,    77,    78,     4,    80,
      81,    82,    -1,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    -1,    72,    73,    74,    75,
      -1,    77,    78,     4,    80,    81,    82,    -1,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      -1,    72,    73,    74,    75,    -1,    77,    78,    -1,    80,
      81,    82,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   264,   265,   266,   267,   268,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     281,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   268,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   278,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   264,    -1,   266,   267,   268,     4,   270,
      -1,    -1,    -1,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    -1,    72,    73,    74,    75,
      -1,    77,    78,     4,    80,    81,    82,    -1,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      -1,    72,    73,    74,    75,    -1,    77,    78,     4,    80,
      81,    82,    -1,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    -1,    72,    73,    74,    75,
      -1,    77,    78,    -1,    80,    81,    82,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   264,    -1,
     266,   267,   268,    -1,   270,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   264,    -1,    -1,    -1,   268,    -1,   270,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   264,   265,
     266,   267,   268,     4,    -1,    -1,    -1,    -1,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      -1,    72,    73,    74,    75,    -1,    77,    78,     4,    80,
      81,    82,    -1,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    -1,    72,    73,    74,    75,
      -1,    77,    78,     4,    80,    81,    82,    -1,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      -1,    72,    73,    74,    75,    -1,    77,    78,    -1,    80,
      81,    82,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   264,    -1,    -1,    -1,   268,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   264,    -1,
      -1,    -1,   268,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    -1,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,   268
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,    83,    84,    85,    86,    87,    88,    89,    90,    92,
      93,    94,    99,   100,   102,   103,   125,   286,   287,   288,
     289,   290,   291,   292,   312,   313,   336,   338,   339,   340,
     341,   343,   344,   345,   361,   362,   363,   364,   366,   367,
     380,   384,   264,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   306,   307,   308,   309,   310,   311,
     267,   264,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   197,   198,
     318,   319,   321,   322,   335,     0,   288,   270,   352,   278,
     350,   356,   365,   277,   352,    91,    96,   119,   120,   121,
     122,   126,   342,   358,   382,   178,   319,   322,   325,   328,
       5,   269,   319,     4,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    72,    73,    74,    75,
      77,    78,    80,    81,    82,   268,   293,   333,   385,     3,
       7,     8,    95,   123,   336,   352,   368,   369,   370,   380,
     383,   386,   387,   388,   397,   398,   399,   411,   122,   125,
     346,   347,   354,   104,   105,   106,   107,   371,   372,   375,
     376,   377,   378,   379,   271,   353,   370,   293,   328,   328,
     127,   129,   132,   278,   348,   355,   357,   264,   124,   322,
     325,   327,   319,   325,   319,   322,   293,   269,   337,   310,
     275,   269,   277,   334,     6,   195,   196,   297,   264,     5,
     370,   353,   369,   121,   122,   126,   381,   328,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,   417,   418,   419,   420,   422,   427,   428,   429,   430,
     432,   439,   442,   443,   444,   446,   450,   452,   453,   454,
     458,   459,   460,   461,   466,   468,   473,   474,   475,   479,
     480,   484,   485,   487,   489,   492,   495,   497,   498,   499,
     500,   501,   502,   506,   507,   508,   510,   511,   514,   515,
     520,   521,   523,   526,   529,   530,   531,   536,   537,   540,
     542,   543,   548,   549,   550,   551,   555,   328,   269,   279,
     351,   264,   264,   264,   306,   373,   374,   264,   374,   377,
     297,   353,   293,   293,   293,   293,   293,   354,   293,   359,
     119,   120,   121,   126,   326,   325,   322,   325,   325,   319,
     322,   319,   272,   329,   330,   264,   264,   293,   278,   392,
     293,   295,   264,   277,   353,   293,   165,   166,   170,   171,
     172,   173,   180,   186,   412,   414,   423,   424,   481,   482,
     483,   319,   481,   496,   174,   425,   426,   481,   486,   170,
     187,   188,   469,   470,   471,   472,   481,   488,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   423,   516,   517,   522,
     319,   423,   517,   524,   319,   424,   319,   175,   476,   477,
     478,   182,   183,   184,   185,   413,   414,   415,   424,   462,
     463,   464,   465,   119,   120,   121,   122,   126,   194,   314,
     315,   315,   478,   192,   233,   547,   234,   235,   236,   237,
     238,   239,   240,   241,   242,   243,   541,   547,   547,   541,
     177,   431,   176,   431,   438,   431,   277,   277,   277,   216,
     217,   218,   219,   220,   221,   222,   532,   533,   478,   315,
     449,   315,   177,   203,   204,   251,   552,   553,   128,   130,
     131,   490,   491,   264,   265,   266,   267,   281,   293,   402,
     403,   407,   319,   319,   319,   189,   451,   319,   319,   319,
     258,   259,   260,   261,   262,   263,   534,   535,   190,   191,
     192,   193,   538,   539,   247,   248,   250,   455,   315,   231,
     232,   505,   205,   206,   207,   208,   527,   528,   297,   319,
     187,   421,   424,   424,   181,   414,   416,   441,   181,   440,
     445,   483,   319,   170,   187,   509,   293,   347,   269,   280,
     269,   330,   330,   277,   277,   277,   279,   349,   356,   264,
     273,   272,   274,   331,   332,   269,   276,   279,   346,   390,
     391,   293,   269,   277,   264,   330,   426,   181,   424,   319,
     407,   319,   426,   319,   424,   424,   424,   319,   440,   319,
     517,   167,   168,   169,   424,   518,   519,   319,   517,   524,
     517,   424,   519,   319,   407,   319,   270,   407,   410,   315,
     316,   319,   425,   424,   319,   315,   133,   140,   317,   320,
     407,   319,   533,   319,   533,   167,   168,   169,   209,   210,
     211,   212,   213,   512,   513,   319,   293,   389,   409,   278,
     435,   277,   407,   198,   319,   167,   168,   169,   209,   210,
     211,   212,   213,   214,   215,   447,   448,   513,   143,   148,
     554,   277,   293,   199,   200,   201,   202,   400,   401,   407,
     252,   253,   254,   255,   256,   257,   503,   504,   407,   319,
     407,   407,   407,   319,   217,   456,   513,   505,   272,   319,
     407,   424,   319,   319,   424,   319,   319,   319,   407,   424,
     424,   319,   330,   264,   264,   373,   277,   277,   277,   360,
     273,   264,   277,   264,   266,   267,   270,   293,   296,   298,
     299,   300,   301,   302,   303,   304,   305,   264,   318,   269,
     279,   278,   396,   293,   277,   426,   407,   269,   407,   407,
     407,   407,   424,   319,   407,   525,   424,   525,   269,   319,
     295,   269,   223,   225,   226,   228,   230,   544,   545,   272,
     425,   319,   320,   407,   269,   410,   545,   407,   545,   533,
     407,   277,   406,   407,   433,   434,   293,   277,   319,   410,
     319,   319,   407,   269,   407,   269,   407,   269,   269,   269,
     407,   198,   407,   457,   319,   272,   293,   403,   404,   405,
     408,   525,   269,   319,   407,   407,   426,   407,   407,   407,
     269,   407,   269,   273,   270,   296,   298,   300,   301,   303,
     304,   269,   269,   269,   293,   294,   390,   279,   346,   394,
     395,   277,   269,   407,   269,   269,   269,   269,   319,   407,
     284,   269,   319,   269,   407,   407,   271,   293,   405,   410,
     493,   494,   321,   323,   324,   408,   407,   407,   269,   407,
     269,   323,   269,   323,   319,   269,   269,   279,   269,   437,
     319,   269,   407,   407,   269,   407,   269,   407,   269,   407,
     407,   407,   269,   319,   457,   277,   408,   282,   283,   273,
     269,   407,   407,   269,   269,   269,   269,   269,   407,   269,
     264,   296,   298,   300,   301,   303,   304,   271,   271,   271,
     271,   271,   271,   264,   293,   267,   266,   325,   393,   269,
     279,   407,   269,   407,   407,   407,   407,   319,   269,   407,
     407,   525,   407,   269,   269,   272,   277,   273,   269,   269,
     293,   405,   407,   467,   277,   272,   319,   272,   319,   244,
     245,   246,   546,   272,   433,   269,   278,   293,   277,   410,
     272,   269,   269,   407,   277,   407,   277,   407,   277,   269,
     269,   407,   319,   273,   264,   264,   277,   407,   269,   269,
     407,   407,   407,   407,   407,   277,   407,   271,   271,   271,
     271,   271,   271,   269,   269,   269,   269,   269,   269,   318,
     394,   269,   407,   269,   269,   269,   269,   407,   407,   269,
     269,   269,   407,   407,   264,   269,   407,   467,   277,   408,
     546,   407,   546,   272,   407,   279,   406,   436,   269,   408,
     272,   407,   277,   269,   277,   407,   407,   277,   410,   277,
     269,   407,   407,   277,   269,   269,   269,   269,   277,   270,
     270,   270,   270,   270,   270,   294,   407,   269,   407,   407,
     407,   407,   269,   269,   407,   407,   407,   269,   269,   273,
     410,   277,   277,   273,   410,   273,   272,   407,   273,   269,
     269,   279,   272,   273,   408,   277,   407,   269,   269,   269,
     407,   277,   277,   407,   407,   407,   407,   296,   270,   298,
     300,   270,   301,   303,   270,   304,   330,   277,   407,   277,
     269,   277,   269,   407,   407,   277,   269,   277,   407,   407,
     277,   277,   269,   277,   407,   269,   277,   293,   406,   269,
     407,   277,   273,   269,   407,   407,   272,   269,   277,   277,
     277,   269,   271,   271,   271,   271,   271,   271,   277,   407,
     407,   269,   277,   407,   277,   269,   272,   269,   410,   293,
     269,   269,   407,   269,   277,   407,   407,   407,   277,   277,
     407,   269,   407,   407,   410,   273,   410,   407,   277,   407,
     269,   277,   277,   269,   407,   277,   269,   273,   269,   273,
     269,   277,   277,   410,   407,   277,   410,   269,   410,   277,
     407,   273,   277,   273,   410,   277,   277,   277,   277,   277
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (&yylloc, lexer, state, YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (&yylval, &yylloc, YYLEX_PARAM)
#else
# define YYLEX yylex (&yylval, &yylloc, lexer, state)
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value, Location, lexer, state); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, parser::PTXLexer& lexer, parser::PTXParser::State& state)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp, lexer, state)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    YYLTYPE const * const yylocationp;
    parser::PTXLexer& lexer;
    parser::PTXParser::State& state;
#endif
{
  if (!yyvaluep)
    return;
  YYUSE (yylocationp);
  YYUSE (lexer);
  YYUSE (state);
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, parser::PTXLexer& lexer, parser::PTXParser::State& state)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep, yylocationp, lexer, state)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    YYLTYPE const * const yylocationp;
    parser::PTXLexer& lexer;
    parser::PTXParser::State& state;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp, lexer, state);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule, parser::PTXLexer& lexer, parser::PTXParser::State& state)
#else
static void
yy_reduce_print (yyvsp, yylsp, yyrule, lexer, state)
    YYSTYPE *yyvsp;
    YYLTYPE *yylsp;
    int yyrule;
    parser::PTXLexer& lexer;
    parser::PTXParser::State& state;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       , &(yylsp[(yyi + 1) - (yynrhs)])		       , lexer, state);
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, yylsp, Rule, lexer, state); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, parser::PTXLexer& lexer, parser::PTXParser::State& state)
#else
static void
yydestruct (yymsg, yytype, yyvaluep, yylocationp, lexer, state)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
    YYLTYPE *yylocationp;
    parser::PTXLexer& lexer;
    parser::PTXParser::State& state;
#endif
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  YYUSE (lexer);
  YYUSE (state);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (parser::PTXLexer& lexer, parser::PTXParser::State& state);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */






/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (parser::PTXLexer& lexer, parser::PTXParser::State& state)
#else
int
yyparse (lexer, state)
    parser::PTXLexer& lexer;
    parser::PTXParser::State& state;
#endif
#endif
{
  /* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;
/* Location data for the look-ahead symbol.  */
YYLTYPE yylloc;

  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;

  /* The location stack.  */
  YYLTYPE yylsa[YYINITDEPTH];
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;
  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[2];

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;
  yylsp = yyls;
#if YYLTYPE_IS_TRIVIAL
  /* Initialize the default location before parsing starts.  */
  yylloc.first_line   = yylloc.last_line   = 1;
  yylloc.first_column = yylloc.last_column = 0;
#endif

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;
	YYLTYPE *yyls1 = yyls;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);
	yyls = yyls1;
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);
	YYSTACK_RELOCATE (yyls);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 13:
#line 156 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.statementEnd( (yylsp[(1) - (1)]) );
;}
    break;

  case 29:
#line 170 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.preprocessor( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 30:
#line 175 "ocelot/parser/implementation/ptxgrammar.yy"
    { 
	state.version( (yyvsp[(2) - (2)].doubleFloat), (yylsp[(2) - (2)]) );
;}
    break;

  case 36:
#line 183 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.identifierList( (yyvsp[(1) - (1)].text) );
;}
    break;

  case 37:
#line 188 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.identifierList2( (yyvsp[(3) - (3)].text) );
;}
    break;

  case 38:
#line 193 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.decimalListSingle( 0 );
	state.symbolListSingle( (yyvsp[(1) - (1)].text) );
;}
    break;

  case 39:
#line 199 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.decimalListSingle2( 0 );
	state.symbolListSingle2( (yyvsp[(3) - (3)].text) );
;}
    break;

  case 40:
#line 205 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.decimalListSingle( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 41:
#line 210 "ocelot/parser/implementation/ptxgrammar.yy"
    {
    state.metadata("");
;}
    break;

  case 42:
#line 215 "ocelot/parser/implementation/ptxgrammar.yy"
    {
    state.metadata( (yyvsp[(1) - (1)].text) );
;}
    break;

  case 43:
#line 220 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.decimalListSingle2( (yyvsp[(3) - (3)].value) );
;}
    break;

  case 50:
#line 231 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.floatList( (yyvsp[(1) - (1)].doubleFloat) );
;}
    break;

  case 51:
#line 236 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.floatList1( (yyvsp[(3) - (3)].doubleFloat) );
;}
    break;

  case 58:
#line 247 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.singleList( (yyvsp[(1) - (1)].singleFloat) );
;}
    break;

  case 59:
#line 252 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.singleList1( (yyvsp[(3) - (3)].singleFloat) );
;}
    break;

  case 80:
#line 270 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.targetElement( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 84:
#line 278 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.target();
;}
    break;

  case 85:
#line 283 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.addressSize( (yyvsp[(2) - (2)].value) );
;}
    break;

  case 91:
#line 291 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.addressSpace( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 93:
#line 297 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.noAddressSpace();
;}
    break;

  case 112:
#line 308 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.dataType( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 113:
#line 313 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.dataType( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 116:
#line 320 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.statementVectorType( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 117:
#line 325 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instructionVectorType( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 120:
#line 332 "ocelot/parser/implementation/ptxgrammar.yy"
    { state.alignment = (yyvsp[(2) - (2)].value); ;}
    break;

  case 125:
#line 336 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.paramArgumentDeclaration((yyvsp[(2) - (2)].value));
;}
    break;

  case 126:
#line 340 "ocelot/parser/implementation/ptxgrammar.yy"
    {state.alignment = 1;;}
    break;

  case 127:
#line 341 "ocelot/parser/implementation/ptxgrammar.yy"
    {state.alignment = 1;;}
    break;

  case 128:
#line 342 "ocelot/parser/implementation/ptxgrammar.yy"
    { state.alignment = 1; ;}
    break;

  case 138:
#line 354 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.arrayDimensionSet( (yyvsp[(2) - (3)].value), (yylsp[(2) - (3)]), false );
;}
    break;

  case 139:
#line 359 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.arrayDimensionSet( (yyvsp[(3) - (4)].value), (yylsp[(3) - (4)]), true );
;}
    break;

  case 140:
#line 364 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.arrayDimensionSet( );
;}
    break;

  case 141:
#line 369 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.arrayDimensions();
;}
    break;

  case 144:
#line 378 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.assignment();
;}
    break;

  case 148:
#line 386 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.registerDeclaration( (yyvsp[(1) - (1)].text), (yylsp[(1) - (1)]) );
;}
    break;

  case 149:
#line 391 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.registerDeclaration( (yyvsp[(1) - (4)].text), (yylsp[(1) - (4)]), (yyvsp[(3) - (4)].value) );
;}
    break;

  case 150:
#line 396 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.registerSeperator( (yylsp[(1) - (1)]) );
;}
    break;

  case 151:
#line 401 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.registerDeclaration( (yyvsp[(3) - (3)].text), (yylsp[(3) - (3)]) );
;}
    break;

  case 157:
#line 416 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.fileDeclaration( (yyvsp[(2) - (4)].value), (yyvsp[(3) - (4)].text) );
;}
    break;

  case 158:
#line 422 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.locationAddress( (yyvsp[(2) - (6)].value) );
	state.initializableDeclaration( (yyvsp[(4) - (6)].text), (yylsp[(4) - (6)]), (yylsp[(6) - (6)]) );
;}
    break;

  case 159:
#line 429 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.initializableDeclaration( (yyvsp[(3) - (6)].text), (yylsp[(3) - (6)]), (yylsp[(5) - (6)]) );
	state.statementEnd( (yylsp[(3) - (6)]) );
;}
    break;

  case 160:
#line 436 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.locationAddress( (yyvsp[(2) - (6)].value) );
	state.initializableDeclaration( (yyvsp[(4) - (6)].text), (yylsp[(4) - (6)]), (yylsp[(6) - (6)]) );
;}
    break;

  case 163:
#line 444 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.textureDeclaration( (yyvsp[(2) - (5)].value), (yyvsp[(4) - (5)].text), (yylsp[(1) - (5)]) );
;}
    break;

  case 164:
#line 449 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.samplerDeclaration( (yyvsp[(2) - (5)].value), (yyvsp[(4) - (5)].text), (yylsp[(1) - (5)]) );
;}
    break;

  case 165:
#line 454 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.surfaceDeclaration( (yyvsp[(2) - (5)].value), (yyvsp[(4) - (5)].text), (yylsp[(1) - (5)]) );
;}
    break;

  case 166:
#line 459 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.locationAddress( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 167:
#line 464 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.locationAddress( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 168:
#line 470 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.attribute( false, false, false );
	state.argumentDeclaration( (yyvsp[(3) - (4)].text), (yylsp[(1) - (4)]) );
;}
    break;

  case 169:
#line 476 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.returnArgumentListBegin( (yylsp[(1) - (1)]) );
;}
    break;

  case 170:
#line 481 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.returnArgumentListEnd( (yylsp[(1) - (1)]) );
;}
    break;

  case 171:
#line 486 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.argumentListBegin( (yylsp[(1) - (1)]) );
;}
    break;

  case 172:
#line 491 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.argumentListEnd( (yylsp[(1) - (1)]) );
;}
    break;

  case 173:
#line 496 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.openBrace( (yylsp[(1) - (1)]) );
;}
    break;

  case 174:
#line 501 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.closeBrace( (yylsp[(1) - (2)]) );
;}
    break;

  case 182:
#line 516 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.functionBegin( (yylsp[(1) - (1)]) );
;}
    break;

  case 183:
#line 521 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.functionName( (yyvsp[(1) - (1)].text), (yylsp[(1) - (1)]) );
;}
    break;

  case 186:
#line 530 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.functionDeclaration( (yylsp[(4) - (6)]), false );
;}
    break;

  case 187:
#line 536 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.functionDeclaration( (yylsp[(4) - (5)]), true );
;}
    break;

  case 189:
#line 543 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.entry( (yyvsp[(3) - (3)].text), (yylsp[(1) - (3)]) );
;}
    break;

  case 192:
#line 551 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.entryDeclaration( (yylsp[(1) - (3)]) );
;}
    break;

  case 195:
#line 560 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.entryPrototype( (yylsp[(1) - (2)]) );
;}
    break;

  case 203:
#line 570 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.statementEnd( (yylsp[(1) - (1)]) );
;}
    break;

  case 204:
#line 575 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.entryStatement( (yylsp[(2) - (3)]) );
	state.instruction();
;}
    break;

  case 208:
#line 586 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.maxnreg( (yyvsp[(2) - (2)].value) );
;}
    break;

  case 209:
#line 591 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.maxntid( (yyvsp[(2) - (2)].value) );
;}
    break;

  case 210:
#line 596 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.maxntid( (yyvsp[(2) - (4)].value), (yyvsp[(4) - (4)].value) );
;}
    break;

  case 211:
#line 602 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.maxntid( (yyvsp[(2) - (6)].value), (yyvsp[(4) - (6)].value), (yyvsp[(6) - (6)].value) );
;}
    break;

  case 212:
#line 607 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.ctapersm( (yyvsp[(1) - (3)].value), (yyvsp[(3) - (3)].value) );
;}
    break;

  case 215:
#line 614 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.minnctapersm( (yyvsp[(2) - (2)].value) );
;}
    break;

  case 216:
#line 619 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.minnctapersm( );
;}
    break;

  case 217:
#line 624 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.maxnctapersm( (yyvsp[(2) - (2)].value) );
;}
    break;

  case 218:
#line 629 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.maxnctapersm();
;}
    break;

  case 227:
#line 639 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.attribute( false, false, true );
;}
    break;

  case 228:
#line 644 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.attribute( false, true, false );
;}
    break;

  case 229:
#line 649 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.attribute( true, false, false );
;}
    break;

  case 230:
#line 654 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.attribute( false, false, false );
;}
    break;

  case 236:
#line 662 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.locationAddress( (yyvsp[(2) - (2)].value) );
;}
    break;

  case 237:
#line 667 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.locationAddress( (yyvsp[(2) - (2)].value) );
;}
    break;

  case 309:
#line 690 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.uninitializableDeclaration( (yyvsp[(3) - (5)].text) );
	state.statementEnd( (yylsp[(2) - (5)]) );
;}
    break;

  case 310:
#line 697 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.location( (yyvsp[(2) - (4)].value), (yyvsp[(3) - (4)].value), (yyvsp[(4) - (4)].value) );
;}
    break;

  case 311:
#line 702 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.label( (yyvsp[(1) - (2)].text) );
;}
    break;

  case 312:
#line 707 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.labelOperand( (yyvsp[(1) - (1)].text) );
;}
    break;

  case 313:
#line 712 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.returnType( (yyvsp[(2) - (3)].value) );
;}
    break;

  case 321:
#line 724 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.argumentType( (yyvsp[(3) - (5)].value) );
;}
    break;

  case 326:
#line 734 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.callPrototype( (yyvsp[(1) - (6)].text), (yyvsp[(4) - (6)].text), (yylsp[(1) - (6)]) );
;}
    break;

  case 327:
#line 739 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.callTargets( (yyvsp[(1) - (4)].text), (yylsp[(1) - (4)]) );
;}
    break;

  case 328:
#line 744 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.pragma( (yyvsp[(2) - (2)].text) );
;}
    break;

  case 329:
#line 749 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.pragma( (yyvsp[(2) - (3)].text) );
;}
    break;

  case 334:
#line 756 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.vectorIndex( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 335:
#line 761 "ocelot/parser/implementation/ptxgrammar.yy"
    {

;}
    break;

  case 336:
#line 766 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.nonLabelOperand( (yyvsp[(1) - (2)].text), (yylsp[(1) - (2)]), false );
;}
    break;

  case 337:
#line 771 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.nonLabelOperand( (yyvsp[(2) - (2)].text), (yylsp[(1) - (2)]), true );
;}
    break;

  case 338:
#line 776 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.constantOperand( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 339:
#line 781 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.constantOperand( (yyvsp[(1) - (1)].uvalue) );
;}
    break;

  case 340:
#line 786 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.constantOperand( (yyvsp[(1) - (1)].doubleFloat) );
;}
    break;

  case 341:
#line 791 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.constantOperand( (yyvsp[(1) - (1)].singleFloat) );
;}
    break;

  case 342:
#line 796 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.addressableOperand( (yyvsp[(1) - (1)].text), 0, (yylsp[(1) - (1)]), false );
;}
    break;

  case 343:
#line 801 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.addressableOperand( (yyvsp[(1) - (3)].text), (yyvsp[(3) - (3)].value), (yylsp[(1) - (3)]), false );
;}
    break;

  case 344:
#line 806 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.addressableOperand( (yyvsp[(1) - (3)].text), (yyvsp[(3) - (3)].value), (yylsp[(1) - (3)]), true );
;}
    break;

  case 353:
#line 819 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.arrayOperand( (yylsp[(1) - (3)]) );
;}
    break;

  case 354:
#line 824 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.guard( (yyvsp[(1) - (1)].text), (yylsp[(1) - (1)]), false );
;}
    break;

  case 355:
#line 829 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.guard( (yyvsp[(1) - (1)].text), (yylsp[(1) - (1)]), true );
;}
    break;

  case 356:
#line 834 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.guard();
;}
    break;

  case 365:
#line 842 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 366:
#line 847 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 431:
#line 866 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (8)].text), (yyvsp[(2) - (8)].value) );
;}
    break;

  case 437:
#line 874 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 438:
#line 880 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (8)].text), (yyvsp[(4) - (8)].value) );
;}
    break;

  case 439:
#line 885 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 442:
#line 892 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 447:
#line 902 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (7)].text), (yyvsp[(3) - (7)].value) );
;}
    break;

  case 450:
#line 910 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (9)].text), (yyvsp[(3) - (9)].value) );
;}
    break;

  case 451:
#line 915 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.uni( false );
;}
    break;

  case 452:
#line 920 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.uni( true );
;}
    break;

  case 453:
#line 925 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (4)].text) );
;}
    break;

  case 454:
#line 930 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.returnOperand();
;}
    break;

  case 461:
#line 943 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.callPrototypeName( (yyvsp[(6) - (6)].text) );
;}
    break;

  case 462:
#line 948 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.callPrototypeName( (yyvsp[(5) - (5)].text) );
;}
    break;

  case 463:
#line 953 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.callPrototypeName( (yyvsp[(2) - (2)].text) );
;}
    break;

  case 467:
#line 961 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.tail( true );
;}
    break;

  case 468:
#line 966 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.tail( false );
;}
    break;

  case 470:
#line 974 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.call( (yyvsp[(4) - (6)].text), (yylsp[(1) - (6)]) );
;}
    break;

  case 471:
#line 979 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.carry( true );
;}
    break;

  case 472:
#line 984 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.carry( false );
;}
    break;

  case 473:
#line 989 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.carry( true );
;}
    break;

  case 474:
#line 994 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.carry( false );
;}
    break;

  case 477:
#line 1002 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (9)].text), (yyvsp[(3) - (9)].value) );
;}
    break;

  case 481:
#line 1012 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.carryIn();
	state.instruction( (yyvsp[(1) - (9)].text), (yyvsp[(3) - (9)].value) );
;}
    break;

  case 492:
#line 1021 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.atomic( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 494:
#line 1027 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.addressSpace(TOKEN_GLOBAL);
;}
    break;

  case 495:
#line 1033 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (12)].text), (yyvsp[(4) - (12)].value) );
;}
    break;

  case 496:
#line 1039 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (14)].text), (yyvsp[(4) - (14)].value) );
;}
    break;

  case 497:
#line 1044 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.shiftAmount( true );
;}
    break;

  case 498:
#line 1049 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.shiftAmount( false );
;}
    break;

  case 499:
#line 1055 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (10)].text), (yyvsp[(2) - (10)].value) );
;}
    break;

  case 500:
#line 1061 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (12)].text), (yyvsp[(2) - (12)].value) );
;}
    break;

  case 501:
#line 1066 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (7)].text), (yyvsp[(3) - (7)].value) );
;}
    break;

  case 504:
#line 1071 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.barrierOperation( (yyvsp[(1) - (1)].value), (yylsp[(1) - (1)]) );
;}
    break;

  case 509:
#line 1080 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (5)].text) );
;}
    break;

  case 510:
#line 1085 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (6)].text), (yyvsp[(2) - (6)].value) );
;}
    break;

  case 511:
#line 1090 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (2)].text) );
;}
    break;

  case 512:
#line 1095 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (6)].text), (yyvsp[(2) - (6)].value) );
;}
    break;

  case 513:
#line 1100 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 514:
#line 1105 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 521:
#line 1117 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (8)].text), (yyvsp[(3) - (8)].value) );
	state.relaxedConvert( (yyvsp[(4) - (8)].value), (yylsp[(1) - (8)]) );
;}
    break;

  case 524:
#line 1125 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (7)].text), (yyvsp[(3) - (7)].value) );
;}
    break;

  case 525:
#line 1131 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (8)].text), (yyvsp[(4) - (8)].value) );
	state.cvtaTo();
;}
    break;

  case 526:
#line 1141 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.full();
;}
    break;

  case 527:
#line 1146 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier((yyvsp[(1) - (2)].value));
;}
    break;

  case 528:
#line 1151 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier((yyvsp[(1) - (2)].value));
;}
    break;

  case 533:
#line 1161 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (9)].text), (yyvsp[(3) - (9)].value) );
;}
    break;

  case 534:
#line 1166 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (2)].text) );
;}
    break;

  case 535:
#line 1171 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (6)].text), TOKEN_U32 );
;}
    break;

  case 536:
#line 1176 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.volatileFlag( true );
;}
    break;

  case 538:
#line 1183 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.volatileFlag( false );
;}
    break;

  case 540:
#line 1191 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (9)].text), (yyvsp[(3) - (9)].value) );
;}
    break;

  case 541:
#line 1196 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (9)].text), (yyvsp[(3) - (9)].value) );
;}
    break;

  case 547:
#line 1205 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier( (yyvsp[(1) - (3)].value) );
	state.carry( false );
;}
    break;

  case 548:
#line 1211 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.carry( true );
;}
    break;

  case 549:
#line 1216 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.carry( false );
;}
    break;

  case 552:
#line 1224 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (11)].text), (yyvsp[(3) - (11)].value) );
;}
    break;

  case 554:
#line 1231 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier( (yyvsp[(1) - (2)].value) );
;}
    break;

  case 555:
#line 1237 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (11)].text), (yyvsp[(3) - (11)].value) );
;}
    break;

  case 556:
#line 1242 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier( (yyvsp[(1) - (2)].value) );
;}
    break;

  case 557:
#line 1248 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (11)].text), (yyvsp[(3) - (11)].value) );
;}
    break;

  case 561:
#line 1255 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.level( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 562:
#line 1260 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (3)].text) );
;}
    break;

  case 563:
#line 1265 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.indexedOperand( (yyvsp[(1) - (4)].text), (yylsp[(1) - (4)]), (yyvsp[(3) - (4)].value) );
;}
    break;

  case 567:
#line 1272 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (6)].text), (yyvsp[(2) - (6)].value) );
;}
    break;

  case 569:
#line 1279 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 570:
#line 1284 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (9)].text), (yyvsp[(3) - (9)].value) );
;}
    break;

  case 571:
#line 1289 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (9)].text), (yyvsp[(3) - (9)].value) );
;}
    break;

  case 574:
#line 1296 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (6)].text), (yyvsp[(2) - (6)].value) );
;}
    break;

  case 575:
#line 1301 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (2)].text) );
;}
    break;

  case 576:
#line 1306 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (6)].text), (yyvsp[(2) - (6)].value) );
;}
    break;

  case 583:
#line 1314 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.permute( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 584:
#line 1319 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.defaultPermute();
;}
    break;

  case 586:
#line 1324 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.cacheLevel( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 587:
#line 1329 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (7)].text) );
;}
    break;

  case 588:
#line 1334 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (6)].text) );
;}
    break;

  case 589:
#line 1340 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (11)].text), (yyvsp[(2) - (11)].value) );
;}
    break;

  case 590:
#line 1345 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier( (yyvsp[(1) - (2)].value) );
;}
    break;

  case 592:
#line 1351 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.modifier( (yyvsp[(1) - (2)].value) );
;}
    break;

  case 595:
#line 1359 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (7)].text), (yyvsp[(3) - (7)].value) );
;}
    break;

  case 604:
#line 1367 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.reduction( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 605:
#line 1373 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (8)].text), (yyvsp[(4) - (8)].value) );
;}
    break;

  case 606:
#line 1378 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (3)].text) );
;}
    break;

  case 607:
#line 1383 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (4)].text) );
;}
    break;

  case 626:
#line 1392 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.comparison( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 630:
#line 1399 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.boolean( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 631:
#line 1404 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (10)].text), (yyvsp[(2) - (10)].value) );
;}
    break;

  case 632:
#line 1409 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (10)].text), (yyvsp[(2) - (10)].value) );
	state.operandCIsAPredicate();
;}
    break;

  case 635:
#line 1421 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (10)].text), (yyvsp[(3) - (10)].value) );
	state.convert( (yyvsp[(4) - (10)].value), (yylsp[(1) - (10)]) );
;}
    break;

  case 636:
#line 1428 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (14)].text), (yyvsp[(5) - (14)].value) );
	state.convert( (yyvsp[(6) - (14)].value), (yylsp[(1) - (14)]) );
;}
    break;

  case 641:
#line 1441 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (9)].text), (yyvsp[(3) - (9)].value) );
;}
    break;

  case 642:
#line 1447 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (9)].text), (yyvsp[(2) - (9)].value) );
;}
    break;

  case 643:
#line 1453 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (13)].text), (yyvsp[(5) - (13)].value) );
;}
    break;

  case 648:
#line 1460 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.shuffle( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 649:
#line 1466 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (11)].text), (yyvsp[(3) - (11)].value) );
;}
    break;

  case 650:
#line 1472 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (12)].text), (yyvsp[(3) - (12)].value) );
	state.convertC( (yyvsp[(4) - (12)].value), (yylsp[(1) - (12)]) );
;}
    break;

  case 651:
#line 1478 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (9)].text), (yyvsp[(3) - (9)].value) );
;}
    break;

  case 659:
#line 1486 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.geometry( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 666:
#line 1494 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.floatingPointMode( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 667:
#line 1499 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (7)].text), (yyvsp[(3) - (7)].value) );
;}
    break;

  case 668:
#line 1505 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.tex( (yyvsp[(5) - (13)].value) );
	state.convertD( (yyvsp[(4) - (13)].value), (yylsp[(1) - (13)]) );
;}
    break;

  case 673:
#line 1513 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.colorComponent( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 674:
#line 1519 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.tld4( (yyvsp[(5) - (14)].value) );
;}
    break;

  case 684:
#line 1531 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.surfaceQuery( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 685:
#line 1536 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.surfaceQuery( (yyvsp[(2) - (9)].value) );
	state.instruction( (yyvsp[(1) - (9)].text), (yyvsp[(3) - (9)].value) );
;}
    break;

  case 686:
#line 1542 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (9)].text), (yyvsp[(3) - (9)].value) );
	state.surfaceQuery( (yyvsp[(2) - (9)].value) );
;}
    break;

  case 691:
#line 1548 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.cacheOperation( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 696:
#line 1555 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.clampOperation( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 698:
#line 1560 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.formatMode( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 699:
#line 1567 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (15)].text), (yyvsp[(6) - (15)].value) );
	state.formatMode( (yyvsp[(2) - (15)].value) );
	state.clampOperation( (yyvsp[(7) - (15)].value) );
;}
    break;

  case 700:
#line 1576 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (15)].text), (yyvsp[(6) - (15)].value) );
	state.formatMode( (yyvsp[(2) - (15)].value) );
	state.clampOperation( (yyvsp[(7) - (15)].value) );
;}
    break;

  case 701:
#line 1584 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (14)].text), (yyvsp[(5) - (14)].value) );
	state.formatMode( (yyvsp[(2) - (14)].value) );
	state.clampOperation( (yyvsp[(6) - (14)].value) );
;}
    break;

  case 702:
#line 1595 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (2)].text) );
;}
    break;

  case 707:
#line 1602 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.vote( (yyvsp[(1) - (1)].value) );
;}
    break;

  case 710:
#line 1609 "ocelot/parser/implementation/ptxgrammar.yy"
    {
	state.instruction( (yyvsp[(1) - (7)].text), (yyvsp[(3) - (7)].value) );
;}
    break;


/* Line 1267 of yacc.c.  */
#line 5239 ".release_build/ptxgrammar.cpp"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (&yylloc, lexer, state, YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (&yylloc, lexer, state, yymsg);
	  }
	else
	  {
	    yyerror (&yylloc, lexer, state, YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }

  yyerror_range[0] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval, &yylloc, lexer, state);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[0] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      yyerror_range[0] = *yylsp;
      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp, yylsp, lexer, state);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;

  yyerror_range[1] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the look-ahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, (yyerror_range - 1), 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, lexer, state, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval, &yylloc, lexer, state);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp, yylsp, lexer, state);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


#line 1613 "ocelot/parser/implementation/ptxgrammar.yy"


int yylex( YYSTYPE* token, YYLTYPE* location, parser::PTXLexer& lexer, 
	parser::PTXParser::State& state )
{
	lexer.yylval = token;
	
	int tokenValue         = lexer.yylexPosition();
	location->first_line   = lexer.lineno();
	location->first_column = lexer.column;
	
	report( " Lexer (" << location->first_line << ","
		<< location->first_column 
		<< "): " << parser::PTXLexer::toString( tokenValue ) << " \"" 
		<< lexer.YYText() << "\"");
	
	return tokenValue;
}

void yyerror( YYLTYPE* location, parser::PTXLexer& lexer, 
	parser::PTXParser::State& state, char const* message )
{
	parser::PTXParser::Exception exception;
	std::stringstream stream;
	stream << parser::PTXParser::toString( *location, state ) 
		<< " " << message;
	exception.message = stream.str();
	exception.error = parser::PTXParser::State::SyntaxError;
	throw exception;
}

}

