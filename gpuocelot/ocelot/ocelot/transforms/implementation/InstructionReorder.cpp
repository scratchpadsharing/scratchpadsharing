
#include "ocelot/ir/interface/ControlFlowGraph.h"
#include "ocelot/analysis/interface/DataflowGraph.h"
#include "ocelot/transforms/interface/InstructionReorder.h"
#include "ocelot/analysis/interface/DivergenceAnalysis.h"
#include "ocelot/analysis/interface/SSAGraph.h"
#include "ocelot/ir/interface/PTXOperand.h"
#include <ocelot/analysis/interface/PostdominatorTree.h>
#include <ocelot/analysis/interface/DominatorTree.h>

#include<iostream>

typedef analysis::DataflowGraph::InstructionVector   InstructionVector;
 
  
 namespace transforms {
 
    InstructionReorderPass::InstructionReorderPass()
      : KernelPass({"DataflowGraphAnalysis", "LoopAnalysis", "DominatorTreeAnalysis", "PostDominatorTreeAnalysis"}, "InstructionReorderPass") {
    }
 
    void InstructionReorderPass::runOnKernel( ir::IRKernel& k ) {
       
        std::cout << "kernel name is " << k.name; 
        
        
    Analysis* dfg_structure = getAnalysis("DataflowGraphAnalysis");
    assert(dfg_structure != 0);
      
    analysis::DataflowGraph& dfg =
		*static_cast<analysis::DataflowGraph*>(dfg_structure);
    
    analysis::DataflowGraph::iterator block = dfg.begin();
    analysis::DataflowGraph::iterator blockEnd = dfg.end();     
     
    auto cfg = k.cfg();     
     
    for (; block != blockEnd; block++) {     
      // std::cout << "\n--------------- Start Block: " <<  block->label() << " ---------------\n";
       InstructionVector::iterator next =  block->instructions().begin();
       InstructionVector::iterator end =  block->instructions().end();
       int inst_count=0;
       
        while(next!= end) {
            ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(next->i); 
            if(inst->opcode == ir::PTXInstruction::Bar ) { 
                if(inst_count != 0 ) {
                    block = dfg.split(block,next,true);
                }
                block = dfg.split(block,1,true);
                block--;
                break;
            }
            next++;
            inst_count++;
       }
       //std::cout << "--------------- End Block: ---------------\n";             
    }
    //Graph g(inst_count);
    
    block = dfg.begin();
    blockEnd = dfg.end();    
    for (; block != blockEnd; block++) {     
       std::cout << "\nBlock:" << block->label() << "\n";
       InstructionVector::iterator next =  block->instructions().begin();
       InstructionVector::iterator end =  block->instructions().end();
       
       
       InstructionReorderPass::Graph g(block->instructions().size());
       
       int inst_count = 0;
       while (next != end) {
            ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(next->i);            
            insert_dependency_edges(inst, inst_count, &g);
            next++; 
            inst_count++;
       }
       readMap.clear();
       writeMap.clear(); 
       
    }  
    
    ir::ControlFlowGraph::edge_iterator edge; 
    for (edge = cfg->edges_begin(); 
               edge != cfg->edges_end(); ++edge) {
               std::cout << edge->head->label() << " -> " << edge->tail->label();
               //out << " " << blockFormatter.toString(edgePtr);
               std::cout << "\n";
    }
    
  }
  void InstructionReorderPass::insert_dependency_edges(ir::PTXInstruction* inst, int inst_count, Graph *g) {
      
    ir::PTXOperand* operands[] = {&inst->a, &inst->b, &inst->c, &inst->d, &inst->pg};
    std::cout << "I_" << inst_count << std::endl;

    //std::cout << "Instruction I" << inst_count << ": "<< inst->toString()<< std::endl;
    for(int i =0; i<4; i++) {                
        if (operands[i]->isRegister()) {
            /* Handling Register Operands*/
            if(i==3) {
                //std::cout << "Destination is " << operands[i]->reg << std::endl;
                /* Inserting WAW edges */
                writemap_iterator wt =writeMap.find(operands[i]->reg);
                if(wt !=writeMap.end()) {
                    for(std::list<int>::iterator it= wt->second.begin(); it!= wt->second.end(); ++it) {
                        //std::cout<< "WAW Dependency: " << *it << "--> " << inst_count << std::endl;
                        std::cout<< "I_" << *it << "-> I_" << inst_count << std::endl;
                        g->addEdge(*it, inst_count);
                        g->compute_height(inst_count, 0);
                        
                    }
                    wt->second.push_back(inst_count);
                    wt->second.unique();
                }
                else {
                    std::list<int> writeList;
                    writeList.push_back(inst_count);
                    writeMap[operands[i]->reg]=writeList;
                }
                /* Inserting WAR edges */
                readmap_iterator rt =readMap.find(operands[i]->reg); 
                if(rt!= readMap.end()) {
                    for(std::list<int>::iterator it= rt->second.begin(); it!= rt->second.end(); ++it) {
                        if(inst_count != *it) {
                            // std::cout<< "WAR Dependency: " << *it << "--> " << inst_count << std::endl;
                            std::cout<< "I_" << *it << "-> I_" << inst_count << std::endl;
                            g->addEdge(*it, inst_count);
                            g->compute_height(inst_count, 0);
                        }
                    }
                }
            }
            else {
                //std::cout << "Source is " << operands[i]->reg << std::endl;

                /* Addining RAW Dependency edges*/
                writemap_iterator wt =writeMap.find(operands[i]->reg);                        
                if(wt !=writeMap.end()) {
                    for(std::list<int>::iterator it= wt->second.begin(); it!= wt->second.end(); ++it) {
                        //std::cout<< "RAW Dependency :" << *it << "--> " << inst_count << std::endl;
                        std::cout<< "I_" << *it << "-> I_" << inst_count << std::endl;
                        g->addEdge(*it, inst_count);
                        g->compute_height(inst_count, 0);
                    }

                }
                /* Inserting the instruction which reads a register */
                readmap_iterator rt =readMap.find(operands[i]->reg);                        
                if(rt!= readMap.end()) {
                    rt->second.push_back(inst_count);
                    rt->second.unique();
                }
                else {
                    std::list<int> readList;
                    readList.push_back(inst_count);
                    readMap[operands[i]->reg]=readList;
                }
            }

        }
        if (operands[i]->isVector()) {
            /* Handing Vector Operands */
            if(i==3) {
                for (ir::PTXOperand::Array::iterator at = operands[i]->array.begin(); at != operands[i]->array.end(); ++at) {

                    /* Inserting WAW edges */
                    writemap_iterator wt =writeMap.find( at->reg);
                    if(wt !=writeMap.end()) {
                        for(std::list<int>::iterator it= wt->second.begin(); it!= wt->second.end(); ++it) {
                            //std::cout<< "WAW Dependency: " << *it << "--> " << inst_count << std::endl;
                            std::cout<< "I_" << *it << "-> I_" << inst_count << std::endl;
                            g->addEdge(*it, inst_count);
                            g->compute_height(inst_count, 0);
                        }
                        wt->second.push_back(inst_count);
                        wt->second.unique();
                    }
                    else {
                        std::list<int> writeList;
                        writeList.push_back(inst_count);
                        writeMap[at->reg]=writeList;
                    }
                    /* Inserting WAR edges */
                    readmap_iterator rt =readMap.find(at->reg); 
                    if(rt!= readMap.end()) {
                        for(std::list<int>::iterator it= rt->second.begin(); it!= rt->second.end(); ++it) {
                            if(inst_count != *it) {
                                //std::cout<< "WAR Dependency: " << *it << "--> " << inst_count << std::endl;
                                std::cout<< "I_" << *it << "-> I_" << inst_count << std::endl;
                                g->addEdge(*it, inst_count);
                                g->compute_height(inst_count, 0);
                            }
                        }
                    }
                }

            }
            else {
                for (ir::PTXOperand::Array::iterator at = operands[i]->array.begin(); at != operands[i]->array.end(); ++at) {

                    /* Addining RAW Dependency edges*/
                    writemap_iterator wt =writeMap.find(at->reg);                        
                    if(wt !=writeMap.end()) {
                        for(std::list<int>::iterator it= wt->second.begin(); it!= wt->second.end(); ++it) {
                            //std::cout<< "RAW Dependency :" << *it << "--> " << inst_count << std::endl;
                            std::cout<< "I_" << *it << "-> I_" << inst_count << std::endl;
                            g->addEdge(*it, inst_count);
                            g->compute_height(inst_count, 0);
                        }
                    }
                    /* Inserting the instruction into read list */
                    readmap_iterator rt =readMap.find(at->reg);                        
                    if(rt!= readMap.end()) {
                        rt->second.push_back(inst_count);
                        rt->second.unique();
                    }
                    else {
                        std::list<int> readList;
                        readList.push_back(inst_count);
                        readMap[at->reg]=readList;
                    }
                }  
            }
        }
    }
} 

InstructionReorderPass::Graph::Graph(int V)
{
    this->V = V;
    adj = new std::list<int>[V];
    parent = new std::list<int>[V];
    height = new int[V];
    for(int i=0; i<V; i++) 
        height[i]=0;
}

void InstructionReorderPass::Graph::addEdge(int v, int w)
{   
    adj[v].push_back(w); 
    parent[w].push_back(v);
    adj[v].unique(); // removing duplicate edges *
    parent[w].unique(); // removing duplicate parent information 
}

void InstructionReorderPass::Graph::compute_height(int v, int h) {            
    height[v] = h;        
    std::list<int>::iterator i;
    for (i = parent[v].begin(); i != parent[v].end(); ++i) {
        compute_height(*i,h+1);
    }
}
void InstructionReorderPass::Graph::print_graph() {
    
    printf("\n************* Graph ************ \n");
    for(int i=0; i<V; i++)
    {    
        std::cout << "Child[" << i << "]: " ;
        std::list<int>::iterator it;
        for (it =adj[i].begin(); it != adj[i].end(); ++it) {
            std::cout << *it << " ";
        }
        printf("\n");
    }
    printf("\n");
    
    for(int i=0; i<V; i++)
    {    
        std::cout << "Parent[" << i << "]: " ;
        std::list<int>::iterator it;
        for (it = parent[i].begin(); it != parent[i].end(); ++it) {
            std::cout << *it << " ";
        }
        printf("\n");
    }
    printf("\n");
    for(int i=0; i<V; i++)
    {   
        std::cout << "height["<<i<<"] " << height[i] << std::endl;
    }
    printf("\n**************************** \n");
}
  
  
    
    
    
    
}
