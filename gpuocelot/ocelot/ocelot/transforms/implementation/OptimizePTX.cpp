#include "ocelot/ir/interface/ControlFlowGraph.h"
#include "ocelot/analysis/interface/DataflowGraph.h"
#include "ocelot/transforms/interface/OptimizePTX.h" 
#include "ocelot/analysis/interface/DivergenceAnalysis.h" 
#include "ocelot/analysis/interface/SSAGraph.h"
#include "ocelot/ir/interface/PTXOperand.h"
#include "ocelot/ir/interface/PTXInstruction.h"
#include "ocelot/ir/interface/ControlFlowGraph.h"
#include "ocelot/ir/interface/Kernel.h"
#include "ocelot/ir/interface/PTXKernel.h"
#include "ocelot/analysis/interface/LoopAnalysis.h"

#include <iostream>
#include <set>
#include <fstream>      // std::ifstream
#include <sstream> 
#include <cmath>
 
typedef analysis::DataflowGraph::InstructionVector   InstructionVector;
typedef analysis::DataflowGraph::BlockPointerSet       BlockPointerSet;

namespace transforms { 
    
    OptimizePTX::OptimizePTX() 
        : KernelPass({"DataflowGraphAnalysis", "DominatorTreeAnalysis", "LoopAnalysis", "PostDominatorTreeAnalysis"}, "OptimizePTX") {
    }
    void OptimizePTX::runOnKernel( ir::IRKernel& k ) {
        
        Analysis* dfg_structure = getAnalysis("DataflowGraphAnalysis");
        assert(dfg_structure != 0);    
      
        analysis::DataflowGraph& dfg = *static_cast<analysis::DataflowGraph*>(dfg_structure);
        
        auto loopAnalysis = static_cast<analysis::LoopAnalysis*>(getAnalysis("LoopAnalysis"));
        
        
        analysis::Analysis* postdomAnalysis = getAnalysis("PostDominatorTreeAnalysis");
        assert(postdomAnalysis != 0);
        analysis::PostdominatorTree* postdomTree = static_cast<analysis::PostdominatorTree*>(postdomAnalysis);
        
        analysis::DataflowGraph::iterator block = ++dfg.begin();
        analysis::DataflowGraph::iterator blockEnd = --dfg.end();
        
        for (; block != blockEnd; block++)                
        {       
                if(loopAnalysis->isContainedInLoop(block->getCFGblock()) ) {

                    std::string header_label = loopAnalysis->_getLoopAt(block->getCFGblock())->getHeader()->label();
                    //std::cout << "loop header is " << header_label << "\n";
                    if (block->label().compare(header_label) == 0) {
                        block->loop_count = LOOP_COUNT;
                    }
                }
        } 
        
        ssp = computeSSPSize(&k.name);
        computeShmemMap(dfg);
        collectSharedVars(&k, dfg);  
        computePossibleSubsets();
        transformPTX(dfg, *loopAnalysis, postdomTree, &k);  
        sharedVars.clear();
        sharedMap.clear();
        possibleSets.clear();
        
        //printDFG(dfg);        
    } 
    
    void OptimizePTX::reorderScratchpadAllocation(ir::IRKernel *ptxKernel, std::list<std::string>& sharedVars){
            ir::Kernel::LocalList::iterator it = ptxKernel->local_list.begin();
        
            std::list<std::string>::iterator str_it;

            ir::Kernel::LocalList tmp;
            
            for( ; it!=ptxKernel->local_list.end(); ++it) {                
                str_it = std::find (sharedVars.begin(), sharedVars.end(), it->first);                 
                if(str_it == sharedVars.end()) {
                   tmp.push_back( std::make_pair( it->first, it->second));                    
                }
            }
            
            it = ptxKernel->local_list.begin();
            for( ; it!=ptxKernel->local_list.end(); ++it) {                
                str_it = find (sharedVars.begin(), sharedVars.end(), it->first);                 
                if(str_it != sharedVars.end()) {
                    
                   tmp.push_back( std::make_pair( it->first, it->second));                    
                }
            }
            ptxKernel->local_list.clear(); 
            it = tmp.begin();
            for( ; it!=tmp.end(); ++it) {                
                    
                   ptxKernel->local_list.push_back( std::make_pair( it->first, it->second));                    
                
            }       
    }
    
    /* For each shared variable, the function the number of registers that are used accessed shared memory */
    void OptimizePTX::computeShmemMap(analysis::DataflowGraph& dfg)  {
        analysis::DataflowGraph::iterator block = ++dfg.begin(); 
        analysis::DataflowGraph::iterator blockEnd = --dfg.end(); 

        /* Traversing all the basic block */
        for ( ; block != blockEnd; block++) { 
           InstructionVector::iterator current =  block->instructions().begin();
           InstructionVector::iterator end =  block->instructions().end();

            /* Iterating over all the instructions */
            while (current != end) {      
                ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(current->i);            
                ir::PTXOperand* operands[] = {&inst->a, &inst->b, &inst->c, &inst->d, &inst->pg};
                
                /* If it is a mov instruction, insert destination register that is used for accessing shared mem var*/
                if(inst->opcode == ir::PTXInstruction::Mov ) {
                    if(inst->addressSpace == ir::PTXInstruction::Shared) {
                        if( operands[0]->addressMode == ir::PTXOperand::Address) {      
                            dfg.shmap[operands[0]->identifier].insert(operands[3]->reg);
                        }
                    }
                    else {
                        if (operands[0]->isRegister() && isfound(dfg, operands[0]->reg)) {
                            std::string key = getKey(dfg,operands[0]->reg);                        
                            dfg.shmap[key].insert(operands[3]->reg);
                        }
                    }
                }
                else if (inst->opcode == ir::PTXInstruction::Add || inst->opcode == ir::PTXInstruction::Mul || inst->opcode == ir::PTXInstruction::Sub) {                
                    for( int i=0; i<2; i++) {
                        if (operands[i]->isRegister() && isfound(dfg, operands[i]->reg)) {
                            std::string key = getKey(dfg,operands[i]->reg);                        
                            dfg.shmap[key].insert(operands[3]->reg);
                        }
                    }                
                }
                current++;      	
            }
        }  
        for(analysis::DataflowGraph::shmem_iterator ii=dfg.shmap.begin(); ii!=dfg.shmap.end(); ++ii)
        {                
            std::cout << (*ii).first << ": \n" ;
            analysis::DataflowGraph::RegisterIdSet::iterator iter;

            for(iter=(*ii).second.begin(); iter!=(*ii).second.end();++iter){            
                std::cout<<*iter;
                std::cout << "\n";
            }
        }
    }
    bool OptimizePTX::isfound(analysis::DataflowGraph& dfg, ir::PTXOperand::RegisterType reg) {        

        for(analysis::DataflowGraph::shmem_iterator ii=dfg.shmap.begin(); ii!=dfg.shmap.end(); ++ii)
        {     
            analysis::DataflowGraph::RegisterIdSet::iterator iter = (*ii).second.find(reg);            
            if (iter != (*ii).second.end()) {
                return true;
            }            
        } 
        return false;
    }
    std::string OptimizePTX::getKey(analysis::DataflowGraph& dfg, ir::PTXOperand::RegisterType reg) {        

        for(analysis::DataflowGraph::shmem_iterator ii=dfg.shmap.begin(); ii!=dfg.shmap.end(); ++ii)
        {     
            analysis::DataflowGraph::RegisterIdSet::iterator iter = (*ii).second.find(reg);            
            if (iter != (*ii).second.end()) {
                return (*ii).first;
            }
        } 
    } 
    
    void OptimizePTX::collectSharedVars(ir::IRKernel *ptxKernel, analysis::DataflowGraph& dfg) {
        
        ir::Kernel::LocalMap::const_iterator it = ptxKernel->locals.begin();
        
        for (; it != ptxKernel->locals.end(); ++it) {                    
                if (it->second.space == ir::PTXInstruction::Shared) {
                        if(it->second.attribute == ir::PTXStatement::Extern) {
                                sharedMap[it->second.name]= it->second.getSize();
                                sharedVars.push_back(it->second.name);
                        }
                        else {
                           sharedMap[it->second.name]= it->second.getSize(); 
                            sharedVars.push_back(it->second.name);           
                        }
                }
        }
    }
    void OptimizePTX::printDFG(analysis::DataflowGraph& dfg) {

        analysis::DataflowGraph::const_iterator block = ++dfg.begin(); 
        analysis::DataflowGraph::const_iterator blockEnd = --dfg.end(); 

        for (; block != blockEnd; block++) { 
           std::cout << "\n\n**************Basic Block: " <<  block->label() << "************** \n"; 
           InstructionVector::const_iterator next =  block->instructions().begin();
           InstructionVector::const_iterator end =  block->instructions().end();                   
           
           //std::cout << "Forward Liveness In " << block->fowardLiveIn << "\n";        
           //std::cout << "Backward Liveness In " << block->backwardLiveIn << "\n";
           std::cout << "Total Liveness Out " << block->liveIn << "\n";
           std::cout << "Count In " << block->ref_countIn << "\n";
           std::cout << "Count Out " << block->ref_countOut << "\n";
           
           
           while (next != end) {
                ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(next->i);
                //std::cout << next->i->toString()<< std::endl; 
                next++;      	
           }
          // std::cout << "Forward Liveness Out " << block->fowardLiveOut << "\n";
           //std::cout << "Backward Liveness Out " << block->backwardLiveOut << "\n";
           std::cout << "Total Liveness Out " << block->liveOut << "\n";
           std::cout << "Count In " << block->ref_countIn << "\n";
           std::cout << "Count Out " << block->ref_countOut << "\n";
           
        }  
    }
    
    void OptimizePTX::transformPTX(analysis::DataflowGraph& dfg, analysis::LoopAnalysis& loopAnalysis, analysis::PostdominatorTree* pdt, ir::IRKernel *ptxKernel) { 
        
        int Opt_Ref_count = MAX_COUNT;
        std::list<stringSet>::iterator sit;
        
        std::list<std::string> *optimalVars;
        int iter_count=0;
        int opt_index=0;
        
        std::cout << "\n<--------------------Starting Transformation----------------------->\n\n";
        
        for (sit=possibleSets.begin(); sit!=possibleSets.end(); ++sit)
        {   
            std::list<std::string>::iterator it;
            for (it=(*sit).begin(); it!=(*sit).end(); ++it) {
		  std::cout << ' ' << *it;
	    }
            int Ref_count= dfg.doSharedMemAnalysis(*sit, loopAnalysis, pdt);
            std::cout << "----------> Count Out "  << Ref_count<< "<-----------------\n";            
            if(Opt_Ref_count > Ref_count ) {                
                Opt_Ref_count = Ref_count;
                opt_index = iter_count;
               
            }
            iter_count++;
        }
        sit=possibleSets.begin();
        std::advance(sit,opt_index);
        
        int Ref_count= dfg.doSharedMemAnalysis(*sit, loopAnalysis, pdt);          
            
        std::cout << "----------> Count Out "  << Ref_count<< "<-----------------\n";
        
        //dfg.placeOptimalRelease(*sit, loopAnalysis, pdt);        
        dfg.OptimalReleaseAnalysis(*sit, loopAnalysis, pdt);
        
        reorderScratchpadAllocation(ptxKernel, *sit);
        
        /* This has to be debugged correctly */
        
                
    }
    int OptimizePTX::computeSSPSize(std::string *k_name) {  
            float n_res, n_res_tb;
            float per_sharing;
            int ssp;
            std::ifstream fin("/home/vishwesh/gpuocelot-read-only/config.txt", std::ifstream::in);
            std::string line;
            std::istringstream sin;
            bool flag=false;
            
            while (std::getline(fin, line)) {
                    sin.str(line);
                    std::string kernel_name;
                    sin >> kernel_name >> n_res >> n_res_tb >> per_sharing ;
                    //std::cout << "Kernel name is " << *k_name << "\n";
                    
                    //std::cout << kernel_name  << " " << n_res <<  " " << n_res_tb << " " << per_sharing << "\n";
                    
                    if(k_name->compare(kernel_name) == 0)
                    {
                           flag = true;
                           break;
                    }
                    sin.clear();
            }
            
            if(flag) {
                ssp = per_sharing*n_res_tb/100;
            }
            else {
                ssp =0;
            }
            return ssp;
            
            /*while (std::getline(fin, line)) {
                     sin.str(line.substr(line.find("=")+1));
                     if (line.find("Scratchpad Size") != std::string::npos) {		  
                            sin >> n_res;
                     }
                     else if (line.find("Scratchpad per TB") != std::string::npos) {
                            sin >> n_res_tb;
                     }
                     else if (line.find("Sharing Percentage") != std::string::npos) {
                            sin >> per_sharing;
                     }		 
                     sin.clear();
            }
            ssp = per_sharing*n_res_tb/100;
            return ssp;
             */ 
     }
    
    void OptimizePTX::computePossibleSubsets(){
        int n = sharedMap.size();
        int mask[MASK_SIZE]; /* Guess what this is */
        int i;
        for (i = 0; i < n; ++i)
            mask[i] = 0;

        /* Print the first set */
        genSet(mask, n);

        /* Print all the others */
        while (next(mask, n))
            genSet(mask, n);

        printPossibilities();     
 
    }
    void OptimizePTX::printPossibilities() {

	std::list<stringSet>::iterator sit;
	std::list<std::string>::iterator it;
	 
	 for (sit=possibleSets.begin(); sit!=possibleSets.end(); ++sit) {
	 	for (it=(*sit).begin(); it!=(*sit).end(); ++it) {
		  std::cout << ' ' << *it;
		}
		 std::cout << '\n';
	}	
    }

    int OptimizePTX::computeSize(stringSet *ss){

            std::list<std::string>::iterator it;
            int size =0;
            for (it=ss->begin(); it!=ss->end(); ++it) {
                    size= size+sharedMap[*it];
            }
            return size;
    }
    void OptimizePTX::genSet(int mask[], int n) {
        int i;
        stringSet s;
        std::map<std::string,int>::iterator it=sharedMap.begin();    
        for (i = 0;  i < n && it!=sharedMap.end(); ++i, ++it) {
            if (mask[i]) {
                    s.push_back(it->first);
            }
        }
        computeSize(&s);
        if(computeSize(&s) >= ssp ) {   
                computeSize(&s);
                possibleSets.push_back(s);
        }

    }
    int OptimizePTX::next(int mask[], int n) {
        int i;
        for (i = 0; (i < n) && mask[i]; ++i)
            mask[i] = 0;

        if (i < n) {
            mask[i] = 1;
            return 1;
        }
        return 0;
    }
} 