#include "ocelot/ir/interface/ControlFlowGraph.h"
#include "ocelot/analysis/interface/DataflowGraph.h"
#include "ocelot/transforms/interface/PrettyPrinterPass.h"
#include "ocelot/analysis/interface/DivergenceAnalysis.h"
#include "ocelot/analysis/interface/SSAGraph.h"
#include <ocelot/analysis/interface/PostdominatorTree.h>
#include <ocelot/analysis/interface/DominatorTree.h>
#include "ocelot/transforms/interface/InstructionReorder.h"

#include<iostream>

typedef analysis::DataflowGraph::InstructionVector   InstructionVector;
 
  
 namespace transforms {
 
    PrettyPrinterPass::PrettyPrinterPass()
      : KernelPass({"DataflowGraphAnalysis", "LoopAnalysis", "DominatorTreeAnalysis", "PostDominatorTreeAnalysis"}, "PrettyPrinterPass") {
    }
 
    void PrettyPrinterPass::runOnKernel( ir::IRKernel& k ) {
       
        std::cout << "kernel name is " << k.name; 
        
        
    Analysis* dfg_structure = getAnalysis("DataflowGraphAnalysis");
    assert(dfg_structure != 0);
      
    analysis::DataflowGraph& dfg =
		*static_cast<analysis::DataflowGraph*>(dfg_structure);
    
    analysis::DataflowGraph::iterator block = dfg.begin();
    analysis::DataflowGraph::iterator blockEnd = dfg.end();     
     
    auto cfg = k.cfg();     
     
    for (; block != blockEnd; block++) {     
       std::cout << "\n--------------- Start Block: " <<  block->label() << " ---------------\n";        

       InstructionVector::iterator next =  block->instructions().begin();
       InstructionVector::iterator end =  block->instructions().end();

       while (next != end) {            
           ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(next->i);
           std::cout << next->i->toString()<< std::endl;; 
           next++; 
       } 
       std::cout << "--------------- End Block: ---------------\n";             
    }
    
    ir::ControlFlowGraph::edge_iterator edge; 
    for (edge = cfg->edges_begin(); 
               edge != cfg->edges_end(); ++edge) {
               std::cout << edge->head->label() << " -> " << edge->tail->label();
               //out << " " << blockFormatter.toString(edgePtr);
               std::cout << "\n";
    }
    
  }
    
}
