#ifndef PRETTY_PRINTER_PASS_H_
#define PRETTY_PRINTER_PASS_H_

#include <ocelot/transforms/interface/Pass.h>
#include "ocelot/analysis/interface/DataflowGraph.h"

namespace transforms {
/*! \brief This pass prints the instructions in the Dataflow Graph
 */
class PrettyPrinterPass: public KernelPass
{
private:

public:
  PrettyPrinterPass();
  virtual ~PrettyPrinterPass() {};
  virtual void initialize( const ir::Module& m ){};
  virtual void runOnKernel( ir::IRKernel& k );
  virtual void finalize(){};
};

}

#endif /* BLOCKUNIFICATIONPASS_H_ */
