/* 
 * File:   OptimizePTX.h
 * Author: vishwesh
 *
 * Created on 2 November, 2015, 2:14 PM
 */

#ifndef OPTIMIZEPTX_H
#define	OPTIMIZEPTX_H


 
#include <ocelot/transforms/interface/Pass.h>
#include <ocelot/analysis/interface/DataflowGraph.h>

#include <ocelot/ir/interface/PTXOperand.h>
#include <ocelot/ir/interface/PTXInstruction.h>
#include <ocelot/ir/interface/ControlFlowGraph.h>
#include <ocelot/ir/interface/Kernel.h>
#include "ocelot/ir/interface/PTXKernel.h"
#include <math.h>
#include<map>
#include <algorithm>

#define MASK_SIZE 1024
#define MAX_COUNT 10000000




namespace transforms { 
/*! \brief This pass prints the instructions in the Dataflow Graph 
 */ 
class OptimizePTX: public KernelPass 
{ 
private: 
    std::list<std::string> sharedVars;
    std::map<std::string,int> sharedMap;
    typedef std::list<std::string> stringSet;
    std::list<stringSet> possibleSets;
    int ssp;
 
 
 
public: 
  OptimizePTX(); 
  virtual ~OptimizePTX() {}; 
  virtual void initialize( const ir::Module& m ){}; 
  virtual void runOnKernel( ir::IRKernel& k ); 
  virtual void finalize(){}; 
  void computeShmemMap(analysis::DataflowGraph& dfg); 
  bool isfound(analysis::DataflowGraph& dfg, ir::PTXOperand::RegisterType reg);
  std::string getKey(analysis::DataflowGraph& dfg, ir::PTXOperand::RegisterType reg);
  void printDFG(analysis::DataflowGraph& dfg);
  void transformPTX(analysis::DataflowGraph& dfg, analysis::LoopAnalysis& loopAnalysis, analysis::PostdominatorTree* pdt, ir::IRKernel *ptxKernel);
  void collectSharedVars(ir::IRKernel *ptxKernel, analysis::DataflowGraph& dfg);
  int computeSSPSize(std::string *k_name);
  void computePossibleSubsets();
  void printPossibilities();
  int computeSize(stringSet *ss);
  void genSet(int mask[], int n);
  int next(int mask[], int n);
  void reorderScratchpadAllocation(ir::IRKernel *ptxKernel, std::list<std::string>& sharedVars);
  
}; 
 
} 



#endif	/* OPTIMIZEPTX_H */

