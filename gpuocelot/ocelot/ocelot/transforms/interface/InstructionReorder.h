/* 
 * File:   InstructionReorder.h
 * Author: vishwesh
 *
 * Created on 17 April, 2016, 7:22 PM
 */

#ifndef INSTRUCTIONREORDER_H
#define	INSTRUCTIONREORDER_H


#include <ocelot/transforms/interface/Pass.h>
#include "ocelot/analysis/interface/DataflowGraph.h"
#include <ocelot/ir/interface/Instruction.h>

namespace transforms {
/*! \brief This pass prints the instructions in the Dataflow Graph
 */
class InstructionReorderPass: public KernelPass
{
private:
    
    class Graph
    {
        int V;    // No. of vertices'
        int *height;

        // Pointer to an array containing adjacency listsList
        std::list<int> *adj;
        std::list<int> *parent;
        
    public: 
        Graph(int V);   // Constructor
        
         // function to add an edge to graph
        void addEdge(int v, int w);
        
        void compute_height(int v, int height);
        
        void print_graph();
    }; 

   /* Graph::Graph(int V)
    {
        this->V = V;
        adj = new list<int>[V];
         
    } */
    
    std::map<ir::Instruction::RegisterType,std::list<int>> readMap;
    std::map<ir::Instruction::RegisterType,std::list<int>> writeMap;
    
    
    typedef std::map<ir::Instruction::RegisterType,std::list<int>>::iterator writemap_iterator, readmap_iterator;
    
    
    

public:
  InstructionReorderPass();
  virtual ~InstructionReorderPass() {};
  virtual void initialize( const ir::Module& m ){};
  virtual void runOnKernel( ir::IRKernel& k );
  virtual void finalize(){};
  void insert_dependency_edges(ir::PTXInstruction* inst, int inst_count, Graph *g);
};

}

#endif	/* INSTRUCTIONREORDER_H */




