/*! \file DataflowGraph.cpp
	\author Gregory Diamos <gregory.diamos@gatech.edu>
	\date Tuesday June 23, 2009
	\file The source file for the DataflowGraph class.
*/

#include <ocelot/analysis/interface/DataflowGraph.h>
#include <ocelot/analysis/interface/SSAGraph.h>


#include <ocelot/ir/interface/IRKernel.h>

#include <hydrazine/interface/string.h>
#include <ocelot/executive/interface/CooperativeThreadArray.h>
#include <cmath>
#include <cfenv>

#include <unordered_map>
#include <list>
#include <ocelot/analysis/interface/LoopAnalysis.h>
#include <ocelot/analysis/interface/DominatorTree.h>

#include <queue>

#ifdef REPORT_BASE
#undef REPORT_BASE
#endif

#define REPORT_BASE    0
#define REPORT_CONVERT 0
#define MAX_RANGE_VALUE 1000000

namespace analysis
{

DataflowGraph::Register::Register( RegisterId _id, Type _type ) 
	: type( _type ), id( _id )
{

}

DataflowGraph::Register::Register( const RegisterPointer& r ) 
	: type( r.type ), id( *r.pointer )
{

}

DataflowGraph::RegisterPointer::RegisterPointer( 
	RegisterId* _id, Type _type ) : type( _type ), pointer( _id )
{

}

bool DataflowGraph::Register::operator==( const Register& r ) const
{
	return id == r.id;
}

bool DataflowGraph::RegisterPointer::operator==( 
	const RegisterPointer& r ) const
{
	return *pointer == *r.pointer;
}

DataflowGraph::NoProducerException::NoProducerException( 
	RegisterId reg )
{
	std::stringstream message;
	message << "No producer exists for register " << reg;
	_message = message.str();
}

DataflowGraph::NoProducerException::~NoProducerException() throw()
{
	
}

const char* DataflowGraph::NoProducerException::what() const throw()
{
	return _message.c_str();
}

DataflowGraph::PhiInstruction::PhiInstruction(const Register& destination,
	const RegisterVector& source)
: d(destination), s(source)
{

}

std::string DataflowGraph::PhiInstruction::toString() const
{
	std::stringstream stream;
	
	stream << "phi [r" << d.id << "] <- ";
	
	for(auto source = s.begin(); source != s.end(); ++source)
	{
		if(source != s.begin()) stream << ", ";
		
		stream << "r" << source->id;
	}
	
	return stream.str();
}

DataflowGraph::Instruction DataflowGraph::convert( ir::PTXInstruction& i )
{
	Instruction result;

	reportE( REPORT_CONVERT, " Converting instruction \"" 
		<< i.toString() << "\"" );

	ir::PTXOperand ir::PTXInstruction::* sources[ 5 ] 
		= { &ir::PTXInstruction::pg, &ir::PTXInstruction::a, 
		&ir::PTXInstruction::b, &ir::PTXInstruction::c, 
		&ir::PTXInstruction::d };

	unsigned int limit = 4;

	if( ir::PTXInstruction::St == i.opcode )
	{
		limit = 5;
	}
	else if( ir::PTXInstruction::Bfi == i.opcode )
	{
		limit = 5;
		sources[ 4 ] = &ir::PTXInstruction::pq;
	}

	for( unsigned int j = 0; j < limit; ++j )
	{
		if( !( i.*sources[ j ] ).array.empty() )
		{
			for( ir::PTXOperand::Array::iterator 
				fi = ( i.*sources[ j ] ).array.begin(); 
				fi != ( i.*sources[ j ] ).array.end(); ++fi )
			{
				if( !fi->isRegister() )
				{
					continue;
				}
				reportE( REPORT_CONVERT, "  Converting register \"" 
					<< fi->identifier << "\" to id " << fi->reg
					<< " type " << ir::PTXOperand::toString( fi->type ) );
				_maxRegister = std::max( _maxRegister, fi->reg );
				result.s.push_back( 
					RegisterPointer( &fi->reg, fi->type ) );
			}
		}			
		else if( ( i.*sources[ j ] ).isRegister() )
		{
			if( ( i.*sources[ j ] ).type == ir::PTXOperand::pred )
			{
				if( ( i.*sources[ j ] ).condition == ir::PTXOperand::PT
					|| ( i.*sources[ j ] ).condition 
					== ir::PTXOperand::nPT )
				{
					continue;
				}
			}

			reportE( REPORT_CONVERT, "  Converting register \"" 
				<< ( i.*sources[ j ] ).identifier 
				<< "\" to id " << ( i.*sources[ j ] ).reg << " type "
				<< ir::PTXOperand::toString( ( i.*sources[ j ] ).type ) );
			_maxRegister = std::max( _maxRegister, 
				( i.*sources[ j ] ).reg );
			result.s.push_back( 
				RegisterPointer( &( i.*sources[ j ] ).reg, 
				( i.*sources[ j ] ).type ) );
		}
	}

	ir::PTXOperand ir::PTXInstruction::* destinations[ 2 ] 
		= { &ir::PTXInstruction::pq, &ir::PTXInstruction::d };

	if( ir::PTXInstruction::St == i.opcode )
	{
		limit = 1;
	}
	else if( ir::PTXInstruction::Bfi == i.opcode )
	{
		limit = 1;
		destinations[ 0 ] = &ir::PTXInstruction::d;
	}
	else
	{
		limit = 2;
	}

	for( unsigned int j = 0; j < limit; ++j )
	{
		if( !( i.*destinations[ j ] ).array.empty() )
		{
			for( ir::PTXOperand::Array::iterator 
				fi =  ( i.*destinations[ j ] ).array.begin(); 
				fi != ( i.*destinations[ j ] ).array.end(); ++fi )
			{
				if( !fi->isRegister() )
				{
					continue;
				}
				reportE( REPORT_CONVERT, "  Converting register \"" 
					<< fi->identifier 
					<< "\" to id " << fi->reg
					<< " type " << ir::PTXOperand::toString( fi->type ) );
				_maxRegister = std::max( _maxRegister, fi->reg );
				result.d.push_back( 
					RegisterPointer( &fi->reg, fi->type ) );
			}
		} 
		else if( ( i.*destinations[ j ] ).isRegister() )
		{
			if( ( i.*destinations[ j ] ).type == ir::PTXOperand::pred )
			{
				if( ( i.*destinations[ j ] ).condition == ir::PTXOperand::PT
					|| ( i.*destinations[ j ] ).condition 
					== ir::PTXOperand::nPT )
				{
					continue;
				}
			}			

			_maxRegister = std::max( _maxRegister, 
				( i.*destinations[ j ] ).reg );
			if( i.mayHaveRelaxedTypeDestination() )
			{
				result.d.push_back( 
					RegisterPointer( &( i.*destinations[ j ] ).reg, 
					i.type ) );
			}
			else
			{
				result.d.push_back( 
					RegisterPointer( &( i.*destinations[ j ] ).reg, 
					( i.*destinations[ j ] ).type ) );
			}
			
			reportE( REPORT_CONVERT, "  Converting register \"" 
				<< ( i.*destinations[ j ] ).identifier 
				<< "\" to id " << *result.d.back().pointer << " type "
				<< ir::PTXOperand::toString(
					result.d.back().type ) );
		}

	}
	
	result.i = &i;
	
	return result;
}

bool DataflowGraph::Block::_equal( const RegisterSet& one, 
	const RegisterSet& two )
{
	if( one.size() != two.size() ) return false;
	
	for( RegisterSet::const_iterator fi = one.begin(); 
		fi != one.end(); ++fi )
	{
		RegisterSet::const_iterator ti = two.find( *fi );
		if( ti == two.end() ) return false;
		
		if( ti->type != fi->type ) return false;
	}
	
	return true;
}

DataflowGraph::Block::Block( DataflowGraph& dfg, 
	ir::ControlFlowGraph::iterator block ) : _fallthrough( dfg.end() ),
	_type( Body ), _block( block ), _dfg( &dfg )
{
	_fallthrough = dfg.end();
	for( ir::ControlFlowGraph::InstructionList::const_iterator 
		fi = block->instructions.begin(); 
		fi != block->instructions.end(); ++fi )
	{
		_instructions.push_back( dfg.convert( 
			*static_cast< ir::PTXInstruction* >( *fi ) ) );
	}
        _aliveSharedLocIn = false;
        _aliveSharedLocOut = false;
        loop_count = -1;
        hasClear = false;
        isDone = false;
}

DataflowGraph::Block::Block( DataflowGraph& dfg, Type t ) :
	_fallthrough( dfg.end() ), _type( t ), _dfg( &dfg )
{
	loop_count = -1;
        hasClear = false;
}

void DataflowGraph::Block::_resolveTypes( DataflowGraph::Type& d,
	const DataflowGraph::Type& s )
{
	if( ir::PTXOperand::relaxedValid( d, s ) )
	{
		d = s;
	}
}

bool DataflowGraph::Block::compute( bool hasFallthrough )
{
	if( type() != Body ) return false;
	
	report( " Block name " << label() );
	
	RegisterSet previousIn = std::move( _aliveIn );
	assert( _aliveIn.empty() );
	
	report( "  Scanning targets: " );
	
	if( hasFallthrough )
	{
		report( "   " << _fallthrough->label() );		
		_aliveOut = _fallthrough->_aliveIn;
	}
	
	bool isOwnPredecessor = false;
	
	for( BlockPointerSet::iterator bi = _targets.begin(); 
		bi != _targets.end(); ++bi )
	{
		isOwnPredecessor |= (this == &(**bi));
		report( "   " << (*bi)->label() );		
		for( RegisterSet::iterator ri = (*bi)->_aliveIn.begin(); 
			ri != (*bi)->_aliveIn.end(); ++ri )
		{
                    _aliveOut.insert( *ri );
		}
	}
	
	_aliveIn = _aliveOut;
	
	for( InstructionVector::reverse_iterator ii = _instructions.rbegin(); 
		ii != _instructions.rend(); ++ii )
	{
		for( RegisterPointerVector::iterator di = ii->d.begin(); 
			di != ii->d.end(); ++di )
		{
			RegisterSet::iterator ai = _aliveIn.find( *di );
			if( ai != _aliveIn.end() )
			{
				_resolveTypes( di->type, ai->type );
				_aliveIn.erase( *ai );
			}
		}
		
		for( RegisterPointerVector::iterator si = ii->s.begin(); 
			si != ii->s.end(); ++si )
		{
			std::pair<RegisterSet::iterator, bool> insertion =
				_aliveIn.insert( *si );
			
			if( !insertion.second )
			{
				_resolveTypes( si->type, insertion.first->type );
			}
		}
	}
	
	if( isOwnPredecessor )
	{
		for( RegisterSet::iterator ri = _aliveIn.begin(); 
			ri != _aliveIn.end(); ++ri )
		{
			_aliveOut.insert( *ri );
		}
	}
	
	return !_equal( _aliveIn, previousIn );
}

/* Value Range Analysis */
/*
bool  DataflowGraph::Block::computeSharedLiveInfo(bool hasFallthrough )
{
	if( type() != Body ) return false;
	
	report( " Block name " << label() );        
        
        bool previousIn = _aliveSharedLocIn;
	
        report( "  Scanning targets: " );
        
          
        if( hasFallthrough )
	{
		report( "   " << _fallthrough->label() );		
		_aliveSharedLocOut = _fallthrough->_aliveSharedLocIn;
	} 
	bool isOwnPredecessor = false;
       
	for( BlockPointerSet::iterator bi = _targets.begin(); 
		bi != _targets.end(); ++bi )
	{
		isOwnPredecessor |= (this == &(**bi));
		report( "   " << (*bi)->label() );
                _aliveSharedLocOut = (*bi)->_aliveSharedLocIn || _aliveSharedLocOut ;
	}
        _aliveSharedLocIn = _aliveSharedLocOut;
	
	for( InstructionVector::reverse_iterator ii = _instructions.rbegin(); 
		ii != _instructions.rend(); ++ii )
	{           
                ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(ii->i); 
                if(inst->isLoad() && inst->addressSpace == ir::PTXInstruction::Shared ) {                    
                    _aliveSharedLocIn = true;
                }
	}        
       
        return !(_aliveSharedLocIn == previousIn);
}
bool  DataflowGraph::Block::computeRegisterRange(bool hasFallthrough )
{
	if( type() != Body ) return false;
	
	report( " Block name " << label() );        
        
        RegisterSet previousOut = std::move( _rangeOut );
	assert( _rangeOut.empty() );
        
        report( "  Scanning successive blocks: " );  
        
        if( hasFallthrough )
	{
	//	report( "   " << _fallthrough->label() );		
	//	_aliveSharedLocOut = _fallthrough->_aliveSharedLocIn;
	} 
	bool isOwnPredecessor = false;        
       
	for( BlockPointerSet::iterator bi = _predecessors.begin(); bi != _predecessors.end(); ++bi )
	{
                isOwnPredecessor |= (this == &(**bi));
		report( "   " << (*bi)->label() );
                
                for( RegisterSet::iterator ri = (*bi)->_rangeOut.begin(); ri != (*bi)->_rangeOut.end(); ++ri )
		{              
                    RegisterSet::iterator r = _rangeIn.find( *ri );
                     if( r != _rangeIn.end() ) {
                         if (r->lower  > ri->lower ) {
                             Register R ;
                             R.id = r->id;
                             R.type = r->type;
                             R.lower = ri->lower;
                             _rangeIn.insert(R);
                             _rangeIn.erase(*r);
                         }
                         if ( r->upper < ri->upper ) {
                             Register R ;
                             R.id = r->id;
                             R.type = r->type;
                             R.upper = ri->upper;
                             _rangeIn.insert(R);
                             _rangeIn.erase(*r);
                         }
                     }
                     else {
                         _rangeIn.insert( *ri );     
                     }
		}                
	}
        _rangeOut = _rangeIn;	        
	for( InstructionVector::iterator ii = _instructions.begin(); 
		ii != _instructions.end(); ++ii )
	{       
                UpdateRangeFromInstruction(ii);
	}   
        return !_equal( _rangeOut, previousOut );

        
}
void DataflowGraph::Block::UpdateRangeFromInstruction(InstructionVector::iterator i) {
    
    
        double lower[3], upper[3];
        double d_lower = 0, d_upper = 0;
        ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(i->i);

        ir::PTXOperand* operands[] = {&inst->a, &inst->b, &inst->c, &inst->pg};

        for (int i =0 ; i<3; i++ ) {
            lower[i] = 0;
            upper[i] = 0;
        }     
        
        RegisterPointerVector::iterator si = i->s.begin();
        for(unsigned int j = 0; j < 4; ++j)
        {
            if(operands[j]->addressMode == ir::PTXOperand::Immediate) {
                double a;
                if(ir::PTXOperand::isFloat(inst->type))
                {
                    a = getDouble(*operands[j]);
                }
                else {
                    a = getValue(*operands[j]); 
                }
                lower[j] = a;
                upper[j] = a;
            }
            else if(operands[j]->addressMode == ir::PTXOperand::Indirect || operands[j]->addressMode == ir::PTXOperand::Address) {
                lower[j] = -MAX_RANGE_VALUE;
                upper[j] =  MAX_RANGE_VALUE; 
            }
            
            
            else if(operands[j]->isRegister()) {
                if(si != i->s.end() ) {
                    RegisterSet::iterator s = _rangeOut.find( *si );       
                    if(s != _rangeOut.end()) {
                        lower[j] = s->lower;
                        upper[j] = s->upper;
                    }
                    ++si;
                }
            }
            else if(operands[j]->addressMode == ir::PTXOperand::Special) {                
                if(operands[j]->special == ir::PTXOperand::ctaId || operands[j]->special == ir::PTXOperand::nctaId) {
                    lower[j] = 0;
                    upper[j] = 65534; // TODO: I am not able to compute the precise value at compile time, it has be determined at runtime 
                    
                }
                else if(operands[j]->special == ir::PTXOperand::tid || operands[j]->special == ir::PTXOperand::ntid) {
                    lower[j] = 0;
                    upper[j] = 511; // TODO: I am not able to compute the precise value at compile time, it has be determined at runtime 
                }
            }            
        }
        for( RegisterPointerVector::iterator di = i->d.begin(); 
                            di != i->d.end(); ++di )
        {      
            if(inst->opcode == ir::PTXInstruction::Mov ) {
                d_lower = lower[0];
                d_upper = upper[0];
            }     
            else if(inst->opcode == ir::PTXInstruction::Add ) {
                for( int j = 0; j< 2; j++ ) {
                    d_lower = d_lower + lower[j];
                    d_upper = d_upper + upper[j];
                }
            }  
            else if(inst->opcode == ir::PTXInstruction::Sub ) {
                for( int j = 0; j< 2; j++ ) {
                    if ( j == 0 ) {
                        d_lower = d_lower + lower[j];
                        d_upper = d_upper + upper[j];
                    }
                    else {                            
                        d_lower = d_lower - upper[j];
                        d_upper = d_upper - lower[j];
                    }
                }
            }
            else if(inst->opcode == ir::PTXInstruction::Div ) {
                // Note: Currently I have implemented only div.full statement 
                for( int j = 0; j< 2; j++ ) {
                    if ( j == 0 ) {
                        d_lower = lower[j];
                        d_upper = upper[j];
                    }
                    else {                            
                        d_lower = d_lower / upper[j];
                        d_upper = d_upper / lower[j];
                    }
                }
            }
            else if(inst->opcode == ir::PTXInstruction::Mul ) {
                d_lower = 1;
                d_upper = 1;
                for( int j = 0; j< 2; j++ ) {
                    d_lower = d_lower * lower[j];
                    d_upper = d_upper * upper[j];
                }
                unsigned long long int u_lower, u_upper;
                u_lower = d_lower;
                u_upper = d_upper;
                if((inst->type == ir::PTXOperand::u16) || (inst->type == ir::PTXOperand::s16)) {
                        if (inst->modifier & ir::PTXInstruction::hi) {
                                u_lower = ((u_lower >> 16) & 0x0ffff);
                                u_upper = ((u_upper >> 16) & 0x0ffff);
                        }
                        else if (inst->modifier & ir::PTXInstruction::lo) { 
                                u_lower = (u_lower & 0x0ffff);
                                u_upper = (u_upper & 0x0ffff);
                        }  
                }
                if((inst->type == ir::PTXOperand::u32) || (inst->type == ir::PTXOperand::s32)) {
                        if (inst->modifier & ir::PTXInstruction::hi) {
                                u_lower = ((u_lower >> 32) & 0x0ffffffff);
                                u_upper = ((u_upper >> 32) & 0x0ffffffff);
                        }
                        else if (inst->modifier & ir::PTXInstruction::lo) { {
                                u_lower = (u_lower & 0x0ffffffff);
                                u_upper = (u_upper & 0x0ffffffff);
                        }
                    }  
                }
                d_lower = u_lower;
                d_upper = u_upper;
            }
            else if(inst->opcode == ir::PTXInstruction::Mad ) {
                d_lower = 1;
                d_upper = 1;
                for( int j = 0; j< 2; j++ ) {
                    d_lower = d_lower * lower[j];
                    d_upper = d_upper * upper[j];
                }
                unsigned long long int u_lower, u_upper;
                u_lower = d_lower;
                u_upper = d_upper;
                if((inst->type == ir::PTXOperand::u16) || (inst->type == ir::PTXOperand::s16)) {
                        if (inst->modifier & ir::PTXInstruction::hi) {
                                u_lower = ((u_lower >> 16) & 0x0ffff);
                                u_upper = ((u_upper >> 16) & 0x0ffff);
                        }
                        else if (inst->modifier & ir::PTXInstruction::lo) { 
                                u_lower = (u_lower & 0x0ffff);
                                u_upper = (u_upper & 0x0ffff);
                        }  
                }
                if((inst->type == ir::PTXOperand::u32) || (inst->type == ir::PTXOperand::s32)) {
                        if (inst->modifier & ir::PTXInstruction::hi) {
                                u_lower = ((u_lower >> 32) & 0x0ffffffff);
                                u_upper = ((u_upper >> 32) & 0x0ffffffff);
                        }
                        else if (inst->modifier & ir::PTXInstruction::lo) { {
                                u_lower = (u_lower & 0x0ffffffff);
                                u_upper = (u_upper & 0x0ffffffff);
                        }
                    }  
                }
                d_lower = u_lower;
                d_upper = u_upper;
                d_lower = d_lower + lower[2];
                d_upper = d_upper + upper[2];
            }
            
            else if(inst->opcode == ir::PTXInstruction::Cvt ) {
                // Note: Currently it has support for cvt.rn.f32.f64. It has to be extended for others similarly
                if(inst->type == ir::PTXOperand::f32) {
                    if(inst->a.type == ir::PTXOperand::f64) {
                        if (inst->modifier == ir::PTXInstruction::rn )  {
                            \/* This function has to be debugged.
                            fesetround(FE_TONEAREST);
                            
                            d_lower = std::nearbyint(lower[0]);
                            d_upper = std::nearbyint(upper[0]);
                            *\/
                        }
                    }
                }
                else {
                    d_lower = lower[0];
                    d_upper = upper[0];
                }
            }
            else if(inst->opcode == ir::PTXInstruction::Ld ) {
                d_lower = lower[0];
                d_upper = upper[0];
            } 
            else if(inst->opcode == ir::PTXInstruction::Bar ) {
                //Note : Currently supported for bar.sync 0
            }
            
            /*  
                \/* if(inst->opcode == ir::PTXInstruction::Setp ) {

                    if(inst->comparisonOperator== ir::PTXInstruction::Eq ) {

                        if (inst->a.addressMode == ir::PTXOperand::Immediate ) {
                            uint64_t a = getValue(inst->a);
                            lower = a;
                            upper = a;
                        }
                    }

                    for( RegisterPointerVector::iterator si = i->s.begin(); 
                            si != i->s.end(); ++si )
                    { 
                        RegisterSet::iterator s = _rangeIn.find( *si );       
                        if(s != _rangeIn.end()) {
                            lower = s->lower + lower;
                            upper = s->upper + upper;
                        }
                    }
                } *\/           


                RegisterSet::iterator ai = _rangeOut.find( *di );
                if( ai != _rangeOut.end() )
                {     
                        Register R ;
                        R.id = ai->id;   
                        R.type = ai->type;
                                
                        R.upper = d_upper;
                        R.lower = d_lower;
                        _rangeIn.insert(R);
                        _rangeIn.erase(*ai); 
                }
                else {
                    Register R ;
                    R.id = *(di->pointer);
                    R.lower = d_lower;
                    R.upper = d_upper;
                    R.type = inst->type;
                    _rangeOut.insert(R);
                }
        }
}

bool DataflowGraph::Block::computeSetPValue(ir::PTXOperand& result,
	const ir::PTXInstruction& ptx)
{
	result = ir::PTXOperand(ir::PTXOperand::Immediate, ptx.d.type);

	uint64_t a = getValue(ptx.a);
	uint64_t b = getValue(ptx.b);

	switch(ptx.comparisonOperator)
	{
	case ir::PTXInstruction::Eq:
	{
		setValue(result, (uint64_t)(a == b));
		break;
	}
	default:
	{
		return false;
	}
	}

	return true;
}
*/
uint64_t DataflowGraph::Block::getMask(const ir::PTXOperand& operand)
{
	uint64_t mask = (1ULL << (operand.bytes() * 8)) - 1;

	if(mask == 0) mask = 0xffffffffffffffffULL;

	return mask;
}

uint64_t DataflowGraph::Block::getValue(const ir::PTXOperand& operand)
{
	uint64_t value = operand.imm_uint;

	uint64_t mask = getMask(operand);

	return value & mask;
}

double DataflowGraph::Block::getDouble(const ir::PTXOperand& operand)
{
	if(operand.type == ir::PTXOperand::f32)
	{
		return operand.imm_single;
	}
	return operand.imm_float;
}

const DataflowGraph::RegisterSet& DataflowGraph::Block::aliveIn() const
{
	return _aliveIn;
}

const DataflowGraph::RegisterSet& DataflowGraph::Block::aliveOut() const
{
	return _aliveOut;
}

DataflowGraph::RegisterSet& DataflowGraph::Block::aliveIn()
{
	return _aliveIn;
}

DataflowGraph::RegisterSet& DataflowGraph::Block::aliveOut()
{
	return _aliveOut;
}
bool DataflowGraph::Block::IsSharedLocaliveIn()
{
	return _aliveSharedLocIn;
}

bool DataflowGraph::Block::IsSharedLocaliveOut()
{
	return _aliveSharedLocOut;
}


DataflowGraph::RegisterSet& DataflowGraph::Block::rangeIn()
{
	return _rangeIn;
}

DataflowGraph::RegisterSet& DataflowGraph::Block::rangeOut()
{
	return _rangeOut;
}

bool DataflowGraph::Block::hasFallthrough() const
{
	return _fallthrough != _dfg->end();
}

DataflowGraph::BlockVector::iterator 
	DataflowGraph::Block::fallthrough() const
{
	return _fallthrough;
}

const DataflowGraph::BlockPointerSet& 
	DataflowGraph::Block::targets() const
{
	return _targets;
}

const DataflowGraph::BlockPointerSet& 
	DataflowGraph::Block::successors() const
{
	return _successors;
}

const DataflowGraph::BlockPointerSet& 
	DataflowGraph::Block::predecessors() const
{
	return _predecessors;
}

DataflowGraph::BlockPointerSet& DataflowGraph::Block::targets()
{
	return _targets;
}

DataflowGraph::BlockPointerSet& DataflowGraph::Block::successors()
{
	return _successors;
}

DataflowGraph::BlockPointerSet& DataflowGraph::Block::predecessors()
{
	return _predecessors;
}

DataflowGraph::Block::Type DataflowGraph::Block::type() const
{
	return _type;
}

const DataflowGraph::InstructionVector& 
	DataflowGraph::Block::instructions() const
{
	return _instructions;
}

DataflowGraph::InstructionVector& DataflowGraph::Block::instructions()
{
	return _instructions;
}

const DataflowGraph::PhiInstructionVector& 
	DataflowGraph::Block::phis() const
{
	return _phis;
}

DataflowGraph::PhiInstructionVector& DataflowGraph::Block::phis()
{
	return _phis;
}

std::string DataflowGraph::Block::label() const
{
	return _block->label();
}

DataflowGraph::const_iterator
	DataflowGraph::Block::producer( const Register& r ) const
{
	assertM( aliveIn().count( r ) != 0, "Register " << r.id 
		<< " is not in the alive-in set of block " << label() );

	BlockPointerSet::const_iterator predecessor = predecessors().end();

	for( BlockPointerSet::const_iterator pi = predecessors().begin();
		pi != predecessors().end(); ++pi )
	{
		if( (*pi)->aliveOut().count( r ) != 0 )
		{

			predecessor = pi;
			break;
		}
	}
	
	if( predecessor == predecessors().end() )
	{
		return _dfg->end();
	}
	
	return *predecessor;
}

DataflowGraph::RegisterSet DataflowGraph::Block::alive( 
	const InstructionVector::const_iterator& i )
{
	RegisterSet alive( _aliveOut );
	
	InstructionVector::const_reverse_iterator 
		end = InstructionVector::const_reverse_iterator( i );
	
	for( InstructionVector::reverse_iterator ii = _instructions.rbegin(); 
		ii != end; ++ii )
	{
		for( RegisterPointerVector::iterator di = ii->d.begin(); 
			di != ii->d.end(); ++di )
		{
			alive.erase( *di );
		}
		
		for( RegisterPointerVector::iterator si = ii->s.begin(); 
			si != ii->s.end(); ++si )
		{
			alive.insert( *si );
		}
	}
	
	return alive;
}

ir::ControlFlowGraph::BasicBlock::Id DataflowGraph::Block::id() const
{
	return _block->id;
}

ir::ControlFlowGraph::iterator DataflowGraph::Block::block()
{
	return _block;
}

ir::ControlFlowGraph::const_iterator DataflowGraph::Block::block() const
{
	return _block;
}

DataflowGraph* DataflowGraph::Block::dfg()
{
	return _dfg;
}

DataflowGraph::DataflowGraph()  
	: KernelAnalysis("DataflowGraphAnalysis", {"PostDominatorTreeAnalysis"}),
		_cfg( 0 ), _consistent( false ), _ssa( SsaType::None ),
		_maxRegister( 0 )
{
}

DataflowGraph::~DataflowGraph()
{
	if( _ssa ) fromSsa();
}

void DataflowGraph::analyze(ir::IRKernel& kernel)
{
	_cfg	    = kernel.cfg();
	_consistent = _cfg->empty();
	
	_blocks.clear();
	_maxRegister = 0;
	
	typedef std::unordered_map< ir::ControlFlowGraph::iterator, 
		iterator > BlockMap;
	BlockMap map;
	
	ir::ControlFlowGraph& cfg = *_cfg;
	
	ir::ControlFlowGraph::BlockPointerVector blocks 
		= cfg.executable_sequence();
	assert( blocks.size() >= 2 );
			
	report( "Importing " << blocks.size() << " blocks from CFG." );
	
	unsigned int count = 1;
	map.insert( std::make_pair( cfg.get_entry_block(), 
		_blocks.insert( _blocks.end(), Block( *this, Block::Entry ) ) ) );	
	for( ir::ControlFlowGraph::BlockPointerVector::iterator 
		bbi = blocks.begin(); bbi != blocks.end(); ++bbi, ++count )
	{
		if( *bbi == cfg.get_exit_block() ) continue;
		if( *bbi == cfg.get_entry_block() ) continue;
		Block newB( *this, *bbi );
		map.insert( std::make_pair( *bbi, 
			_blocks.insert( _blocks.end(), newB ) ) );	
	}
	map.insert( std::make_pair( cfg.get_exit_block(), 
		_blocks.insert( _blocks.end(), Block( *this, Block::Exit ) ) ) );	
	
	_blocks.front()._block = cfg.get_entry_block();
	_blocks.back()._block = cfg.get_exit_block();
	_blocks.back()._fallthrough = _blocks.end();

	report( "Adding edges from CFG" );
	ir::ControlFlowGraph::BlockPointerVector::iterator bbi = blocks.begin();
	for( ; bbi != blocks.end(); ++bbi )
	{
		BlockMap::iterator bi = map.find( *bbi );
		assert( bi != map.end() );
	
		report( " Adding edges into " << (*bbi)->label() );
	
		for( ir::ControlFlowGraph::edge_pointer_iterator 
			ei = (*bbi)->in_edges.begin(); 
			ei != (*bbi)->in_edges.end(); ++ei )
		{
			BlockMap::iterator begin = map.find( (*ei)->head );
			assert( begin != map.end() );
			
			assert( (*ei)->head == begin->second->_block );
			assert( (*ei)->tail == bi->second->_block );
			
			if( (*ei)->type == ir::ControlFlowGraph::Edge::FallThrough )
			{
				report( "  fallthrough " << begin->second->label() << " -> "
					<< bi->second->label() );
				begin->second->_fallthrough = bi->second;
				begin->second->_successors.insert( bi->second );
				bi->second->_predecessors.insert( begin->second );
			}
			else if( (*ei)->type == ir::ControlFlowGraph::Edge::Branch )
			{
				report( "  branch " << begin->second->label() << " -> "
					<< bi->second->label() );
				begin->second->_targets.insert( bi->second );
				begin->second->_successors.insert( bi->second );
				bi->second->_predecessors.insert( begin->second );	
			}
			else
			{
				/* assertM( false, "Got invalid edge type between " 
					<< begin->second->label() << " and " 
					<< bi->second->label() );*/
				begin->second->_targets.insert( bi->second );
				begin->second->_successors.insert( bi->second );
				bi->second->_predecessors.insert( begin->second );
			}
		}
	}
	
	report( "Setting instruction->block pointers" );
	for(auto block = _blocks.begin(); block != _blocks.end(); ++block)
	{
		for(auto instruction = block->instructions().begin();
			instruction != block->instructions().end(); ++instruction)
		{
			instruction->block = block;
		}
	}
	
	auto form = _ssa;
	
	_ssa = None;
	
	compute();
	
	if(form != None)
	{
		toSsa(form);
	}
}

DataflowGraph::iterator DataflowGraph::begin()
{
	return _blocks.begin();
}

DataflowGraph::const_iterator DataflowGraph::begin() const
{
	return _blocks.begin();
}

DataflowGraph::iterator DataflowGraph::end()
{
	return _blocks.end();
}

DataflowGraph::const_iterator DataflowGraph::end() const
{
	return _blocks.end();
}

bool DataflowGraph::empty() const
{
	return _blocks.empty();
}

DataflowGraph::size_type DataflowGraph::size() const
{
	return _blocks.size();
}

DataflowGraph::size_type DataflowGraph::max_size() const
{
	return _blocks.max_size();
}

DataflowGraph::iterator DataflowGraph::insert( iterator predecessor )
{
	BlockVector::iterator successor = predecessor->_fallthrough;

	if(successor == end())
	{
		assert( !predecessor->_targets.empty() );
		successor = *predecessor->_targets.begin();
	}
	
	return insert( predecessor, successor );
}

DataflowGraph::iterator DataflowGraph::insert( iterator predecessor, 
	iterator successor )
{
	assert( predecessor->successors().count( successor ) != 0 );

	_consistent = false;
	
	report( "Inserting new block between " 
		<< predecessor->label() << " and " << successor->label() );
	
	assert( successor != begin() );
	
	BlockPointerSet::iterator fi 
		= successor->_predecessors.find( predecessor );
	assert( fi != successor->_predecessors.end() );
	successor->_predecessors.erase( fi );
	
	ir::ControlFlowGraph::edge_iterator edge = 
		predecessor->_block->get_edge( successor->_block );
	ir::BasicBlock::Edge::Type edgeType = edge->type;
	
	_cfg->remove_edge( edge );
        
        
	
	iterator current = _blocks.insert( successor, Block( *this, 
		Block::Body ) );
	current->_block = _cfg->insert_block(
		ir::ControlFlowGraph::BasicBlock( _cfg->newId() ) );
	
	_cfg->insert_edge( ir::ControlFlowGraph::Edge( predecessor->_block, 
		current->_block, edgeType ) );
	
	if( predecessor->_fallthrough == successor )
	{
		predecessor->_fallthrough = current;
		current->_fallthrough = successor;
	}
	else
	{
		BlockPointerSet::iterator ti
			= predecessor->_targets.find( successor );
		assert( ti != predecessor->_targets.end() );
		predecessor->_targets.erase( ti );

		predecessor->_targets.insert( current );
		current->_targets.insert( successor );
	}
		
	BlockPointerSet::iterator ti
		= predecessor->_successors.find( successor );
	assert( ti != predecessor->_successors.end() );
	predecessor->_successors.erase( ti );

	predecessor->_successors.insert( current );
	
	current->_predecessors.insert( predecessor );
	current->_successors.insert( successor );
	successor->_predecessors.insert( current );
	
	_cfg->insert_edge( ir::ControlFlowGraph::Edge( current->_block, 
		successor->_block, edgeType ) );
	
	return current;
}

void DataflowGraph::target( iterator block,
	iterator target, bool fallthrough )
{
	report( "Adding branch from " << block->label() 
		<< " to " << target->label() );
	_consistent = false;
	
	std::pair< BlockPointerSet::iterator, bool > insertion 
		= block->_successors.insert( target );
	
	if( insertion.second )
	{
		assert( target->_predecessors.count( block ) == 0 );
		target->_predecessors.insert( block );
		
		if( fallthrough )
		{
			// this should assert if there is already a fallthrough
			_cfg->insert_edge( ir::ControlFlowGraph::Edge( block->_block, 
				target->_block, ir::ControlFlowGraph::Edge::FallThrough ) );
			block->_fallthrough = target;
		}
		else
		{
			_cfg->insert_edge( ir::ControlFlowGraph::Edge( block->_block, 
				target->_block, ir::ControlFlowGraph::Edge::Branch ) );
			block->_targets.insert( target );
		}
	}
	else
	{
		report( " edge insert failed." );
	}
}

void DataflowGraph::disconnect( iterator block )
{
	report( "Disconnecting edges from " << block->label() );
	_consistent = false;
	
	// DFG edges
	
	/// DFG out-edges
	for( BlockPointerSet::iterator pi = block->_successors.begin(); 
		pi != block->_successors.end(); ++pi )		
	{
		BlockVector::iterator target = *pi;
		
		report( " disconnecting edge to " << target->label() );
	
		BlockPointerSet::iterator fi = target->_predecessors.find( block );
		assert( fi != target->_predecessors.end() );
		target->_predecessors.erase( fi );
	}

	block->_successors.clear();
	block->_targets.clear();

	block->_fallthrough = end();
	
	/// DFG in-edges
	for( BlockPointerSet::iterator pi = block->_predecessors.begin(); 
		pi != block->_predecessors.end(); ++pi )		
	{
		if( (*pi)->_fallthrough == block )
		{
			(*pi)->_fallthrough = end();
		}

		BlockPointerSet::iterator target = (*pi)->_targets.find( block );

		if( target != (*pi)->_targets.end() )
		{
			(*pi)->_targets.erase( target );
		}
	}

	block->_predecessors.clear();
	
	// CFG edges
	_cfg->disconnect_block( block->block() );
}

void DataflowGraph::disconnectOutEdges( iterator block )
{
	report( "Disconnecting out-edges from " << block->label() );
	_consistent = false;
	
	// DFG edges
	
	/// DFG out-edges
	for( BlockPointerSet::iterator pi = block->_successors.begin(); 
		pi != block->_successors.end(); ++pi )		
	{
		BlockVector::iterator target = *pi;
	
		report( " disconnecting edge to " << target->label() );
	
		BlockPointerSet::iterator fi = target->_predecessors.find( block );
		assert( fi != target->_predecessors.end() );
		target->_predecessors.erase( fi );
	}

	block->_successors.clear();
	block->_targets.clear();

	block->_fallthrough = end();
	
	// CFG edges
	_cfg->disconnect_block_out_edges( block->block() );
}

DataflowGraph::iterator DataflowGraph::split( iterator block, 
	unsigned int instruction, bool isFallthrough )
{
	report( "Splitting block " << block->label() 
		<< " at instruction " << instruction );
	_consistent = false;
	
	assertM( instruction <= block->_instructions.size(), 
		"Cannot split block of size " << block->_instructions.size() 
		<< " at instruction " << instruction );
	
	InstructionVector::iterator begin = block->_instructions.begin();
	std::advance( begin, instruction );
	InstructionVector::iterator end = block->_instructions.end();
	
	iterator added = _blocks.insert( ++iterator( block ), 
		Block( *this, Block::Body ) );

	ir::ControlFlowGraph::instruction_iterator cfgBegin =
		block->_block->instructions.begin();
	
	std::advance( cfgBegin, instruction );

	if( isFallthrough )
	{
		added->_block = _cfg->split_block( block->_block, cfgBegin, 
			ir::ControlFlowGraph::Edge::FallThrough );
	}
	else
	{
		added->_block = _cfg->split_block( block->_block, cfgBegin, 
			ir::ControlFlowGraph::Edge::Branch );
	}
	
	added->_predecessors.insert( block );
	
	added->_instructions.insert( added->_instructions.end(), begin, end );
	block->_instructions.erase( begin, end );
	
	added->_fallthrough = block->_fallthrough;

	if( added->_fallthrough != this->end() )
	{
		BlockPointerSet::iterator predecessor = 
			added->_fallthrough->_predecessors.find( block );
		assert( predecessor != added->_fallthrough->_predecessors.end() );
		added->_fallthrough->_predecessors.erase( block );
		added->_fallthrough->_predecessors.insert( added );
	}
	
	for( BlockPointerSet::iterator target = block->_targets.begin(); 
		target != block->_targets.end(); ++target )
	{
		BlockPointerSet::iterator predecessor = 
			(*target)->_predecessors.find( block );
		assert( predecessor != (*target)->_predecessors.end() );
		(*target)->_predecessors.erase( block );
		(*target)->_predecessors.insert( added );
	}
	
	added->_targets = std::move( block->_targets );
	block->_targets.clear();
	
	added->_successors = std::move( block->_successors );
	block->_successors.clear();
	
	if( isFallthrough )
	{
		report("  Joining block via a fallthrough edge.");
		block->_fallthrough = added;
	}
	else
	{
		report("  Joining block via a branch edge.");
		block->_fallthrough = this->end();
		block->_targets.insert( added );
	}
	
	block->_successors.insert( added );
	
	return added;
}

DataflowGraph::iterator DataflowGraph::split( iterator block, 
	InstructionVector::iterator position, bool isFallthrough )
{
	_consistent = false;
	report( "Splitting block " << block->label()); 
	InstructionVector::iterator begin = position; 

	InstructionVector::iterator end = block->_instructions.end();
	if( begin == end )
	{
		return block;	
	}
	unsigned int index = std::distance( block->_instructions.begin(),
		position );

	iterator added = _blocks.insert( ++iterator( block ), 
		Block( *this, Block::Body ) );

	ir::ControlFlowGraph::instruction_iterator cfgBegin =
		block->_block->instructions.begin();
	
	std::advance( cfgBegin, index );

	if( isFallthrough )
	{
		added->_block = _cfg->split_block( block->_block, cfgBegin, 
			ir::ControlFlowGraph::Edge::FallThrough );
	}
	else
	{
		added->_block = _cfg->split_block( block->_block, cfgBegin, 
			ir::ControlFlowGraph::Edge::Branch );
	}
	
	added->_predecessors.insert( block );
	
	added->_instructions.insert( added->_instructions.end(), begin, end );
	block->_instructions.erase( begin, end );
	
	added->_fallthrough = block->_fallthrough;
	for( BlockPointerSet::iterator
		i = added->_fallthrough->_predecessors.begin();
		i != added->_fallthrough->_predecessors.end(); ++i ) 
	{
		report ("predecessors to the the fallthrough edge to this  block "
			<< (*i)->label() << " id " << (*i)->id());
	}	

	if( added->_fallthrough != this->end() )
	{
		BlockPointerSet::iterator predecessor = 
			added->_fallthrough->_predecessors.find( block );
		assert( predecessor != added->_fallthrough->_predecessors.end() );
		added->_fallthrough->_predecessors.erase( block );
		added->_fallthrough->_predecessors.insert( added );
	}
	
	for( BlockPointerSet::iterator target = block->_targets.begin(); 
		target != block->_targets.end(); ++target )
	{
		BlockPointerSet::iterator predecessor = 
			(*target)->_predecessors.find( block );
		assert( predecessor != (*target)->_predecessors.end() );
		(*target)->_predecessors.erase( block );
		(*target)->_predecessors.insert( added );
	}
	
	added->_targets = std::move( block->_targets );
	block->_targets.clear();
	
	added->_successors = std::move( block->_successors );
	block->_successors.clear();
	
	if( isFallthrough )
	{
		report("  Joining block via a fallthrough edge.");
		block->_fallthrough = added;
	}
	else
	{
		report("  Joining block via a branch edge.");
		block->_fallthrough = this->end();
		block->_targets.insert( added );
	}
	
	block->_successors.insert( added );
	
	return added;
}

void DataflowGraph::redirect( iterator source, 
	iterator destination, iterator newTarget )
{
	_consistent = false;

	report( "Redirecting " << source->label() << " from " 
		<< destination->label() << " to " << newTarget->label() );
	BlockPointerSet::iterator 
		predecessor = destination->_predecessors.find( source );
	assertM( predecessor != destination->_predecessors.end(),
		"There is no edge between " << source->label() << " and " 
		<< destination->label() );
	destination->_predecessors.erase( predecessor );

	if( source->_fallthrough == destination )
	{
		report( " Redirecting fallthrough edge." );
		source->_fallthrough = newTarget;
	}
	else
	{
		report( " Redirecting branch edge." );
		BlockPointerSet::iterator 
			target = source->_targets.find( destination );
		assertM( target != source->_targets.end(), 
			"There is no edge between " << source->label() << " and " 
			<< destination->label() );
		source->_targets.erase( target );
		source->_targets.insert( newTarget );
	}
	
	{
		BlockPointerSet::iterator 
			target = source->_successors.find( destination );
		assertM( target != source->_successors.end(), 
			"DFG is not consistent? There is no successor edge between "
			<< source->label() << " and " << destination->label() );
		source->_successors.erase( target );
		source->_successors.insert( newTarget );
	}
	
	newTarget->_predecessors.insert( source );

	ir::ControlFlowGraph::edge_iterator 
		existingEdge = source->_block->get_edge( destination->_block );

	ir::ControlFlowGraph::Edge edge( source->_block, newTarget->_block, 
		existingEdge->type );

	_cfg->remove_edge( existingEdge );
	_cfg->insert_edge( edge );
}

DataflowGraph::InstructionVector::iterator DataflowGraph::insert(
	iterator block, const ir::Instruction& instruction, unsigned int index )
{
	_consistent = false;
	
	InstructionVector::iterator position = block->_instructions.begin();
	std::advance( position, index );
	
	ir::PTXInstruction* ptx = static_cast< ir::PTXInstruction* >( 
		instruction.clone() );
	
	InstructionVector::iterator newPosition =
		block->_instructions.insert( position, convert( *ptx ) );
	
	ir::ControlFlowGraph::InstructionList::iterator 
		bbPosition = block->_block->instructions.begin();
	std::advance( bbPosition, index );
	
	block->_block->instructions.insert( bbPosition, ptx );
	
	return newPosition;
}

DataflowGraph::InstructionVector::iterator DataflowGraph::insert(
	iterator block, const ir::Instruction& instruction,
	InstructionVector::iterator position )
{
	_consistent = false;

	int index = std::distance(block->_instructions.begin(), position);

	ir::PTXInstruction* ptx = static_cast< ir::PTXInstruction* >( 
	instruction.clone() );

	InstructionVector::iterator newPosition = 
		block->_instructions.insert( position, convert( *ptx ) );

	ir::ControlFlowGraph::InstructionList::iterator 
	bbPosition = block->_block->instructions.begin();
	std::advance( bbPosition, index );

	block->_block->instructions.insert( bbPosition, ptx );
	return newPosition;
}

DataflowGraph::InstructionVector::iterator DataflowGraph::insert(
	iterator block, const ir::Instruction& instruction )
{
	return insert( block, instruction, block->instructions().size() );
}

DataflowGraph::iterator DataflowGraph::erase( iterator block )
{
	_consistent = false;
	BlockVector::iterator fallthrough = block->_fallthrough;
	
	BlockPointerSet::iterator fi 
		= fallthrough->_predecessors.find( block );
	assert( fi != fallthrough->_predecessors.end() );
	fallthrough->_predecessors.erase( fi );
	
	fallthrough->_predecessors.insert( block->_predecessors.begin(), 
		block->_predecessors.end() );
	
	for( BlockPointerSet::iterator pi = block->_predecessors.begin(); 
		pi != block->_predecessors.end(); ++pi )		
	{
		if( (*pi)->_fallthrough == block )
		{
			(*pi)->_fallthrough = fallthrough;
			(*pi)->_successors.insert( fallthrough );
			
			_cfg->insert_edge( ir::ControlFlowGraph::Edge( (*pi)->_block, 
				fallthrough->_block, 
				ir::ControlFlowGraph::Edge::FallThrough ) );
		}

		BlockPointerSet::iterator target 
			= (*pi)->_targets.find( block );

		if( target != (*pi)->_targets.end() )
		{
			(*pi)->_targets.erase( target );
			(*pi)->_targets.insert( fallthrough );
			(*pi)->_successors.insert( fallthrough );
			
			_cfg->insert_edge( ir::ControlFlowGraph::Edge( (*pi)->_block, 
				fallthrough->_block, ir::ControlFlowGraph::Edge::Branch ) );
		}
	}
	
	_cfg->remove_block( block->_block );
	
	return fallthrough;
}

void DataflowGraph::clear()
{
	_consistent = true;
	_blocks.clear();
	_blocks.push_back( Block( *this, Block::Entry ) );
	_blocks.push_back( Block( *this, Block::Exit  ) );
	
	_cfg->clear();
	
	_blocks.front()._fallthrough = --_blocks.end();
	_blocks.front()._successors.insert( _blocks.front()._fallthrough );
	_blocks.front()._block = _cfg->get_entry_block();

	_blocks.back()._predecessors.insert( _blocks.begin() );
	_blocks.back()._fallthrough = end();
	_blocks.back()._block = _cfg->get_exit_block();
}

void DataflowGraph::erase( iterator block, unsigned int index )
{
	assert( index < block->instructions().size() );

	_consistent = false;
	
	InstructionVector::iterator position = block->_instructions.begin();
	std::advance( position, index );
	
	block->_instructions.erase( position );
	
	// Erase from the CFG
	ir::ControlFlowGraph::iterator cfgBlock = block->_block;
	
	ir::BasicBlock::InstructionList::iterator instruction =
		cfgBlock->instructions.begin();
		
	std::advance( instruction, index );
	
	cfgBlock->instructions.erase( instruction );
}

DataflowGraph::InstructionVector::iterator DataflowGraph::erase(
	iterator block, InstructionVector::iterator position )
{
	_consistent = false;

	// Erase from DFG
	int index = std::distance( block->_instructions.begin(), position );
	
	InstructionVector::iterator next = position;
	++next;
	
	block->_instructions.erase( position );

	// Erase from the CFG
	ir::ControlFlowGraph::iterator cfgBlock = block->_block;

	ir::BasicBlock::InstructionList::iterator instruction =
	cfgBlock->instructions.begin();

	std::advance( instruction, index );

	cfgBlock->instructions.erase( instruction );
	
	return next;
}

DataflowGraph::InstructionVector::iterator DataflowGraph::erase(
	InstructionVector::iterator position )
{
	return erase( position->block, position );
}

void DataflowGraph::compute()
{
	if( _consistent ) return;		
	_consistent = true;
	
	if( _ssa ) fromSsa();
	
	BlockPointerSet worklist;
	
	for( iterator fi = begin(); fi != end(); ++fi )
	{
		if( fi->type() == Block::Body )
		{
			fi->_aliveIn.clear();
			fi->_aliveOut.clear();
			fi->_phis.clear();
			worklist.insert( fi );
		}
	}
	
	while( !worklist.empty() )
	{
		iterator block = *worklist.begin();
		worklist.erase( worklist.begin() );
		
		report( "Running dataflow for basic block " << block->label() );
		bool changed = block->compute( block->_fallthrough != end() );
		
		if( changed )
		{
			report( "  Block changed, adding predecessors:" );
			for( BlockPointerSet::iterator 
				fi = block->_predecessors.begin(); 
				fi != block->_predecessors.end(); ++fi )
			{
				report( "    " << (*fi)->label() );
				worklist.insert( *fi );
			}
		}                
	}
        
        /* Compute Shared Live information */
        /* 
        for( iterator fi = begin(); fi != end(); ++fi )
	{
		if( fi->type() == Block::Body )
		{
			//fi->_aliveIn.clear();
			//fi->_aliveOut.clear();
			//fi->_phis.clear();
			worklist.insert( fi );
		}
	}
        
        while( !worklist.empty() )
	{
		iterator block = *worklist.begin();
		worklist.erase( worklist.begin() );
		
		report( "Running shared live variable anlaysis for basic block " << block->label() );
		bool changed = block->computeSharedLiveInfo(block->_fallthrough != end());
		
		if( changed )
		{
			report( "  Block changed, adding predecessors:" );
			for( BlockPointerSet::iterator 
				fi = block->_predecessors.begin(); 
				fi != block->_predecessors.end(); ++fi )
			{
				report( "    " << (*fi)->label() );
				worklist.insert( *fi );
			}
		}           
	}
        */
        
        /* Value range analysis */
        /* 
        for( iterator fi = begin(); fi != end(); ++fi )
	{
		if( fi->type() == Block::Body )
		{
			fi->_rangeIn.clear();
			fi->_rangeOut.clear();
			fi->_phis.clear();
			worklist.insert( fi );
		}
	}
        
        while( !worklist.empty() )
	{
		iterator block = *worklist.begin();
		worklist.erase( worklist.begin() );
		
		report( "Running shared live variable anlaysis for basic block " << block->label() );
		bool changed = block->computeRegisterRange(block->_fallthrough != end());
		
		if( changed )
		{
			report( "  Block changed, adding successors:" );
			for( BlockPointerSet::iterator 
				fi = block->_successors.begin(); 
				fi != block->_successors.end(); ++fi )
			{
				report( "    " << (*fi)->label() );
				worklist.insert( *fi );
			}
		}           
	}
        */

	#if !defined(NDEBUG) && REPORT_BASE > 0
	report( "Final Report" );
	
	for( iterator fi = begin(); fi != end(); ++fi )
	{
		report( " Block " << fi->label() );
		report( "  Alive In" );
		for( RegisterSet::iterator ri = fi->_aliveIn.begin(); 
			ri != fi->_aliveIn.end(); ++ri )
		{
			report( "   r" << ri->id << " "
				<< ir::PTXOperand::toString( ri->type ) );
		}
		report( "  Alive Out" );
		for( RegisterSet::iterator ri = fi->_aliveOut.begin(); 
			ri != fi->_aliveOut.end(); ++ri )
		{
			report( "   r" << ri->id << " "
				<< ir::PTXOperand::toString( ri->type ) );
		}
	}
	#endif
}

DataflowGraph::RegisterId DataflowGraph::maxRegister() const
{
	return _maxRegister;
}

DataflowGraph::RegisterId DataflowGraph::newRegister()
{
	return ++_maxRegister;
}

void DataflowGraph::toSsa(DataflowGraph::SsaType form)
{
	compute();
	report("Converting to SSA");
	
	SSAGraph graph( *this, form );
	graph.toSsa();		
}

void DataflowGraph::fromSsa()
{
	SSAGraph graph( *this );
	graph.fromSsa();		
}

unsigned int DataflowGraph::ssa() const
{
	return _ssa;
}

void DataflowGraph::setPreferredSSAType(SsaType form)
{
	assert(_cfg == 0);

	_ssa = form;
}
void DataflowGraph::convertToSSAType(SsaType form)
{
	if(form == _ssa) return;
	
	if(_ssa != None)
	{
		fromSsa();
	}
	
	if(form != None)
	{
		toSsa(form);
	}
}

DataflowGraph::BlockPointerVector DataflowGraph::executableSequence()
{
	ir::ControlFlowGraph::BlockPointerVector 
		sequence = _cfg->executable_sequence();
	
	IteratorMap map = getCFGtoDFGMap();
	
	BlockPointerVector result;
	
	for( ir::ControlFlowGraph::pointer_iterator block = sequence.begin(); 
		block != sequence.end(); ++block )
	{
		IteratorMap::iterator mapping = map.find( *block );
		assert( mapping != map.end() );
		
		result.push_back( mapping->second );
	}
	
	return std::move( result );
}

DataflowGraph::IteratorMap DataflowGraph::getCFGtoDFGMap()
{
	IteratorMap map;
	
	for( iterator block = begin(); block != end(); ++block )
	{
		map.insert( std::make_pair( block->block(), block ) );
	}
	
	return map;	
}

const DataflowGraph::RegisterIdSet& DataflowGraph::getPhiPredicates(
	PhiInstruction const* phi ) const
{
	assertM(hasPhi(phi), "Asked for a phi not known");
	return _phiPredicateMap.find(phi)->second;
}

const DataflowGraph::PhiPredicateMap& DataflowGraph::phiPredicateMap() const
{
	assertM(_ssa == SsaType::Gated, "Asking for a phiPridicateMap not in GSSA");
	return _phiPredicateMap;
}

bool DataflowGraph::hasPhi( DataflowGraph::PhiInstruction const *phi ) const
{
  return _phiPredicateMap.find(phi) != _phiPredicateMap.end();
}

void DataflowGraph::write( std::ostream& out ) const
{
	const DataflowGraph& graph = *this;
	DataflowGraph& nonConstGraph = const_cast< DataflowGraph& >( graph );
	nonConstGraph.compute();
	
	out << "digraph DFG {\n";
	out << "\tb_0_AliveOut[ shape = Mdiamond, label = \"Entry\" ];\n";
	out << "\tb_" << ( graph.size() - 1 ) 
		<< "_AliveIn[ shape = Msquare, label = \"Exit\" ];\n";
	out << "\tb_0_AliveOut -> b_1_AliveIn[ " 
		<< "style = dotted ];\n";
	
	unsigned int blockCount = 1;
	for( DataflowGraph::const_iterator block = ++graph.begin(); 
		block != --graph.end(); ++block, ++blockCount )
	{
		typedef std::unordered_map< DataflowGraph::RegisterId, 
			std::string > RegisterMap;
		RegisterMap map;
					
		out << "\tsubgraph cluster" << blockCount << "{\n";
		out << "\t\tnode[ shape = record ];\n";
		out << "\t\tlabel=\"" << block->label() << "\";\n";
		
		std::stringstream aliveInPrefix;
		aliveInPrefix << "b_" << blockCount << "_AliveIn";
		out << "\t\t" << aliveInPrefix.str() 
			<< "[ shape = record, label = \"{ AliveIn ";
		if( !block->aliveIn().empty() )
		{
			out << " | { ";
			DataflowGraph::RegisterSet::const_iterator 
				ri = block->aliveIn().begin();
			std::stringstream value;
			value << "r" << ri->id;
			out << "<" << value.str() << "> " << value.str();
			map[ ri->id ] = aliveInPrefix.str() + ":" + value.str();
			for( ++ri; ri != block->aliveIn().end(); ++ri )
			{
				std::stringstream stream;
				stream << "r" << ri->id;
				out << " | <" << stream.str() << "> " << stream.str();
				map[ ri->id ] = aliveInPrefix.str() 
					+ ":" + stream.str();
			}
			out << " }";
		}
		out << " }\"];\n";
		
		unsigned int count = 0;
		for( DataflowGraph::PhiInstructionVector::const_iterator 
			ii = block->phis().begin(); 
			ii != block->phis().end(); ++ii, ++count )
		{
			std::stringstream instructionPrefix;
			instructionPrefix << "b_" << blockCount << "_instruction" 
				<< count;

			for( DataflowGraph::RegisterVector::const_iterator 
				si = ii->s.begin(); si != ii->s.end(); ++si )
			{
				assert( map.count( si->id ) != 0 );
				out << "\t\t" << map[ si->id ] << "->" 
					<< instructionPrefix.str() << ":rs" << si->id 
					<< "[style = dashed, color = blue];\n";				
			}
			
			out << "\t\t" << instructionPrefix.str() 
				<< "[ label = \"{ phi | { ";
			
			DataflowGraph::RegisterVector::const_iterator 
			 	si = ii->s.begin();
			 	
			out << "<rs" << si->id << "> ";
			out << "rs" << si->id;
			++si;
			for( ; si != ii->s.end(); ++si )
			{
				out << " | ";
				out << "<rs" << si->id << "> ";
				out << "rs" << si->id;
			}
			
			out << " } | { ";
			std::stringstream value;
			value << "rd" << ii->d.id;
			out << "<" << value.str() << "> " << value.str();
			map[ ii->d.id ] = instructionPrefix.str() + ":" + value.str();
			out << " } }\"];\n";
		}
		
		count = 0;
		for( DataflowGraph::InstructionVector::const_iterator 
			ii = block->instructions().begin(); 
			ii != block->instructions().end(); ++ii, ++count )
		{
			std::stringstream instructionPrefix;
			instructionPrefix << "b_" << blockCount << "_instruction" 
				<< ( count + block->phis().size() );

			for( DataflowGraph::RegisterPointerVector::const_iterator 
				si = ii->s.begin(); si != ii->s.end(); ++si )
			{
				assert( map.count( *si->pointer ) != 0 );
				out << "\t\t" << map[ *si->pointer ] << "->" 
					<< instructionPrefix.str() << ":rs" << *si->pointer
					<< "[style = dashed, color = blue];\n";				
			}
			
			out << "\t\t" << instructionPrefix.str() << "[ label = \"{ " 
				<< hydrazine::toGraphVizParsableLabel(
					ii->i->toString() )
				<< " | { ";
			
			bool any = false;
			
			DataflowGraph::RegisterPointerVector::const_iterator 
			 	si = ii->s.begin();
			 	
			if( si != ii->s.end() )
			{
				out << "<rs" << *si->pointer << "> ";
				out << "rs" << *si->pointer;
				any = true;
				++si;
			}
			for( ; si != ii->s.end(); ++si )
			{
				if( any )
				{
					out << " | ";
				}
				out << "<rs" << *si->pointer << "> ";
				out << "rs" << *si->pointer;
				any = true;
			}
			
			DataflowGraph::RegisterPointerVector::const_iterator 
			 	di = ii->d.begin();
			
			if( di != ii->d.end() )
			{
				if( any )
				{
					out << " } | { ";
				}
				std::stringstream value;
				value << "rd" << *di->pointer;
				out << "<" << value.str() << "> " << value.str();
				map[ *di->pointer ] = instructionPrefix.str() 
					+ ":" + value.str();
				++di;
			}
			for( ; di != ii->d.end(); ++di )
			{
				std::stringstream value;
				value << "rd" << *di->pointer;
				out << " | <" << value.str() << "> " << value.str();
				map[ *di->pointer ] = instructionPrefix.str() + ":" 
					+ value.str();
			}
			
			out << " } }\"];\n"; 
		}

		out << "\t\tb_" << blockCount 
			<< "_AliveOut[ shape = record, label = \"{ AliveOut ";

		if( !block->aliveOut().empty() )
		{
			out << " | { ";
			DataflowGraph::RegisterSet::const_iterator 
				ri = block->aliveOut().begin();
			out << "<r" << ri->id << "> ";
			out << "r" << ri->id;
			for( ++ri; ri != block->aliveOut().end(); ++ri )
			{
				out << " | <r" << ri->id << "> "; 
				out << "r" << ri->id << ""; 
			}
			out << " }";
		}
		out << " }\"];\n";
		
		for( DataflowGraph::RegisterSet::const_iterator 
			ri = block->aliveOut().begin(); 
			ri != block->aliveOut().end(); ++ri )
		{
			assert( map.count( ri->id ) != 0 );
			out << "\t\t" << map[ ri->id ] << "->b_" << blockCount 
				<< "_AliveOut" << ":r" << ri->id 
				<< "[ style=dashed, color=blue];\n";
		}

		out << "\t}\n\n";
		out << "\tb_" << blockCount << "_AliveOut->" << "b_" 
			<< std::distance( graph.begin(), 
				DataflowGraph::const_iterator( block->fallthrough() ) )
			<< "_AliveIn[ style = dotted ];\n";
		
		for( DataflowGraph::BlockPointerSet::const_iterator 
			pi = block->targets().begin(); 
			pi != block->targets().end(); ++pi )
		{
			out << "\tb_" << blockCount << "_AliveOut->" << "b_" 
				<< std::distance( graph.begin(), 
				DataflowGraph::const_iterator( *pi ) ) 
				<< "_AliveIn[ color = red ];\n";
		}

	}
	out << "}";
}

std::ostream& operator<<( std::ostream& out, const DataflowGraph& graph )
{
	graph.write(out);

	return out;
}

std::ostream& operator<<( std::ostream& out,
	const DataflowGraph::PhiInstruction& phi )
{
	out << phi.d.id << " = phi( ";
	
	auto source = phi.s.begin();
	auto sourceEnd = phi.s.end();

	for(; source != sourceEnd; source++){
		out << source->id << ' ';
	}

	out << ")";
	return out;
}

std::ostream& operator<<( std::ostream& out,
	const DataflowGraph::Instruction& i )
{
	auto destination    = i.d.begin();
	auto destinationEnd = i.d.end();

	for(; destination != destinationEnd; destination++)
	{
		out << *destination->pointer << ' ';
	}

	out << i.i->toString() << ' ';

	auto source    = i.s.begin();
	auto sourceEnd = i.s.end();

	for(; source != sourceEnd; source++)
	{
		out << *source->pointer << ' ';
	}
	
	return out;
}

bool DataflowGraph::Block::backwardTransferFunction( bool hasFallthrough, std::list<std::string>& sharedVars )
{
        if( type() != Body ) return false;
	
	report( " Block name " << label() );      
        
        bool previousIn = backwardLiveIn;
        
        report( "  Scanning targets: " );
          
        if( hasFallthrough )
	{
		report( "   " << _fallthrough->label() );		
		backwardLiveOut = _fallthrough->backwardLiveIn;
	} 
	bool isOwnPredecessor = false;
       
	for( BlockPointerSet::iterator bi = _successors.begin(); 
		bi != _successors.end(); ++bi )
	{   
            isOwnPredecessor |= (this == &(**bi));
		report( "   " << (*bi)->label() );
                backwardLiveOut = (*bi)->backwardLiveIn || backwardLiveOut ;
                
	}
        backwardLiveIn = backwardLiveOut;
        
        if(backwardLiveOut == false) {
            for( InstructionVector::reverse_iterator ii = _instructions.rbegin(); 
		ii != _instructions.rend(); ++ii )
            { 
                    ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(ii->i); 
                    
                    if(inst->isLoad() && inst->addressSpace == ir::PTXInstruction::Shared ) {
                        if(!inst->a.identifier.empty())  {
                            if(isfound(inst->a.identifier, sharedVars))
                                backwardLiveIn = true;
                        }
                        else {
                            if(isfound(inst->a.reg, sharedVars))
                                backwardLiveIn = true;
                        }
                    }
                    if( inst->isStore() && inst->addressSpace == ir::PTXInstruction::Shared ) {  
                        
                        if(!inst->d.identifier.empty())  {
                            if(isfound(inst->d.identifier, sharedVars))
                                backwardLiveIn = true;
                        }
                        else {
                            if(isfound(inst->d.reg, sharedVars))
                                backwardLiveIn = true;
                        }
                    }
            }
        }
        return !(backwardLiveIn == previousIn);
}

bool DataflowGraph::Block::isfound(ir::PTXOperand::RegisterType reg, std::list<std::string>& sharedVars ) {
    
        std::list<std::string>::iterator list_iter;
        std::string key;
         
       for(shmem_iterator ii= _dfg->shmap.begin(); ii!=_dfg->shmap.end(); ++ii) {
           
            RegisterIdSet::iterator reg_iter = (*ii).second.find(reg);            
            if (reg_iter != (*ii).second.end()) {
                key = ii->first;
            } 
        } 
        for(list_iter=sharedVars.begin(); list_iter!= sharedVars.end(); list_iter++) {
            
            if(key.compare(*list_iter) == 0){
                return true;
            }    
        }
        return false;
}

bool DataflowGraph::Block::isfound(std::string var, std::list<std::string>& sharedVars ) {
    
        std::list<std::string>::iterator list_iter;
        std::string key;
        
        for(list_iter=sharedVars.begin(); list_iter!= sharedVars.end(); list_iter++) {
            
            if(var.compare(*list_iter) == 0){
                return true;
            }    
        }
        return false;
}
void DataflowGraph::doBackwardShMemAnalysis(std::list<std::string>& sharedVars)
{
        if( _ssa ) fromSsa();
	
	BlockPointerSet worklist;
	
        // Doing the backward analysis to compute the point beyond which there is not access to shared memory location
	for( iterator fi = begin(); fi != end(); ++fi )
	{
		
                fi->backwardLiveIn = false;
                fi->backwardLiveOut = false;
                worklist.insert( fi );                    
	}
        
	while( !worklist.empty() )
	{
		iterator block = *worklist.begin();
		worklist.erase( worklist.begin() );
		
		report( "Running dataflow for basic block " << block->label() );
		bool changed = block->backwardTransferFunction( block->_fallthrough != end(), sharedVars );
		
		if( changed )
		{
			report( "  Block changed, adding predecessors:" );
			for( BlockPointerSet::iterator 
				fi = block->_predecessors.begin(); 
				fi != block->_predecessors.end(); ++fi )
			{
				report( "    " << (*fi)->label() );
				worklist.insert( *fi );
			}
		}                
	}    
}
bool DataflowGraph::Block::forwardTransferFunction( bool hasFallthrough, std::list<std::string>& sharedVars )
{
        if( type() != Body ) return false;
	
	report( " Block name " << label() );      
        
        bool previousOut = fowardLiveOut;
	
        //std::cout << "\n\nBlock Name " << label();
        
        //std::cout << "\nBefore Live In " << fowardLiveIn;
        //std::cout << "\nBefore Live Out " << fowardLiveOut;
        
        report( "  Scanning targets: " );
          
       bool isOwnPredecessor = false;
       
	for( BlockPointerSet::iterator bi = _predecessors.begin(); 
		bi != _predecessors.end(); ++bi )
	{   
                isOwnPredecessor |= (this == &(**bi));
		report( "   " << (*bi)->label() );
                fowardLiveIn = (*bi)->fowardLiveOut || fowardLiveIn ;
                
	}
        fowardLiveOut = fowardLiveIn;
        
        if(fowardLiveIn == false) {
            for( InstructionIterator ii = _instructions.begin();
		ii != _instructions.end(); ++ii )
            { 
                    ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(ii->i); 
                    
                    if(inst->isLoad() && inst->addressSpace == ir::PTXInstruction::Shared ) {
                       
                        if(!inst->a.identifier.empty())  {
                            if(isfound(inst->a.identifier, sharedVars)) {
                                fowardLiveOut = true;
                            }
                        }
                        else {
                            if(isfound(inst->a.reg, sharedVars)) {
                                fowardLiveOut = true;
                            }
                        }
                    }
                    if( inst->isStore() && inst->addressSpace == ir::PTXInstruction::Shared ) {  
                        
                        if(!inst->d.identifier.empty())  {
                            if(isfound(inst->d.identifier, sharedVars))
                                fowardLiveOut = true;
                        }
                        else {
                            if(isfound(inst->d.reg, sharedVars))
                                fowardLiveOut = true;
                        }
                    }
            }
        }
        //std::cout << "\nAfter  Live In " << fowardLiveIn;
        //std::cout << "\nAfter Live out " << fowardLiveOut;        
        
        return !(fowardLiveOut == previousOut);
}

void DataflowGraph::doForwardShMemAnalysis(std::list<std::string>& sharedVars)
{
         if( _ssa ) fromSsa();
	
	BlockPointerSet worklist;
	
        // Doing the Forward analysis to compute the point before which there is not access to shared memory location
	for( iterator fi = begin(); fi != end(); ++fi )
	{
                fi->fowardLiveIn = false;
                fi->fowardLiveOut = false;
                worklist.insert( fi );  
	}
        
	while( !worklist.empty() )
	{
		iterator block = *worklist.begin();
		worklist.erase( worklist.begin() );
		
		report( "Running dataflow for basic block " << block->label() );
		bool changed = block->forwardTransferFunction( block->_fallthrough != end(), sharedVars );
		
		if( changed )
		{
			report( "  Block changed, adding predecessors:" );
			for( BlockPointerSet::iterator 
				fi = block->_successors.begin(); 
				fi != block->_successors.end(); ++fi )
			{
				report( "    " << (*fi)->label() );
				worklist.insert( *fi );
			}
		}                
	} 
}
bool DataflowGraph::Block::checkforShMemAccess(ir::PTXInstruction* inst, std::list<std::string>& sharedVars) {
    
        if(inst->isLoad() && inst->addressSpace == ir::PTXInstruction::Shared ) {
            if(!inst->a.identifier.empty())  {
                if(isfound(inst->a.identifier, sharedVars))
                    return true;
            }
            else {
                if(isfound(inst->a.reg, sharedVars))
                    return true;
            }
        }
        if( inst->isStore() && inst->addressSpace == ir::PTXInstruction::Shared ) {  

            if(!inst->d.identifier.empty())  {
                if(isfound(inst->d.identifier, sharedVars))
                    return true;
            }
            else {
                if(isfound(inst->d.reg, sharedVars))
                    return true;
            }
        }
        return false;
}

bool DataflowGraph::Block::refCountTransferFunction( bool hasFallthrough, std::list<std::string>& sharedVars )
{   
        bool shMemFound = false;
        if( type() != Body ) return false;
	report( " Block name " << label() );      
        
        int previousOut = ref_countOut;	
        
	bool isOwnPredecessor = false;   
        /* Applying the Meet operator */
	for( BlockPointerSet::iterator bi = _predecessors.begin(); 
		bi != _predecessors.end(); ++bi )
	{   
                isOwnPredecessor |= (this == &(**bi));
		report( "   " << (*bi)->label() );
                //std::cout << "\n Predessor is : " << (*bi)->label() << " Live In : " << (*bi)->ref_countOut;
                
                if((*bi)->ref_countOut > ref_countIn)
                    ref_countIn = (*bi)->ref_countOut;
                
	}
        /* Applying Transfer Function */
        ref_countOut = ref_countIn;
        
        if(liveIn && liveOut) {     
                //printf("In True, Out True");
                for( InstructionIterator ii = _instructions.begin();
                    ii != _instructions.end(); ++ii )
                {   
                    ref_countOut++;

                }        
        }
        else if (!liveIn && liveOut) {

                //printf("In False, out True");
                for( InstructionIterator ii = _instructions.begin();
                    ii != _instructions.end(); ++ii )
                {   
                        if(shMemFound) {
                            ref_countOut++;
                            continue;
                        }
                        else {
                                ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(ii->i); 
                                shMemFound= checkforShMemAccess(inst, sharedVars);
                                if(shMemFound) {
                                    ref_countOut++;
                                }
                        }
                }
        }
        else if(liveIn && !liveOut) {
                //printf("In True, out False");
                for( InstructionVector::reverse_iterator ii = _instructions.rbegin(); 
                    ii != _instructions.rend(); ++ii )
                {   
                        if(shMemFound) {
                                ref_countOut++;
                                continue;
                        }
                        else {
                                ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(ii->i); 
                                shMemFound= checkforShMemAccess(inst, sharedVars);
                                if(shMemFound) {
                                    ref_countOut++;
                                }
                        }
                }
        }
        else {
                /* Counting the number of instructions that lie in between the first last scratchpad usage of a basic block */
                //printf("In False  Out False \n");
                int inst_count = 0;
                for( InstructionIterator ii = _instructions.begin();
                    ii != _instructions.end(); ++ii )
                {   
                    inst_count++;
                }
                int front_count=0;
                for( InstructionIterator ii = _instructions.begin();
                    ii != _instructions.end(); ++ii )
                {       
                        ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(ii->i); 
                        shMemFound= checkforShMemAccess(inst, sharedVars);                        
                        front_count++;
                        if(shMemFound) {                            
                            break;
                        }
                }
                if(front_count != inst_count) {
                        //printf("Shared Memory access is found \n");
                        int back_count = inst_count;            
                        for( InstructionVector::reverse_iterator ii = _instructions.rbegin(); 
                            ii != _instructions.rend(); ++ii )
                        {   
                                ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(ii->i); 
                                back_count--;
                                if(back_count == front_count-1)
                                {    
                                    ref_countOut = 1;
                                    break;
                                }
                                shMemFound= checkforShMemAccess(inst, sharedVars);
                                if(shMemFound) {
                                    ref_countOut = back_count-front_count + 2;
                                    break;
                                }
                        }

                }
                else {
                    //printf("No Shared Memory access is found \n");
                }
        } 
       return !(ref_countOut == previousOut);
}
void DataflowGraph::doRefcountAnalysis(std::list<std::string>& sharedVars, LoopAnalysis& loopAnalysis)
{      
        std::priority_queue <Block*, std::vector<Block *>, LessThan> worklist;
        
        std::set< unsigned int> blockIDset;
         
        for( iterator fi = begin(); fi != end(); ++fi )
	{	
                fi->ref_countIn = 0;
                fi->ref_countOut = 0;       
	}
        for( iterator fi = begin(); fi != end(); ++fi )
	{	
                if( fi->type() == Block::Body )
		{
                        worklist.push(&(*fi));                        
                        blockIDset.insert(fi->id());
 		}
	}        
        while(!worklist.empty()) {
             
                Block* block = worklist.top();
                worklist.pop();     
                auto topId = blockIDset.find(block->id());                
                blockIDset.erase(topId);
                bool changed = block->refCountTransferFunction( block->_fallthrough != end(), sharedVars );
                
                if( changed )
                {
                        report( "  Block changed, adding predecessors:" );
                        for( BlockPointerSet::iterator fi = block->_successors.begin(); 
                                fi != block->_successors.end(); ++fi )
                        {
                                if(loopAnalysis.isContainedInLoop(block->getCFGblock()) ) {
                                    
                                        std::string header_label = loopAnalysis._getLoopAt(block->getCFGblock())->getHeader()->label();
                                        int loop_depth = loopAnalysis._getLoopAt(block->getCFGblock())->getLoopDepth() +1 ;
                                        if ((*fi)->label().compare(header_label) == 0)
                                        {
                                                (*fi)->loop_count--;
                                                if((*fi)->loop_count > 0) {
                                                    report( "    " << (*fi)->label() );
                                                    
                                                    auto searchId = blockIDset.find((*fi)->id());
                                                    
                                                    if(searchId == blockIDset.end()) {
                                                        worklist.push(&(**fi));
                                                        blockIDset.insert((*fi)->id());
                                                        
                                                    }
                                                }
                                                else {
                                                    if(loop_depth != 1) {
                                                        (*fi)->loop_count = LOOP_COUNT;
                                                    }
                                                }
                                        }
                                        else {
                                                auto searchId = blockIDset.find((*fi)->id());
                                                if(searchId == blockIDset.end()) {
                                                    worklist.push(&(**fi));
                                                    blockIDset.insert((*fi)->id());
                                                }
                                        }
                                    
                                }
                                else {
                                        auto searchId = blockIDset.find((*fi)->id());
                                        if(searchId == blockIDset.end()) {
                                            worklist.push(&(**fi));
                                            blockIDset.insert((*fi)->id());
                                        }
                                }
                        }
                        
                } 
                 
         }
}
ir::ControlFlowGraph::iterator DataflowGraph::Block::getCFGblock() {    
    
    return _block;    
}

DataflowGraph::iterator DataflowGraph::getDFGblock(ir::ControlFlowGraph::iterator _cfg) {
    
        for( iterator fi = begin(); fi != end(); ++fi )
        { 
            if(fi->id() == _cfg->id) {
                return fi;
            }
        }
}
DataflowGraph::iterator DataflowGraph::computeImmPostDOM(std::list<std::string>& sharedVars, LoopAnalysis& loopAnalysis, PostdominatorTree* pdt) {
        
        
        bool shMemFound; 
       
        for( iterator fi = begin(); fi != end(); ++fi )
	{   
            if( fi->liveIn && !fi->liveOut) {
                
                lastsspblocks.insert(fi);    
            }
            // The basic block which contain starting and ending instructions*
            if( !fi->liveIn && !fi->liveOut) {
                
                InstructionVector::iterator next =  fi->instructions().begin();
                InstructionVector::iterator end =  fi->instructions().end();

                while (next != end) {
                        ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(next->i);
                        shMemFound= fi->checkforShMemAccess(inst, sharedVars);
                        if(shMemFound) {                            
                                lastsspblocks.insert(fi); 
                                break;
                        }   
                        next++;
                }
            }
            // To handle Loop header
            if(fi->liveIn && fi->liveOut) {
                
                 for( BlockPointerSet::iterator succ = fi->successors().begin(); 
                                succ != fi->successors().end(); ++succ )
                 {
                     if(!(*succ)->liveIn && !(*succ)->liveOut) {                         
                         lastsspblocks.insert(fi); 
                         break;
                     }
                 }
            }
	} 
        BlockPointerSet::iterator block = lastsspblocks.begin();
        BlockPointerSet::iterator blockEnd = lastsspblocks.end();
        
        ir::ControlFlowGraph::iterator commonpostdom= begin()->getCFGblock(); 
        for (; block != blockEnd; block++) {
             if(pdt->postDominates((*block)->getCFGblock(), commonpostdom)) {
                 commonpostdom = (*block)->getCFGblock();
             }
             else if(!pdt->postDominates(commonpostdom, (*block)->getCFGblock())) {
                 commonpostdom = pdt->getCommonPostDominator (commonpostdom, (*block)->getCFGblock());
             }
             std::cout << commonpostdom->label() << "\n";             
        }
        return getDFGblock(commonpostdom);
}
bool DataflowGraph::PredDominatesLSSP(iterator pred) {
    
        analysis::Analysis* domAnalysis = getAnalysis("DominatorTreeAnalysis");
        assert(domAnalysis != 0);
        analysis::DominatorTree* domTree = static_cast<analysis::DominatorTree*>(domAnalysis);
        
        for(BlockPointerSet::iterator it= lastsspblocks.begin(); it!= lastsspblocks.end(); ++it) {
            if(domTree->dominates(pred->getCFGblock(), (*it)->getCFGblock())) 
                return true;
        }
        return false;
}

bool DataflowGraph::placeRelease(std::list<std::string>& sharedVars, LoopAnalysis& loopAnalysis, PostdominatorTree* pdt, iterator b) {
    
        bool shMemFound;
        shMemFound = false;        
        
        if(b->hasClear) {
            return true;
        }
        if( b->liveIn && b->liveOut) {
                return false;
        }
        
        int inst_count = 0;
        for( InstructionIterator ii = b->instructions().begin(); ii != b->instructions().end(); ++ii )
        {   
                inst_count++;
        }
        int count=0;
        
        ir::PTXInstruction* clear = new ir::PTXInstruction(ir::PTXInstruction::Clear);          
        
        // If a basic block has last access to SSP then insert RELEASE after the laster instruction 
        if( ( b->liveIn && !b->liveOut) || ( !b->liveIn && !b->liveOut))   {
            
                for( InstructionVector::reverse_iterator ii = b->instructions().rbegin(); 
                       ii != b->instructions().rend(); ++ii )
                {   
                        count++;
                        ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(ii->i); 
                        shMemFound= b->checkforShMemAccess(inst, sharedVars);
                        if(shMemFound) {
                            b->hasClear = true;    
                            insert(b, *clear, inst_count-count+1);
                            b->isDone = true;
                            return true;
                        }
                }
                b->isDone = true;
        }
        analysis::Analysis* domAnalysis = getAnalysis("DominatorTreeAnalysis");
        assert(domAnalysis != 0);
        analysis::DominatorTree* domTree = static_cast<analysis::DominatorTree*>(domAnalysis);
        
        for(BlockPointerSet::iterator it= lastsspblocks.begin(); it!= lastsspblocks.end(); ++it) {
            if(domTree->dominates(b->getCFGblock(), (*it)->getCFGblock())) 
                return false;
        }        
        if(!shMemFound) {                
                for( BlockPointerSet::iterator pred = b->predecessors().begin(); 
                                    pred != b->predecessors().end(); ++pred )
                {   
                        if((!(*pred)->hasClear) && !((*pred)->isDone)) {
                                //std::cout << "Processing Predessor:  " << (*pred)->label() <<  "\n";                                                
                                bool hasPlaced = placeRelease(sharedVars, loopAnalysis, pdt, *pred);

                                if(!hasPlaced) {
                                    if( ( b->predecessors().size() > 1) && ((*pred)->successors().size() > 1))
                                    {   
                                        InstructionVector::reverse_iterator ii = (*pred)->instructions().rbegin();
                                        ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(ii->i);
                                        
                                        if(b->label().compare(inst->d.toString()) == 0 ) {
                                            std::string newLabel;
                                            //newLabel.append("BB_1_");
                                            newLabel.append("BB_");
                                            newLabel = newLabel + std::to_string(_cfg->kernel->id);
                                            newLabel.append("_");
                                            newLabel = newLabel + std::to_string (_cfg->_nextId);                                            
                                            inst->d = std::move(ir::PTXOperand(newLabel));
                                        }                                       
                                        
                                        /* Splitting the critical edge */
                                        std::cout << "Block should be inserted in between " << (*pred)->label() << " and " << b->label() <<"\n";
                                        iterator newblock = insert(*pred, b);
                                        insert(newblock, *clear, 1); 
                                        
                                        /* Inserting a bra.uni instruction after the clear */
                                        ir::PTXInstruction* branch = new ir::PTXInstruction(ir::PTXInstruction::Bra);
                                        branch->uni = true;
                                        branch->d = std::move(ir::PTXOperand(b->label()));
                                        insert(newblock, *branch);
                                        
                                        /* Replacing the target of the predecessor node correctly*/
                                        newblock->hasClear = true;
                                        newblock->isDone = true;
                                    }
                                    else {
                                        insert(b, *clear, b->instructions().begin());
                                        b->hasClear = true;
                                        b->isDone = true;
                                    }
                                    pred = b->predecessors().begin();
                                }
                        }
                }
                return true;
        }
}
void DataflowGraph::placeOptimalRelease(std::list<std::string>& sharedVars, LoopAnalysis& loopAnalysis, PostdominatorTree* pdt) {
    
        iterator ipdom = computeImmPostDOM(sharedVars, loopAnalysis, pdt); 
        //std::cout << "Final PostDom is " << ipdom->label() << "\n";
        /*std::cout << "Last SSP Blocks \n";
        for(BlockPointerSet::iterator it= lastsspblocks.begin(); it!= lastsspblocks.end(); ++it) {
            std::cout << (*it)->label() << "\n";
        } */
        placeRelease(sharedVars, loopAnalysis, pdt, ipdom);
}


/* Optimal Insertion of Clear using Dataflow analysis*/

void DataflowGraph::OptimalReleasePoint(std::list<std::string>& sharedVars, LoopAnalysis& loopAnalysis, PostdominatorTree* pdt) {
    
        bool shMemFound;
        shMemFound = false; 
        
        iterator block = ++begin();
        iterator blockEnd = end();
        
        
        ir::PTXInstruction* clear = new ir::PTXInstruction(ir::PTXInstruction::Clear);          
        for (; block != blockEnd; block++) { 
            
            int inst_count = 0;
            for( InstructionIterator ii = block->instructions().begin(); ii != block->instructions().end(); ++ii )
            {   
                    inst_count++;
            }
            int count=0;            
            
            std::cout << "\n\n**************Basic Block: " <<  block->label() << "************** \n"; 
            std::cout << "Scratchpad In " << block->scratchpadLiveIn << "\n";
            std::cout << "Scratchpad Out " << block->scratchpadLiveOut << "\n";
            
            if(!block->scratchpadLiveIn && block->scratchpadLiveOut) 
            {   
                    for( InstructionVector::reverse_iterator ii = block->instructions().rbegin(); 
                       ii != block->instructions().rend(); ++ii )
                    {   
                            count++;
                            ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(ii->i); 
                            shMemFound= block->checkforShMemAccess(inst, sharedVars);
                            if(shMemFound) {
                                insert(block, *clear, inst_count-count+1);
                                block->isDone = true;
                                break;
                            }
                    }
            }
            if(block->scratchpadLiveIn && block->scratchpadLiveOut) 
            {
                    for( BlockPointerSet::iterator pred = block->predecessors().begin(); 
                                    pred != block->predecessors().end(); ++pred )
                    {
                        if((*pred)->scratchpadLiveOut == false && !((*pred)->isDone))
                        {   
                            if( ( block->predecessors().size() > 1) && ((*pred)->successors().size() > 1))
                            {   
                                    InstructionVector::reverse_iterator ii = (*pred)->instructions().rbegin();
                                    ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(ii->i);

                                    if(block->label().compare(inst->d.toString()) == 0 ) {
                                        std::string newLabel;
                                        //newLabel.append("BB_1_");
                                        newLabel.append("BB_");
                                        newLabel = newLabel + std::to_string(_cfg->kernel->id);
                                        newLabel.append("_");
                                            
                                        newLabel = newLabel + std::to_string (_cfg->_nextId);                                            
                                        inst->d = std::move(ir::PTXOperand(newLabel));
                                    }                                       

                                    /* Splitting the critical edge */
                                    std::cout << "Block should be inserted in between " << (*pred)->label() << " and " << block->label() <<"\n";
                                    iterator newblock = insert(*pred, block);
                                    insert(newblock, *clear, 1); 
                                    
                                    /* Inserting a bra.uni instruction after the clear */
                                    ir::PTXInstruction* branch = new ir::PTXInstruction(ir::PTXInstruction::Bra);
                                    branch->uni = true;
                                    branch->d = std::move(ir::PTXOperand(block->label()));
                                    insert(newblock, *branch);
                                    newblock->isDone = true;
                            }
                            else {
                                insert(block, *clear, block->instructions().begin());
                                 block->isDone = true;
                            }
                            pred = block->predecessors().begin();
                        }
                  }
            }
        }
}
            
           



/* Optimal Insertion of Clear using Dataflow analysis*/

bool DataflowGraph::Block::OptimalReleaseTransferFunction( bool hasFallthrough, std::list<std::string>& sharedVars )
{
        if( type() != Body ) return false;
	
	report( " Block name " << label() );      
        
        bool previousIn = scratchpadLiveIn;
        
        report( "  Scanning targets: " );
          
        if( hasFallthrough )
	{
		report( "   " << _fallthrough->label() );		
		scratchpadLiveOut = _fallthrough->scratchpadLiveIn;
	} 
	bool isOwnPredecessor = false;
       
	for( BlockPointerSet::iterator bi = _successors.begin(); 
		bi != _successors.end(); ++bi )
	{   
                isOwnPredecessor |= (this == &(**bi));
		report( "   " << (*bi)->label() );
                scratchpadLiveOut = (*bi)->scratchpadLiveIn && scratchpadLiveOut ;
                
	}
        scratchpadLiveIn = scratchpadLiveOut;        
        
        if(scratchpadLiveOut == true) {
            for( InstructionVector::reverse_iterator ii = _instructions.rbegin(); 
		ii != _instructions.rend(); ++ii )
            { 
                    ir::PTXInstruction* inst = static_cast<ir::PTXInstruction*>(ii->i); 
                    
                    if(inst->isLoad() && inst->addressSpace == ir::PTXInstruction::Shared ) {
                        if(!inst->a.identifier.empty())  {
                            if(isfound(inst->a.identifier, sharedVars))
                                scratchpadLiveIn = false;
                        }
                        else {
                            if(isfound(inst->a.reg, sharedVars))
                                scratchpadLiveIn = false;
                        }
                    }
                    if( inst->isStore() && inst->addressSpace == ir::PTXInstruction::Shared ) {  
                        
                        if(!inst->d.identifier.empty())  {
                            if(isfound(inst->d.identifier, sharedVars))
                                scratchpadLiveIn = false;
                        }
                        else {
                            if(isfound(inst->d.reg, sharedVars))
                                scratchpadLiveIn = false;
                        }
                    }
            }
        }
        return !(scratchpadLiveIn == previousIn);
}
/* Optimal Insertion of Clear using Dataflow analysis*/
void DataflowGraph::OptimalReleaseAnalysis(std::list<std::string>& sharedVars, LoopAnalysis& loopAnalysis, PostdominatorTree* pdt) {
    
        //placeRelease(sharedVars, loopAnalysis, pdt, ipdom);
    
        if( _ssa ) fromSsa();
	
	BlockPointerSet worklist;
	
        // Doing the backward analysis to compute the point beyond which there is not access to shared memory location
	for( iterator fi = begin(); fi != end(); ++fi )
	{
                fi->scratchpadLiveIn = true;
                fi->scratchpadLiveOut = true;
                worklist.insert( fi );                    
	}
        
	while( !worklist.empty() )
	{
		iterator block = *worklist.begin();
		worklist.erase( worklist.begin() );
		
		report( "Running dataflow for basic block " << block->label() );
		bool changed = block->OptimalReleaseTransferFunction( block->_fallthrough != end(), sharedVars );
		
		if( changed )
		{
			report( "  Block changed, adding predecessors:" );
			for( BlockPointerSet::iterator 
				fi = block->_predecessors.begin(); 
				fi != block->_predecessors.end(); ++fi )
			{
				report( "    " << (*fi)->label() );
				worklist.insert( *fi );
			}
		}                
	}
        iterator block = ++begin();
        iterator blockEnd = end();
        for (; block != blockEnd; block++) { 
            std::cout << "\n\n**************Basic Block: " <<  block->label() << "************** \n"; 
            std::cout << "Scratchpad In " << block->scratchpadLiveIn << "\n";
            std::cout << "Scratchpad Out " << block->scratchpadLiveOut << "\n";
           
        }
        OptimalReleasePoint(sharedVars, loopAnalysis, pdt);
        
}




int DataflowGraph::doSharedMemAnalysis(std::list<std::string>& sharedVars, LoopAnalysis& loopAnalysis, PostdominatorTree* pdt) {    
        
        int Ref_count=0; 
    
        doBackwardShMemAnalysis(sharedVars);
        doForwardShMemAnalysis(sharedVars);        
        
        /* Merging the backward and forward analysis*/
        for( iterator fi = begin(); fi != end(); ++fi )
	{   
            fi->liveIn = false;
            fi->liveOut = false;
            fi->liveIn = fi->fowardLiveIn && fi->backwardLiveIn;
            fi->liveOut = fi->fowardLiveOut && fi->backwardLiveOut;
	}
        doRefcountAnalysis(sharedVars, loopAnalysis);  
        
        iterator block = ++begin();
        iterator blockEnd = end();
        
        for (; block != blockEnd; block++) { 
            std::cout << "\n\n**************Basic Block: " <<  block->label() << "************** \n"; 
            std::cout << "Total Liveness Out " << block->liveIn << "\n";
            std::cout << "Count In " << block->ref_countIn << "\n";
            std::cout << "Count Out " << block->ref_countOut << "\n";


            std::cout << "Total Liveness Out " << block->liveOut << "\n";
            std::cout << "Count In " << block->ref_countIn << "\n";
            std::cout << "Count Out " << block->ref_countOut << "\n";
           
        }
        
        block = ++begin();
        blockEnd = end();
              
        for (; block != blockEnd; block++)                
        {   
            if( block->type() == analysis::DataflowGraph::Block::Exit )
            {   
                for( BlockPointerSet::iterator pred = block->predecessors().begin(); 
                                    pred != block->predecessors().end(); ++pred )
                {   
                    if((*pred)->ref_countOut > Ref_count) {
                        Ref_count =  (*pred)->ref_countOut;
                    }
                    
                }
            }   
        }  
        return Ref_count;
}
}
